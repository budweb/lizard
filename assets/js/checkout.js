$(document).ready(function(){
	$.ajaxSetup({cache:false});
	
	paymethod();
	troco();

});


function troco(){
	$(".dinheiro input").keyup(function(){
		var valor = $(this).val();
		var valor = parseFloat(valor.replace(",", "."));

		var total = $(".total").text();
		var total = total.replace("R$", "");
		var total = parseFloat(total.replace(",", "."));

		var troco = valor-total;

		if(troco >= 0){

			troco = troco.toFixed(2)
			var troco = troco.replace(".", ",");
			$(".troco").text("R$ "+troco);
		}else{
			$(".troco").text("R$ 0,00");
		}

		
	});
}

function paymethod(){
	$("input[name=paymethod]").change(function(){

		var valor = $(this).prop("checked", true).val();
		if(valor == "0"){

			$("#dinheiro").prop("disabled", false).focus();

		}else{
			$("#dinheiro").prop("disabled", true);
		}

		
	});
}
