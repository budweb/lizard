$(document).ready(function(){
	$.ajaxSetup({cache:false});
	
	load_dashbord();
	load_menu();

	selectAll();//list select All
	unselectAll();//list select All
	
	save(); //botão salvar
	
	confirmDelete();
	
	confirmDeleteLot() //apagar em massa
	deleteLot();
	
	ajaxrefresh();
});

function load_dashbord(){

	$( document ).ajaxStart(function() {
		$("#ajax-loader").removeClass("hide");
	});

	$.getJSON( "assets/json/modules-admin.json", function( data ){
		base_url = $("#base_url").text();
		html = "";
		$.each( data, function( key, val ) {
			var href = val.href.replace("?/", base_url);
			html += "<div class='col-xs-6 col-sm-4 col-md-3 block'>\n";
			html +=  "<a href='"+href+"' id='"+val.name+"' class='modules "+val.name+"'>\n";
			html +=  "<label>"+val.label+"</label>\n";
			html += "</a></div>\n \n";
		 });
		$("#ajax-loader").addClass("hide");
		$(".dashboard").html(html);
		
	})

}
function load_menu(){

	$( document ).ajaxStart(function() {
		$("#ajax-loader").removeClass("hide");
	});

	$.getJSON( "assets/json/modules-admin.json", function( data ){
		base_url = $("#base_url").text();
		html = "";
		html +=  "<a href='"+base_url+"' title='Dashboard'>\n";
		html +=  "<li style='background-image: url("+base_url+"assets/images/modules-icons/icon-dashboard.png)'></li>\n";
		html += "</a>\n \n";
		$.each( data, function( key, val ) {
			var href = val.href.replace("?/", base_url);
			var image = base_url+"assets/images/modules-icons/icon-"+val.name+".png";
			html +=  "<a href='"+href+"' title='"+val.label+"'>\n";
			html +=  "<li style='background-image: url("+image+")'></li>\n";
			html += "</a>\n \n";
		 });
		$("#ajax-loader").addClass("hide");
		$(".menu ul").html(html);
		
	})

}

function selectAll(){
	$(".selectAll").click(function(){
		$(".checkEdit").each(function(){
			
			$(this).prop("checked", true);
			
		});
		$(this).removeClass("selectAll").addClass("unselectAll");
		$(this).children("label").text("Limpar Seleções");
		unselectAll();//list select All
		return false;
	});	
	
}

function unselectAll(){
	$(".unselectAll").click(function(){
		$(".checkEdit").each(function(){
			
			$(this).prop("checked", false);
			
		});
		$(this).removeClass("unselectAll").addClass("selectAll");
		$(this).children("label").text("Selecionar tudo");
		selectAll();//list select All
		return false;
	});
}

function save(){
	$("a[type=submit]").click(function(){
		
		$("form").submit();
		
		return false;
	});
}
function confirmDelete(){
	
	$(".confirmDelete").click(function(){
		var title = "Delete";
		var name = $(this).attr("name");
		var href = $(this).attr("href");
		var msg = "Tem certeza que deseja apagar "+name+"? <br /> Você não poderá mais desfazer."
		
		$(".modal .modal-header .modal-title").html(title);
		$(".modal .modal-body").html(msg);
		$(".modal .modal-footer .confirm").addClass("btn-danger").text("Delete").attr("href", href);
		$('#myModal').modal();
		return false;
	});
}

function confirmDeleteLot(){
	
	$(".confirmDeleteLot").click(function(){
		/* Acionar Modal bootstrap */
		var elem = $(this);
		var msg = "Tem certeza que deseja apagar todos os ítens selecionados? <br /> Você não poderá mais desfazer."
		$(".modal .modal-header .modal-title").html("Apagar em massa");
		$(".modal .modal-body").html(msg);
		$(".modal .modal-footer .confirm").addClass("btn-danger").addClass("deleteLot").text("Apagar Todos").attr("href",  elem.attr('formaction'));
		$('#myModal').modal();
		deleteLot();
		return false;
	});
	$(".modal .modal-footer .cancel").click(function(){
		$(".modal .modal-footer .confirm").removeClass("deleteLot");
	});
	
}

function deleteLot(){
	$(".deleteLot").click(function(){
		
		var form = $(".form");
		
		form.attr('action', $(this).attr('href'));

		form.submit();
		return false;
	});

}

function ajaxrefresh(){
	
	$("div[type=ajaxrefresh]").each(function(){
		elem = $(this);
		var time = elem.attr("time");
		var loads = elem.attr("load");
		elem.load(loads);

		setInterval(function(){
			elem.load(loads);
			}, time);
		

	});	
}