$(document).ready(function(){


	categoryIcon();
	categoryIconChange();
	
	nomeEcf();
	
	custoItem();
	marginP();
	
	type();
	showCrtlStock();
});

function categoryIcon(){
	
	$(".category-icon-block").click(function(){
		
		$(this).children("input[type=radio].category-radio").prop("checked", true);
		
		categoryIconChange();

	});
	
	
}

function categoryIconChange(){
	$("input[type=radio].category-radio").each(function(){
		$(this).parent(".category-icon-block").removeClass("selected")
		var check = $(this).prop("checked");
		if(check){
			$(this).parent(".category-icon-block").addClass("selected");
		}
		
		
	});
	
}


function nomeEcf(){

	$(".nameItens").keyup(function(){
		var name = $(this).val();
		
		var words = name.split(" ");
		var text = "";
		for(var i = 0; i < words.length; i++){
			var leng = words[i].length;
			var maxSize = leng*60/100;
			maxSize = maxSize.toFixed(0);
			if(maxSize <= 3){
				maxSize = 3;
			}
			if(leng <= 2){
				maxSize = 0;
			}
			if(maxSize > 0){point = "."}else{point = ""}
			
			text += words[i].substr(0, maxSize)+point+" ";
		}
		
		
		$("#nameEcf").val(text);
	});
	
	
}

function custoItem(){

	
	$("#item #margin").keyup(function(){
		
		if($(this).is( ":focus" )){
			marginChange($(this));		
		}
		
		
	});
	
$("#item #price").keyup(function(){
	if($(this).is( ":focus" )){
		priceChange($(this));		
	}
		
	});
$("#item #cost").keyup(function(){
	var priceElem = $("#item #price");
	var price = parseInt(priceElem.val());
	console.log(price);
	if(price != 0){
		priceChange(priceElem);
	}
});
$("#item #marginp").keyup(function(){
	if($(this).is( ":focus" )){
	var cost = $("#item #cost").val();
	var marginp = $(this).val();
	
	var margin = cost * marginp / 100;
	var margin = margin.toFixed(2);
	
	$("#item #margin").val(margin).attr('cache', margin).keyup();
	marginChange($("#item #margin"));
	}
	
});
}

function marginChange(elem){
	
	var margin = parseFloat(elem.val());
	var cost = parseFloat($("#item #cost").val());
	
	
	//price
	var price = cost+margin;
	price = price.toFixed(2);

	
	$("#item #price").val(price).attr('cache', price).keyup();
	
	marginP();
	
}

function priceChange(elem){
	
	var price = parseFloat(elem.val()).toFixed(2);
	var cost = parseFloat($("#item #cost").val()).toFixed(2);
	
	//margin
	var margin = price-cost;
	margin = margin.toFixed(2);
	
	//marginf = convertNumber(margin);
	
	$("#item #margin").val(margin).attr('cache', margin).keyup();
	
	marginP();
}

function marginP(){
	
	if(!$("#item #marginp").is( ":focus" )){
	//margin %
	var margin = parseFloat($("#item #margin").val());
	var cost = parseFloat($("#item #cost").val());
	var marginp = margin*100/cost;
	marginp = parseFloat(marginp.toFixed(2));
	$("#item #marginp").val(marginp);
	}	
}
function type(){
	var val = $(".type input[name=type]:checked").val();
	showTypeAux(val);	

	$('.type').click(function(event) {
		val = $(".type input[name=type]:checked").val();
		var ca = $(".controlAtive").is(':checked');
		var cs = $(".ctrlStock").is(':checked');
		$(".ctrlStock").attr('checked', false);			
	});	
	
	$(".type input[name=type]").change(function(){
		var val = $(this).val();
		showTypeAux(val);		
	});	
}
function showCrtlStock(){

	if($(".controlAtive").is(':checked')){
		$(".linkStock").removeClass("hide");
		$(".ctrlStock").attr('checked', 'checked');
	}

	$('.ctrlStock').click(function(event) {

		val = $(".type input[name=type]:checked").val();
		var ca = $(".controlAtive").is(':checked');
		var cs = $(".ctrlStock").is(':checked');
		if(cs == true && val == 0){
			$(".linkStock").removeClass("hide");
			$(".controlAtive").attr('checked', 'checked');
		}
		if(cs == false && val == 0){
			$(".linkStock").addClass("hide");
		}		
	});	

	$('.controlAtive').click(function(event) {
		val = $(".type input[name=type]:checked").val();
		var ca = $(".controlAtive").is(':checked');
		var cs = $(".ctrlStock").is(':checked');
		if(ca == true && val == 0){
			$(".linkStock").removeClass("hide");
			$(".ctrlStock").attr('checked', 'checked');
		}
		if(ca == false && val == 0){
			$(".linkStock").addClass("hide");
			$(".ctrlStock").attr('checked', false);
		}		
	});

	$(".ctrlStock").change(function() {
		if(this.checked){
			$(":checkbox[name=ctrlStock]").attr("checked", "checked");
		}else{
			$(":checkbox[name=ctrlStock]").removeAttr('checked');
		}

		if($(".ctrlStock").is(':checked')){
			$(".linkStock").removeClass("hide");				
			return false;
		}else{
			$(".linkStock").addClass("hide");
			return false;
		}
	});			
}	
function showTypeAux(val){
	if( val == "0" ){
		$(".productionTime").removeClass("hide")	
	}else{
		$(".productionTime").addClass("hide")
	}
	
	if( val == "2" ){
		$(".linkStock").removeClass("hide")
	}else{
		$(".linkStock").addClass("hide")
	}
}