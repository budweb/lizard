$(document).ready(function(){
	$.ajaxSetup({cache:false});
	
	menu_ajax_itens(); //recebe os itens via ajax
	flex();// abrir w fechar as partes como pesquisa e categorias
	
	demand_list();//
	
	delete_item();
	
	search_cod();
	search_name();
	
	additional();
	service_tax();
	
	comment();
	
	getLinkTableGarcon();

	search_itens();



	
	// legend_itens();

	//alert($(window).width());
});

function menu_ajax_itens(){
	var loads = $("#select_itens").attr('load');
	// var demand = $("#select_itens").attr('demand');
	$("#select_itens").load(loads+"/?start=1");
	$( document ).ajaxComplete(function() {
		
		});
	
	$("#select_category .category").click(function(){
		var href = $(this).attr('href');
		$("#select_itens").load(href);
		$( document ).ajaxComplete(function() {
		
		});
		return false;
	});
}

function flex(){
	
	$("fieldset.flex legend").click(function(){
		var who = $(this).parent("fieldset.flex").attr("id");
		$("fieldset.flex").each(function(){
			var who2 = $(this).attr("id");
		var status = $("fieldset.flex").attr("status");
		if(status=="close"){
		$(this).children('.hide').addClass("show").removeClass("hide");
		$(this).find('span.dropdown').addClass("dropup").removeClass("dropdown");
		$(this).attr("status", "open");
		}else{
			$(this).children('.show').addClass("hide").removeClass("show");
			$(this).find('span.dropup').addClass("dropdown").removeClass("dropup");
			$(this).attr("status", "close");
		}
		});
		
	});
	
}

function search_cod(){
	var local = $("#select_itens").attr("load");
	$("#cod").keyup(function(){
		var cod = $(this).val();
		
		$("#select_itens").load(local+"/?cod="+cod);
		
	});
	
}
function search_name(){
	var local = $("#select_itens").attr("load");
	$("#name").keyup(function(){
		var name = $(this).val();
		
		name = name.replace(/ /gi, "%%");
				
		$("#select_itens").load(local+"/?name="+name);
		
	});
	
}
// 


function demand_list(){
	
	var loads = $("#demands .list").attr("load");
	
	$("#demands .list").load(loads);
	
	$( document ).ajaxComplete(function() {
		delete_item();
		});
	
	
}

// function demand_list_touch_delete(){
	
// 	$("table.demand_list tbody tr").on( "taphold" , function(){

// 		var href = $(this).find(".deleteItem").attr("href");
				
// 		$.ajax({
//             type: 'GET',           
//             url: href            
//             ,success: function(){
//             	demand_list();
//             }          
//         });

// 	});
	
	
	
// }

function delete_item(){
	$(".deleteItem").click(function(){
		
		var href = $(this).attr("href");
		
		$.ajax({
            type: 'GET',           
            url: href            
            ,success: function(){
            	demand_list();
            }          
        });
		
		return false;
	});
}

function additional(){
	$("#additional #ok").click(function(){
		
		var url = $("#additional #url").val();
		var acresc = $("#additional .radio input[name=additional]:checked").val();
		var descr = $("#additional #description").val();
		var value = $("#additional #value").val();
		
		
		var serial = url+"&acresc="+acresc+"&descr="+descr+"&value="+value;
		if(value != 0){
			$.ajax({
	            type: 'GET',           
	            url: serial            
	            ,success: function(data){
	            	if(data==1){
	            	demand_list();
	            	$("#additional #description").val("");
	    			$("#additional #value").val("").keyup();
	    			$("#additional .radio input#plus").prop("checked", true);
	            	}
	            }          
	        });
		}
	});
}

function service_tax(){
	$("#servicetax").change(function(){
		var checked = $(this).prop('checked');
		var loads = $(this).attr('load');
		if(checked){
			var service = 1;
		}else{
			var service = 0;
		}
		var url = loads+"/"+service;
		$.ajax({
            type: 'GET',           
            url: url            
            ,success: function(data){
            	demand_list();
            	}
                     
			});
	});
}

function comment(){

	$(".comment").keyup(function(){

		var text = $(this).val();
		var url = $(this).attr('url');
		var url_complete = url+"&text="+text;
		$.ajax({

			url : url_complete
		});

		
	});
}

function getLinkTableGarcon(){
	$(".setGarcon li").click(function(){

		var garcon = $(this).html();
		$(".getGarcon"). html(garcon);

		$(".form input[name=garcon]").val($(this).attr("value"));
	});

	$(".setTable li").click(function(){

		var table = $(this).html();

		$(".getTable"). html(table);

		$(".getLink img").add(".form input[type=submit]").show(200);

		$(".form input[name=table]").val($(this).attr("value"));
	});
}

function search_itens(){
	$(".btn-search-itens").click(function(){

		$(".frame-search-itens").css("top", "100%").show().animate({"top": 0}, 500);

	});
	$(".frame-search-itens .close").click(function(){

		$(".frame-search-itens").animate({"top": "100%"}, 500, function(){
			$(this).hide();
		});

	});
}

function select_itens_touch(){
	$.event.special.tap.emitTapOnTaphold =false;
$("#select_itens .itens").on({
		tap : function(e){
			$(this).register_item();
		},
		taphold : function(e){
			$(this).legend_item();
			e.preventDefault().stopPropagation();
		}
});

$("document:not(#select_itens .itens), html:not(#select_itens .itens)").tap(function(){
	$(".itens .info-context").fadeOut(200);
	$(this).off("taphold");
});

}



$.fn.register_item = function(){
				var href = $(this).attr("href");
				var id_demand = $("#select_itens").attr("demand");
				var unid = $(this).attr("unid");

				// count click
				var num = $(this).find(".click .num");
				var click = parseInt(num.text());
				if(isNaN(click)){click=0;}
				click++;
				num.text(click);
				$(this).find(".click").show();
				// end count click

				// status click item
				var name = $(this).find(".name").text();
				$(".status .name").text(name);
				$(".status").show().fadeOut(800);
				// end status click item

				var url = href+"/"+id_demand;

				
				if(unid < 1){

					$.ajax({
			             type: 'GET',           
			             url: url            
			             ,success: function(){
			             	demand_list();
			             	// menu_ajax_itens();
			             }          
			         });

				}else{

					$('.modal.qty').modal();
					$('.modal.qty input.qty').val("");
					$('.modal.qty .unids').text($(this).attr("unidname"));
					$('.modal.qty .confirm').attr("href", url);


					$(".modal.qty .confirm").click(function(event){
						
						var href = $(this).attr("href");
						var qty =  $('.modal.qty input.qty').val();
						var url = href+"/"+qty;
						$.ajax({
			             type: 'GET',           
			             url: url            
			             ,success: function(){
			             	$('.modal.qty').modal("hide");
			             	demand_list();
			             	// menu_ajax_itens();
			             } 

			         });

						return false;

					//event.preventDefault().stopPropagation();
						


					});
				}
			 
		
		 return false;
}

$.fn.legend_item = function(){
	$(".itens .info-context").fadeOut(200);
  	$(this).find(".info-context").fadeIn(200);
  	//$(this).off("tap");
  	return false;
  }


