jQuery.fn.ajaxForm = function (callback) {


  $(this).submit(function(){
    var serial = $(this).serialize();
    var action = $(this).attr("action");
    var method = $(this).attr("method").toUpperCase();
    

    $.ajax({
            type: method,            
            url: action,
            data: serial,            
            success: callback           
        });
    $(this).each (function() { this.reset(); });
    return false;
    });
}
 
