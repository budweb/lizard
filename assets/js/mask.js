$(document).ready(function(){
	
	money();
});


function money(){
	var money  = $("input[rel=money]");

	
	money.each(function(){
	if($(this).val()==""){
		$(this).val(moneyFormat('0'));
	}else{
		val = moneyFormat($(this).val());
		$(this).val(val);
		$(this).attr('cache', val);
	}
	
	negativeFormat($(this));
		
	});
	
	
	
	money.keyup(function(){
	
		var num = $(this).val().split(".").join("");

		var rev = strrev(num);
		var lastStr = rev.substring(0,1);
		if(lastStr=="-"){
			minus = "-";
		}else{
			minus = "";
		}
		if(lastStr == NaN){lastStr="";}
		
		var cache = $(this).attr('cache');
		if(cache==undefined){cache=""}
		
		if(num.length > cache.length){
			$(this).attr('cache', minus+parseInt(cache+lastStr));
		}else{
			$(this).attr('cache',num)
			
		}
		$(this).val(moneyFormat($(this).attr('cache')));
		
		negativeFormat($(this));
	});
	money.css('direction', 'rtl');
	
}
function moneyFormat(valor){
	
	//negativo
	if(valor.substring(0,1)=="-"){
		minus = "-";
		valor = valor.slice(1);		
	}else{
		minus = "";
	}
	var size = valor.length;
	
	if(size==0){
		ret = "0.00";
	}else if(size==1){
		ret = "0.0"+valor;
	}else if(size==2){
		ret = "0."+valor;
	}else if(size>=3){
		valRev = strrev(valor)
		var dec = strrev(valRev.substring(0,2));
		var int = strrev(valRev.slice(2));
		ret = int+"."+dec;
	}
	
	return ret+minus;
}

function negativeFormat(elem){
	var val = elem.val();
	var valRev = strrev(val);
	if(valRev.substring(0,1)=="-"){
		elem.css("color", "#900");
	}else{
		elem.css("color", "initial");
	}
	
}

function strrev(val){
	
	var val = val.split("");
	var inverter = val.reverse().join("");
	return inverter;
}


//SET CURSOR POSITION
$.fn.setCursorPosition = function(pos) {
  this.each(function(index, elem) {
    if (elem.setSelectionRange) {
      elem.setSelectionRange(pos, pos);
    } else if (elem.createTextRange) {
      var range = elem.createTextRange();
      range.collapse(true);
      range.moveEnd('character', pos);
      range.moveStart('character', pos);
      range.select();
    }
  });
  return this;
};