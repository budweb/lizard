<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Session_logged {
	
	public function logged($logged=NULL) {
        if($logged == TRUE):
        	current_url();
        elseif(!isset($logged) || $logged != TRUE):             
            redirect(base_url().'login');
        endif;		
	}
	
	function typeUser($val=NULL) {
		
		$arr = array (
			"" => "Selecione",
			"0" => "Proprietário", 
			"1" => "Administrativo", 
			"2" => "Gerente (Maitre)", 
			"3" => "Garçon", 
			"4" => "Caixa"			
		);	
		if($val==NULL):
			return $arr;
		elseif($val=='5'):
			return 'Cozinha';
		elseif($val=='6'):
			return 'Master';
		else:
			return $arr[$val];
		endif;			
	}

	public function access_permission_user(){
		$CI =& get_instance();
		$type = $CI->session->userdata('type');
		if($type != 6):
			redirect('login');				
		endif;		
	}
	
	public function access_permission_admin(){
		$CI =& get_instance();
		$type = $CI->session->userdata('type');
		if($type == 6):
			redirect('login');
		endif;
	}
	
	function load_mod() {
		$CI =& get_instance();
		$path = $_SERVER ['REQUEST_URI'];
		
		//ADICIONAR AO ARRAY OS MÓDULOS OU BOTÕES(MAIN):
		$modulos = array(
				'checkout', 
				'cook_monitor',
				'garcon_monitor',
				'desk_monitor',
				'cookery', 
				'customer',
				'provider', 
				'demands',
				'demands_garcon',
				'myattendances', 
				'itens', 
				'report_excel', 
				'stock', 
				'tables', 
				'user',
				'financial',
				'balance',
				'sales'				
		);
		//DEFINIR ARRAY DE PERMISSÕES DOS MÓDULOS:
		//Tipos de conta:
		//*0 = Proprietário;
		//*1 = Administrativo;
		//*2 = Gerente(Maitre);
		//*3 = Garçon;
		//*4 = Caixa;
		//*5 = Cozinha;					
		$checkout = array('4'); //FECHAMENTO
		$cook_monitor = array('2', '3', '4', '5'); //MONITOR DA COZINHA
		$garcon_monitor = array('3'); //MONITOR DO GARÇON
		$desk_monitor = array('2', '4'); //MONITOR DO CAIXA
		$cookery = array('0', '1'); //COZINHA
		$customer = array('0', '1', '2'); //CLIENTES
		$provider = array('0', '1');
		$demands = array('2', '3', '4'); //ANOTAR PEDIDOS ; MEUS ATENDIMENTOS 
		$demands_garcon = array('4');
		$myattendances = array('2', '3'); //MEUS ATENDIMENTOS
		$itens = array('0', '1'); //ÍTENS
		$report_excel = array('0', '1'); //MENU NO EXCEL
		$stock = array('0', '1', '2'); //ESTOQUE
		$tables = array('0', '1'); //MESAS
		$user = array('0', '1'); //USUÁRIOS
		$financial = array('0', '1'); //FINANCEIRO
		$balance = array('0', '1'); //BALANÇO
		$sales = array('0', '1'); //VENDAS

		//--------------------------------------------------------------------
		foreach ( $modulos as $key => $value ):
			$busca = strstr($path, $value);
			if($busca != NULL):
				$moduleSearch = $value;
				break;
			else:
				$moduleSearch = NULL;
			endif;
		endforeach;		
				
		$type = $CI->session->userdata('type');
		$automaticService = $CI->session->userdata('automaticService');
		$controlStock = $CI->session->userdata('stock');
		$basic = $CI->session->userdata('basic');
		$financialControl = $CI->session->userdata('financialControl');
		
		$aux = NULL;
		  
		switch($moduleSearch):
			//ADICIONAR A VERIFICAÇÃO DE PERMISSÃO DOS MÓDULOS
							
			//--------------------------------------------------------------------
			case 'checkout':
				$ver = 1;
				for ($i = 0; $i <= count($checkout)-1; $i++):
					if(($checkout[$i] == $type) && ($automaticService == 1)):
						current_url();
						break;																			
					endif;
					$ver = $ver + 1;
				endfor;				
				if($ver > count($checkout)):
					echo "Erro Fatal! Você está executando uma atividade ilegal. Não altere o url do identificador.";
					exit();
					
				endif;
			break;
			
			//--------------------------------------------------------------------
			case 'cook_monitor':
				$ver = 1;
				for ($i = 0; $i <= count($cook_monitor)-1; $i++):
					if(($cook_monitor[$i] == $type) && ($automaticService == 1)):
						current_url();
						break;																			
					endif;
					$ver = $ver + 1;									
				endfor;				
				if($ver > count($cook_monitor)):
					echo "Erro Fatal! Você está executando uma atividade ilegal. Não altere o url do identificador.";
					exit();
				endif;
			break;
			
			//--------------------------------------------------------------------
			case 'garcon_monitor':
				$ver = 1;
				for ($i = 0; $i <= count($garcon_monitor)-1; $i++):
				if(($garcon_monitor[$i] == $type) && ($automaticService == 1)):
				current_url();
				break;
				endif;
				$ver = $ver + 1;
				endfor;
				if($ver > count($garcon_monitor)):
				echo "Erro Fatal! Você está executando uma atividade ilegal. Não altere o url do identificador.";
				exit();
				endif;
				break;
				
			//--------------------------------------------------------------------
			case 'desk_monitor':
				$ver = 1;
				for ($i = 0; $i <= count($desk_monitor)-1; $i++):
				if(($desk_monitor[$i] == $type) && ($automaticService == 1)):
				current_url();
				break;
				endif;
				$ver = $ver + 1;
				endfor;
				if($ver > count($desk_monitor)):
				echo "Erro Fatal! Você está executando uma atividade ilegal. Não altere o url do identificador.";
				exit();
				endif;
				break;
			
			//--------------------------------------------------------------------
			case 'cookery':
				$ver = 1;
				for ($i = 0; $i <= count($cookery)-1; $i++):
					if(($cookery[$i] == $type) && ($automaticService == 1)):
						current_url();
						break;																			
					endif;
					$ver = $ver + 1;									
				endfor;				
				if($ver > count($cookery)):
					echo "Erro Fatal! Você está executando uma atividade ilegal. Não altere o url do identificador.";
					exit();
				endif;
			break;
			
			//--------------------------------------------------------------------
			case 'customer':
				$ver = 1;
				for ($i = 0; $i <= count($customer)-1; $i++):
					if(($customer[$i] == $type) && ($basic == 1)):
						current_url();				
						break;																			
					endif;
					$ver = $ver + 1;									
				endfor;				
				if($ver > count($customer)):
					echo "Erro Fatal! Você está executando uma atividade ilegal. Não altere o url do identificador.";
					exit();
				endif;
			break;

			//--------------------------------------------------------------------
			case 'provider':
				$ver = 1;
				for ($i = 0; $i <= count($provider)-1; $i++):
					if(($provider[$i] == $type) && ($basic == 1)):
						current_url();				
						break;																			
					endif;
					$ver = $ver + 1;									
				endfor;				
				if($ver > count($provider)):
					echo "Erro Fatal! Você está executando uma atividade ilegal. Não altere o url do identificador.";
					exit();
				endif;
			break;
			
			//--------------------------------------------------------------------
			case 'demands':				
				$ver = 1;
				for ($i = 0; $i <= count($demands)-1; $i++):
					if(($demands[$i] == $type) && ($automaticService == 1)):
						current_url();				
						break;																					
					endif;
					$ver = $ver + 1;
				endfor;				
				if($ver > count($demands)):
					echo "Erro Fatal! Você está executando uma atividade ilegal. Não altere o url do identificador.";
					exit();
				endif;				
			break;
			
			//--------------------------------------------------------------------
			case 'demands_garcon':
				$ver = 1;
				for ($i = 0; $i <= count($demands_garcon)-1; $i++):
				if(($demands_garcon[$i] == $type) && ($automaticService == 1)):
				current_url();
				break;
				endif;
				$ver = $ver + 1;
				endfor;
				if($ver > count($demands_garcon)):
				echo "Erro Fatal! Você está executando uma atividade ilegal. Não altere o url do identificador.";
				exit();
				endif;
				break;			
			
			//--------------------------------------------------------------------
			case 'myattendances':
				$ver = 1;
				for ($i = 0; $i <= count($myattendances)-1; $i++):
					if(($myattendances[$i] == $type) && ($automaticService == 1)):
						current_url();
						break;
					endif;
					$ver = $ver + 1;
				endfor;
				if($ver > count($myattendances)):
					echo "Erro Fatal! Você está executando uma atividade ilegal. Não altere o url do identificador.";
					exit();
				endif;
			break;
			
			//--------------------------------------------------------------------
			case 'itens':
				$ver = 1;
				for ($i = 0; $i <= count($itens)-1; $i++):
					if(($itens[$i] == $type) && ($automaticService == 1)):
						current_url();
						break;																					
					endif;
					$ver = $ver + 1;
				endfor;				
				if($ver > count($itens)):
					echo "Erro Fatal! Você está executando uma atividade ilegal. Não altere o url do identificador.";
					exit();
				endif;
			break;	

			//--------------------------------------------------------------------
			case 'report_excel':
				$ver = 1;
				for ($i = 0; $i <= count($report_excel)-1; $i++):
					if(($report_excel[$i] == $type) && ($automaticService == 1)):
						current_url();
						break;																			
					endif;
					$ver = $ver + 1;									
				endfor;				
				if($ver > count($report_excel)):
					echo "Erro Fatal! Você está executando uma atividade ilegal. Não altere o url do identificador.";
					exit();
				endif;
			break;

			//--------------------------------------------------------------------
			case 'stock':
				$ver = 1;
				for ($i = 0; $i <= count($stock)-1; $i++):
					if(($stock[$i] == $type) && ($controlStock == 1)):
						current_url();
						break;																			
					endif;
					$ver = $ver + 1;									
				endfor;				
				if($ver > count($stock)):
					echo "Erro Fatal! Você está executando uma atividade ilegal. Não altere o url do identificador.";
					exit();
				endif;
			break;

			//--------------------------------------------------------------------
			case 'tables':
				$ver = 1;
				for ($i = 0; $i <= count($tables)-1; $i++):
					if(($tables[$i] == $type) && ($automaticService == 1)):
						current_url();
						break;																			
					endif;
					$ver = $ver + 1;									
				endfor;				
				if($ver > count($tables)):
					echo "Erro Fatal! Você está executando uma atividade ilegal. Não altere o url do identificador.";
					exit();
				endif;
			break;

			//--------------------------------------------------------------------
			case 'user':
				$ver = 1;
				for ($i = 0; $i <= count($user)-1; $i++):
					if(($user[$i] == $type)):
						current_url();
						break;																			
					endif;
					$ver = $ver + 1;									
				endfor;				
				if($ver > count($user)):
					echo "Erro Fatal! Você está executando uma atividade ilegal. Não altere o url do identificador.";
					exit();
				endif;
			break;

			//--------------------------------------------------------------------
			case 'financial':
				$ver = 1;
				for ($i = 0; $i <= count($financial)-1; $i++):
					if(($financial[$i] == $type) && ($financialControl == 1)):
						current_url();
						break;																			
					endif;
					$ver = $ver + 1;									
				endfor;				
				if($ver > count($financial)):
					echo "Erro Fatal! Você está executando uma atividade ilegal. Não altere o url do identificador.";
					exit();
				endif;
			break;
			
			//--------------------------------------------------------------------
			case 'balance':
				$ver = 1;
				for ($i = 0; $i <= count($balance)-1; $i++):
				if(($balance[$i] == $type) && ($financialControl == 1)):
				current_url();
				break;
				endif;
				$ver = $ver + 1;
				endfor;
				if($ver > count($balance)):
				echo "Erro Fatal! Você está executando uma atividade ilegal. Não altere o url do identificador.";
				exit();
				endif;
				break;
			
			//--------------------------------------------------------------------
			case 'sales':
				$ver = 1;
				for ($i = 0; $i <= count($sales)-1; $i++):
				if(($sales[$i] == $type) && ($financialControl == 1)):
				current_url();
				break;
				endif;
				$ver = $ver + 1;
				endfor;
				if($ver > count($sales)):
				echo "Erro Fatal! Você está executando uma atividade ilegal. Não altere o url do identificador.";
				exit();
				endif;
				break;
						
			//--------------------------------------------------------------------
			default:
				$segment = $CI->uri->segment(1);				
				if($segment == NULL or $segment == "main"):
				//ADICIONAR HELPER DA TELA MAIN
				
				//------------------------------------------------------
				
				foreach ( $itens as $key_itens => $value_itens ):
					($automaticService == 1) && ($value_itens == $type) ? control('buttonMain', array('id'=> 'itens', 'control'=>'itens', 'class'=>'itens', 'label'=>'Itens')): NULL;
				endforeach;
					
				//------------------------------------------------------

				foreach ( $report_excel as $key_report_excel => $value_report_excel ):
					($automaticService == 1) && ($value_report_excel == $type) ? control('buttonMain', array('id'=> 'menu', 'control'=>'report_excel/menu', 'class'=>'menu', 'label'=>'Menu <small>(Relatório no excel)</small>')) : NULL;
				endforeach;
										
				//------------------------------------------------------

				foreach ( $tables as $key_tables => $value_tables ):
					($automaticService == 1) && ($value_tables == $type) ? control('buttonMain', array('id'=> 'table', 'control'=>'tables', 'class'=>'tables', 'label'=>'Mesas')) : NULL;
				endforeach;
				
				//------------------------------------------------------

				foreach ( $cookery as $key_cookery => $value_cookery ):
					($automaticService == 1) && ($value_cookery == $type) ? control('buttonMain', array('id'=> 'cookery', 'control'=>'cookery', 'class'=>'cookery', 'label'=>'Cozinha')) : NULL;
				endforeach;
										
				//------------------------------------------------------
						
				foreach ( $myattendances as $key_myattendances => $value_myattendances ):
					($automaticService == 1) && ($value_myattendances == $type) ? control('buttonMain', array('id'=> 'myattendances', 'control'=>'demands/myattendances', 'class'=>'myattendances', 'label'=>'Meus Atendimentos')) : NULL;
				endforeach;	

				//------------------------------------------------------
					
				foreach ( $demands as $key_demands => $value_demands ):
					($automaticService == 1) && ($value_demands == $type) ? control('buttonMain', array('id'=> 'demand', 'control'=> $type == 4 ? 'demands/register_new_desk' : 'demands', 'class'=>'demand', 'label'=>'Anotar Pedidos')) : NULL;
				endforeach;

				//------------------------------------------------------
					
				foreach ( $demands_garcon as $key_demands_garcon => $value_demands_garcon ):
					($automaticService == 1) && ($value_demands_garcon == $type) ? control('buttonMain', array('id'=> 'demands_garcon', 'control'=> 'demands/link_table_garcon' , 'class'=>'demands_garcon', 'label'=>'Novo Pedido <small>(Garçom)</small>')) : NULL;
				endforeach;
					
				//------------------------------------------------------
				
				if(($type != 3) && ($type != 4)):	
					foreach ( $cook_monitor as $key_cook_monitor => $value_cook_monitor ):
						($automaticService == 1) && ($value_cook_monitor == $type) ? control('buttonMain', array('id'=> 'cook_monitor', 'control'=>'cook_monitor', 'class'=>'cook_monitor', 'label'=>'Monitor <small>(Cozinha)</small>')) : NULL;
					endforeach;
				endif;
				
				//------------------------------------------------------
					
				foreach ( $garcon_monitor as $key_garcon_monitor => $value_garcon_monitor ):
					($automaticService == 1) && ($value_garcon_monitor == $type) ? control('buttonMain', array('id'=> 'cook_monitor', 'control'=>'cook_monitor/garcon_monitor', 'class'=>'garcon_monitor', 'label'=>'Monitor <small>(Garçon)</small>')) : NULL;
				endforeach;
				
				//------------------------------------------------------
					
				foreach ( $desk_monitor as $key_desk_monitor => $value_desk_monitor ):
					($automaticService == 1) && ($value_desk_monitor == $type) ? control('buttonMain', array('id'=> 'cook_monitor', 'control'=>'cook_monitor/desk_monitor', 'class'=>'desk_monitor', 'label'=>'Monitor <small>(Caixa)</small>')) : NULL;
				endforeach;
					
				//------------------------------------------------------

				foreach ( $financial as $key_financial => $value_financial ):
					($financialControl == 1) && ($value_financial == $type) ? control('buttonMain', array('id'=> 'financial', 'control'=>'financial', 'class'=>'financial', 'label'=>'Gráficos <small>(Financeiro)</small>')) : NULL;
				endforeach;

				//------------------------------------------------------
				
				foreach ( $balance as $key_balance => $value_balance ):
					($financialControl == 1) && ($value_balance == $type) ? control('buttonMain', array('id'=> 'balance', 'control'=>'balance', 'class'=>'balance', 'label'=>'Movimento diário')) : NULL;
				endforeach;
				
				//------------------------------------------------------
				
				foreach ( $sales as $key_sales => $value_sales ):
					($financialControl == 1) && ($value_sales == $type) ? control('buttonMain', array('id'=> 'sales', 'control'=>'sales', 'class'=>'sales', 'label'=>'Vendas')) : NULL;
				endforeach;
					
				//------------------------------------------------------
					
				foreach ( $customer as $key_customer => $value_customer ):
					($basic == 1) && ($value_customer == $type) ? control('buttonMain', array('id'=> 'customer', 'control'=>'customer', 'class'=>'customer', 'label'=>'Clientes')) : NULL;
				endforeach;
									
				//------------------------------------------------------
									
				foreach ( $provider as $key_provider => $value_provider ):
					($value_provider == $type) ? control('buttonMain', array('id'=> 'provider', 'control'=>'provider', 'class'=>'provider', 'label'=>'Fornecedores')) : NULL;
				endforeach;
				
				//------------------------------------------------------
					
				foreach ( $user as $key_user => $value_user ):
					($value_user == $type) ? control('buttonMain', array('id'=> 'user', 'control'=>'user', 'class'=>'user', 'label'=>'Usuários')) : NULL;
				endforeach;				
										
				//------------------------------------------------------
						
				foreach ( $stock as $key_stock => $value_stock ):
					($controlStock == 1) && ($value_stock == $type) ? control('buttonMain', array('id'=> 'stock', 'control'=>'stock', 'class'=>'stock', 'label'=>'Estoque')) : NULL;
				endforeach;
				
				//------------------------------------------------------
				
				control('buttonMain', array('id'=> 'config', 'control'=>'settings', 'class'=>'config', 'label'=>'Configurações'));
				
				//------------------------------------------------------
				endif;
		endswitch;						
	}		
}