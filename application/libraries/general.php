<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class General {

	//Codificação e decodificação
	public function url_encode($data) {
		if($data != (NULL || 0)):
			$base64 = rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
			$md5 = strtoupper(md5($base64));
			return $md5.$base64;
		else:
			return $data = NULL;
		endif;
	}
	public function url_decode($data) {
		if($data != (NULL || 0)):
			$base64 = substr($data, 32);
			$md5 = strtolower(substr($data, 0, 32));
			if(md5($base64)==$md5){
				return base64_decode(str_pad(strtr($base64, '-_', '+/'), strlen($base64) % 4, '=', STR_PAD_RIGHT));
			}else{
				echo "Erro Fatal! Você está executando uma atividade ilegal. Não altere o url do identificador.";
				exit();
			}
		else:
			return $data = NULL;
		endif;			
	}
}