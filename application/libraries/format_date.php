<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Format_date{
	
	public function dt_standard($timezone, $daylight_saving = FALSE){
		
		$daylight_saving== 1 ? TRUE : FALSE;
		if($timezone==NULL):
			$timezone = 'UTC';
		endif;
		
		$timezone_str = $timezone;
		$daylight_saving_str = $daylight_saving;
		$datetime_unix = gmt_to_local(now(), $timezone_str, $daylight_saving_str);
		
		$datestring = "%d/%m/%Y";
		return mdate($datestring, $datetime_unix);		
	}
	
	public function dt_mysql($timezone = 'UTC', $daylight_saving = FALSE){

		$daylight_saving== 1 ? TRUE : FALSE;
		if($timezone==NULL):
			$timezone = 'UTC';
		endif;
		$timezone_str = $timezone;
		$daylight_saving_str = $daylight_saving;
		$datetime_unix = gmt_to_local(now(), $timezone_str, $daylight_saving_str);
			
		$datestring = "%Y-%m-%d %H:%i:%s";
		return $date_mysql = mdate($datestring, $datetime_unix);		
	}
	
	public function mysql_to_data_standard($mysql){
		
		$unix = mysql_to_unix($mysql);
		$datestring = "%d/%m/%Y";
		return  mdate($datestring, $unix);
	}
	public function mysql_to_time_standard($mysql){
	
		$unix = mysql_to_unix($mysql);
		$datestring = "%H:%i:%s";
		return  mdate($datestring, $unix);
	}
	
	public function local_date($string=NULL, $time=NULL){
		$CI =& get_instance();
		$CI->load->library('session');
		
		$timezone = $CI->session->userdata('timeZone');
		
		if($timezone==NULL):
		$timezone = 'UTC';
		endif;
		$timezone_str = $timezone;
		$daylight_saving_str = $CI->session->userdata('daylightSaving');
		$time = $time == NULL ? now() : $time;
		$datetime_unix = gmt_to_local($time, $timezone_str, $daylight_saving_str);
			
		$datestring = $string == NULL ? "%Y-%m-%d %H:%i:%s" : $string;
		return mdate($datestring, $datetime_unix);
	}
}
