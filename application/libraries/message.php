<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Message {

function set_message($message, $type="info"){
	$CI =& get_instance();
		$CI->session->set_flashdata('type', $type);
		$CI->session->set_flashdata('message', $message);
	}
function message_afected_rows($value){
	$CI =& get_instance();
		$true = array(
			'deleted' => 'Apagado com Sucesso!',
			'updated' => 'Atualizado com Sucesso!',
			'inserted' => 'Inserido com Sucesso!'
		);
		$false = array(
			'deleted' => 'Desculpe, mas não pudemos apagar registro. Tente mais tarde',
			'updated' => 'Desculpe, mas não pudemos atualizar registro. Tente mais tarde',
			'inserted' => 'Desculpe, mas não pudemos inserir registro. Tente mais tarde'
		);
		if($CI->db->affected_rows()>0):
			$this->set_message($true[$value], 'success');
		else:
			$this->set_message($false[$value], 'danger');
		endif;
	}
	
function quick_message_generate(){
		$CI =& get_instance();
		echo alert($CI->session->flashdata('type'), $CI->session->flashdata('message'));
	}
}