<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_balance extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'description' => array(
				'type' => 'VARCHAR',
				'constraint' => 200,
				'null' => TRUE
			),
			'value' => array(
				'type' => 'DECIMAL',
				'constraint' => '11,2',
				'null' => TRUE
			),
			'type' => array(
				'type' => 'INT',
				'constraint' => '1'
			),
		));



		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('balance');
	}

	public function down()
	{
		$this->dbforge->drop_table('balance');
	}
}