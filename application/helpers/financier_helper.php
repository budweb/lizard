<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('priceFormat'))
{
function priceFormat($valor)
{
	
	//$valor = strrev($valor);
	$valor = explode(".",$valor);
	if($valor[0]<0){
		$minus = true;
	}
	$int = abs($valor[0]);
	$decimal = @$valor[1] != NULL ? $valor[1] : "00";
	$preco = "";

	$intRev = strrev($int);
	for ($i = 0; $i < strlen($intRev); $i++) {
		$preco .= substr($intRev, $i , 1);
	if (($i+1)%3 == 0 AND ($i+1) < strlen($intRev)){
		$preco .= ".";
	}
	
	}
	$minus = @$minus?"-":"";
	$preco = $minus.strrev($preco).",".$decimal;
	return $preco;
}
}

if ( ! function_exists('priceNoFloat'))
{
	function priceNoFloat($valor)
	{

		$valor = str_replace(".", "",$valor);
		
		return $valor;
	}
}

if ( ! function_exists('unid'))
{
	function unid($value=NULL)
	{
		$unidades = array("Unid", "Kg", "g", "l") ;
		if($value!=NULL){
			return $unidades[$value];
		}else{
			return $unidades;
		}
		
	}
}