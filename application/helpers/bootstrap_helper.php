<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('modal'))
{
function modal()
{
	$estrutura = "
	<div class='modal fade'  id='myModal' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
	<div class='modal-dialog'>
	<div class='modal-content'>
	<div class='modal-header'>
	<button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>
	<h4 class='modal-title'>Modal title</h4>
	</div>
	<div class='modal-body'>
	<p>One fine body&hellip;</p>
	</div>
	<div class='modal-footer'>
	<button type='button' class='btn btn-default cancel' data-dismiss='modal'>Cancelar</button>
	<a class='btn confirm'>Ok</a>
	</div>
	</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<!-- Button trigger modal -->";
	
	return $estrutura;
}
}
if ( ! function_exists('alert'))
{
	function alert($type=NULL, $msg=NULL)
	{
		$type = $type==NULL? "info" : $type;
		$html = "<div class='alert alert-$type alert-dismissible' role='alert'>
  	<button type='button' class='close' data-dismiss='alert'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>
  	".@$msg."
	</div>";
		if($msg==NULL){$html="";}
		return $html;
	}
}