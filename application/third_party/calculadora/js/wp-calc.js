$(document).ready(function(){
	
	digit();
	backspace();
	clean();
	operator();
	percent();
	equal();
	keyboard();
	negative();
	memoryCopy();
	memoryPast();
});

function keyboard(){
	//numbers
	var codes = {
			  	 8  : "back",
			  	 13 : "=", 61 : "=",
			  	 27 : "c",
			  	 37 : "%",
	             42 : "*",
	             43 : "+",
			  	 44 : "dot", 46 : "dot", //Virgula e ponto 
			  	 45 : "-",
			  	 47 : "/",
	             48 : "0",
	             49 : "1",
	             50 : "2",
	             51 : "3",
	             52 : "4",
	             53 : "5",
	             54 : "6",
	             55 : "7",
	             56 : "8",
	             57 : "9"    
	             
	};
	
	$(document).keypress(function(e) {
		
		console.log(e.keyCode);
		 if(codes[e.keyCode] != undefined){ var digit = codes[e.keyCode] }else{ var digit = ""}
		
		 if(digit != ""){
			 
			 $('.btn a[rel="'+digit+'"]').click();
		 }
		});
	
	//para esc
	$(document).keyup(function(e) {
		
		if(codes[e.keyCode] == "c"){
			 $('.btn a[rel='+codes[e.keyCode]+']').click();
		}
		if(codes[e.keyCode] == "back"){
			 $('.btn a[rel='+codes[e.keyCode]+']').click();
		}
	});
}

function digit(){
	$('.btn a.num').click(function(){
		var value = $(this).attr("rel");		
		var cacheNow = $(".display .digit").attr('cache');
		
		//verificar se o ponto flutuantes já esxiste
		if(value == "dot"){
			if(cacheNow.indexOf('.') != -1){
				value = "";
			}else{
				value = ".";
			}
		}
		
		var cacheNew = cacheNow+""+value;		
		$(".display .digit").attr('cache', cacheNew);
		
		digit = parseFloat(cacheNew);
		$(".display .digit").text(mask(digit));
		
		
		return false;
	});
	
}

function backspace(){
	$(".btn a[rel=back]").click(function(){
		
		var cacheNow = $(".display .digit").attr('cache');
		var numSep = cacheNow.split("");
		var numlength = numSep.length-1;
		numSep[numlength] = "";
		
		var cacheNew = numSep.join("");
		
		$(".display .digit").attr('cache', cacheNew);
		
		//verifica se é o ultimo digito
		if(numlength <= 0){
			var digit = 0;
		}else{
			var digit = cacheNew;
		}
		$(".display .digit").text(mask(digit));
		
		return false;
		
	});
		
	
}

function clean(){
	$(".btn a[rel=c]").click(function(){
		
		$(".display .digit").attr('cache', '');
		$(".display .digit").text(0);
		
		$(".display .operator").text("");
		$(".display .lastDigit").text("").attr("cache", '');
		
		$(".memory").text("").attr("cache", '');
		$(".display .mem").text("");
		
		displaysize();
		
		return false;
		
	});
		
	
}
function operator(){
	$('.btn a.op').click(function(){
		var lastOp = $(".display .operator").text();
		var lastDigit = $(".display .lastDigit").text();
		
		var op = $(this).attr("rel");
		var digit = $(".display .digit").attr('cache');
		
		if(lastDigit != ""){
			digit = calculator(lastDigit, digit, lastOp);
		}
		
		
		
		$(".display .operator").text(op);
		$(".display .lastDigit").attr('cache', digit);
		$(".display .lastDigit").text(mask(digit));
		
		$(".display .digit").attr('cache', '');
		
		return false;
	});
}
function percent(){
	$('.btn a.percent').click(function(){
		
		var op = $(".display .operator").text();
		var lastDigit = $(".display .lastDigit").attr('cache');
		var digit = $(".display .digit").attr('cache');
		
		if(op != ""){
		var result = calculator("("+lastDigit, digit+")/100", op);
		}
		if(op != "*" && op != ""){
			var r1 = calculator("("+lastDigit, digit+")/100", "*");
			result = calculator(lastDigit, r1, op);
		}
		if(lastDigit == undefined){
			result = calculator(1, digit+"/100", "*");
		}
		
		$(".display .digit").text(mask(result)).attr("cache", result);
		$(".display .operator").text("");
		$(".display .lastDigit").text("").attr("cache", "");;
		return false;
	});
}

function equal(){
	$('.btn a.equal').click(function(){
		
		var op = $(".display .operator").text();
		var lastDigit = $(".display .lastDigit").attr('cache');
		var digit = $(".display .digit").attr('cache');
		
		var result = calculator(lastDigit, digit, op);
		
		$(".display .digit").text(mask(result)).attr("cache", result);
		$(".display .operator").text("");
		$(".display .lastDigit").text("").attr('cache', "");
		return false;
	});
}


function negative(){
	$('.btn a.negative').click(function(){
		var digit = $(".display .digit").attr('cache');
		
		digit = calculator(digit, "-1", "*");
		
		
		$(".display .digit").attr('cache', digit).text(mask(digit));
		
		return false;
	});
}

function memoryCopy(){
	$('.btn a.mem').click(function(){
		var op = $(this).attr('op');
		var memNow = $(".memory").text();
		var digit = $(".display .digit").attr('cache');
		
		var memNew = calculator(memNow, digit, op);
		
		$(".memory").text(memNew);
		$(".display .digit").attr('cache', "");
		$(".display .mem").text("M");
		return false;
	});
}
function memoryPast(){
	$('.btn a.mem-rec').click(function(){
		var mem = $(".memory").text();	
		var click = $(".memory").attr('cache');
		if(click == "1"){
			$(".memory").text("0").attr('cache', "0");
			$(".display .mem").text("");
		}else{
		$(".display .digit").attr('cache', mem).text(mask(mem));
		$(".memory").attr('cache', "1");
		}
		return false;
	});
	$('.btn a:not(.mem-rec)').click(function(){
		$(".memory").attr('cache', "0");
		return false;
	});
}

function calculator(parc1, parc2, op){
	result =  eval(parc1+op+parc2);
	return result;
	
}
function mask(num){
	
	if(num < 0){
		num = calculator(num, "-1", "*");
		var minus = "-";
	}else{
		var minus = "";
	}
	
	num = num.toString();
	var numArr = num.split(".");
	
	var intArr = numArr[0].split("");
	var intArr = intArr.reverse();
	
	
	var i = 0;
	var dot = 0;
	var int = "";
	while(i < intArr.length){
		int += intArr[i];
		
		dot++;
		if(dot >= 3){
			int += ".";
			dot = 0;
		}
		
		i++
	}
	
	int = int.split("").reverse();
	if(int[0] == "."){
		int[0] = "";
	}
	numArr[0] = int.join("");
	var changed = numArr.join(",");
	
	displaysize();	
	return minus+changed;
}

function displaysize(){
	
var digit = $(".display .digit").text();
	
	var size = digit.length;
	if(size > 12 && size <= 15){
		$(".display .digit").css('font-size', "1.6em");
	}else if(size > 15 && size <= 18){
		$(".display .digit").css('font-size', "1.4em");
	}else if(size > 18 && size <= 21){
		$(".display .digit").css('font-size', "1.2em");
	}else if(size > 21 && size <= 23){
		$(".display .digit").css('font-size', "1em");
	}else if(size > 24){
		$(".display .digit").css('font-size', "0.8em");
	}else if(size <= 12){
		$(".display .digit").css('font-size', "2em");
	}
	
}