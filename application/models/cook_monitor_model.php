<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cook_monitor_model extends CI_Model{
	
	public function demands($user=NULL){
	$this->db->select('d.id, d.created, d.comment, u.firstName,u.lastName, t.name tableName');
	$this->db->where('d.id_property',  $this->session->userdata('id_property'));
	$this->db->where("d.status", 1);
	if($user!=NULL){
		$this->db->where("u.id", $user);
	}
	$this->db->join('user u', 'd.user_fk = u.id');
	$this->db->join('tables t', 'd.table_fk = t.id');
	$this->db->order_by("d.created");
	$query = $this->db->get("demands d");
	return $query;
	}

		
	public function itens_of_demands($id, $type=NULL){
		$this->db->select('*, di.qty qtde, di.id id_di, di.status status_di, i.unid');
		$this->db->where("d.id", $id);
		if($type==NULL){
		$this->db->where("i.type", 0);
		}
		$this->db->join('demands d', 'di.demand_fk = d.id');
		$this->db->join('itens i', 'di.item_fk = i.id');
		$this->db->order_by("d.created");
		$query = $this->db->get("demand_item di");
		return $query;
	}
	
	public function get_status($id){
		$this->db->select('status, startProduction, finishedProduction');
		$this->db->where("id", $id);
		$query = $this->db->get("demand_item")->row();
		$query->status = $query->status == NULL? 0 : $query->status;
		return $query;
	}
	public function update_status_doing($id){
		$this->db->where("id", $id);
		$query = $this->db->update("demand_item", array("status" => "1", "startProduction" => $this->format_date->local_date()));
	}
	public function update_status_done($id){
		$this->db->where("id", $id);
		$query = $this->db->update("demand_item", array("status" => "2", "finishedProduction" => $this->format_date->local_date()));
	}
	
	
	
}
