<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Provider_model extends CI_Model{
	
	public function do_insert($table, $data=NULL){
		if($data!=NULL):
		$this->db->insert($table, $data);
		endif;
	}
	
	public function get_all(){
		return $this->db->get($table);
	}
	
	public function do_update($table, $data=NULL, $condition=NULL){
		if($data!=NULL && $condition!=NULL):
		$this->db->update($table, $data, $condition);
		endif;
	}
	
	public function get_byid($table, $id=NULL){
		if($id!=NULL):
		$this->db->where('id', $id);
		$this->db->limit(1);
		return $this->db->get($table);
		else:
		return FALSE;
		endif;
	}
	
public function do_delete($id, $table, $redirect){
	
		$id = $this->general->url_decode($id);
		$this->db->delete($table, array('id' => $id));
		
		$this->message->message_afected_rows('deleted');
		redirect(base_url($redirect), 'refresh');
	
	}
	
public function do_deleteLot($ids, $table, $redirect){
		
		if($ids != NULL){
			foreach ($ids as $i => $id){
				$ids[$i] = $this->general->url_decode($id);
			}
			$this->db->where_in('id', $ids);
			$this->db->delete($table);
			$this->message->message_afected_rows('deleted');
		}else{
			$this->message->set_message('Nenhum ítem selecionado', 'warning');
		}
		redirect(base_url($redirect), 'refresh');
}
	
	public function get_id_property(){
		$this->db->where('id_property',  $this->session->userdata('id_property'));
	}
	
	
	public function get_with_pagination($base_url, $num_rows, $per_page=10, $segment=3){
		$this->load->library('pagination');
	
		$config['base_url'] = base_url($base_url);
		$config['total_rows'] = $num_rows;
		$config['per_page'] = $per_page;
		$config['full_tag_open'] = '<ul class="pagination pagination-sm">';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = "<li>";
		$config['num_tag_close'] = "</li>";
		$config['cur_tag_open'] = "<li class='active'><a>";
		$config['cur_tag_close'] = "</a></li>";
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['first_link'] = '<span class="glyphicon glyphicon-step-backward"></span>';
		$config['last_link'] = '<span class="glyphicon glyphicon-step-forward"></span>';
		$config['next_link'] = '<span class="glyphicon glyphicon-chevron-right"></span>';
		$config['prev_link'] = '<span class="glyphicon glyphicon-chevron-left"></span>';
			
	
		$this->pagination->initialize($config);
	
		$inicio = $this->uri->segment($segment) != NULL ?$this->uri->segment($segment) : 0;
		$limit=$per_page.', '.$inicio;
		
	
		$this->db->limit($per_page, $inicio);
	
	
	
			
		return  $this->pagination->create_links();
	}
}