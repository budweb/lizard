<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model {
	
//BEGIN:(LOG METHOD)==============================================================================
	
public function log($action ='Ativo', $type = NULL, $timeZone = 'UM3', $daylightSaving = 0, $description ='Usuário utilizou o sistema'){
	
		if($type != 6 && $type != NULL):
			$dt_mysql = $this->format_date->dt_mysql($timeZone, $daylightSaving);
	
			$log['action'] = $action;
			$log['description'] = $description;
			$log['date'] = $dt_mysql;
			$log['id_user'] = $this->session->userdata('id');
	
			if($log['id_user'] != NULL):
				$this->db->insert('log', $log);
			endif;
		endif;
	}
	
//END:(LOG METHOD)=================================================================================
	
	function validate_bd($userName=NULL, $senha=NULL) {
		
			$this->db->where('cpf', $userName)->or_where('email', $userName)->or_where('userName', $userName)->where('password', $senha);
			$get = $this->db->get('user');
    		$get->num_rows == 1;
    		$result = $get->row_array();
    		
    		if(($result['email'] || $result['cpf'] || $result['userName']) != $userName):
    			$this->session->set_flashdata('error_userName', '*Usuário incorreto');
    			redirect(base_url().'login');
    			return FALSE;      		
    		elseif($result['password'] != $senha):
    			$this->session->set_flashdata('error_password', '*Senha incorreta');
    			redirect(base_url().'login');
    			return FALSE; 
    		elseif($result['status'] == 0):
    			$this->session->set_flashdata('error_status', '*Usuário desabilitado');
    			redirect(base_url().'login');
    			return FALSE;
    		else:    		
    			return $result;
    		endif;     		
		}

	public function property_data($id_user){			
		$this->db->select("*");
		$this->db->from("user");
		$this->db->where("user.id", $id_user);
		$this->db->join("property", "property.id = user.id_property");
		$get = $this->db->get();
		$query = $get->row_array();
		return $query;			
	}

	public function get_data_admin_byid($id=NULL){
		if($id!=NULL):
		$this->db->where('id', $id);
		$this->db->limit(1);
		return $this->db->get('admin');
		else:
		return FALSE;
		endif;
	}	
}