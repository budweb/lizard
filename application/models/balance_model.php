<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Balance_model extends CI_Model{

public function get_balance(){
	$this->db->where("id_property", $this->session->userdata('id_property'));
	$this->db->order_by("created");
	$query = $this->db->get("balance");
	return $query;
	}
public function prev_balance($date){
	$this->db->select_sum("value");
	$this->db->where("created <", $date);
	$this->db->where("type >", 0);
	$query = $this->get_balance()->row();
	$inflow = $query->value;

	$this->db->select_sum("value");
	$this->db->where("created <", $date);
	$this->db->where("type", 0);
	$query = $this->get_balance()->row();
	$outflow = $query->value;

	$balance = $inflow - $outflow;
	return $balance;
	}


}