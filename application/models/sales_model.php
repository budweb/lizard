<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sales_model extends CI_Model{

public function get_sales(){
		$this->db->select("u.firstName, u.lastName, u.photo, t.name mesa, d.id, d.finished, d.total");
		$this->db->where("d.id_property", $this->session->userdata('id_property'));
		$this->db->where("d.status", 0);
		$this->db->join("tables t", "t.id = d.table_fk", "left");
		$this->db->join("user u", "u.id = d.user_fk", "left");
		$this->db->order_by("d.finished", "DESC");
		$query = $this->db->get("demands d");

		return $query;
	}
public function get_date_last_register(){
		$this->db->select_max("created");
		$this->db->where("id_property", $this->session->userdata('id_property'));
		$this->db->where("type", 2);
		$query = $this->db->get("balance")->row();

		return $query->created;
	}

	
}
