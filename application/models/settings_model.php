<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings_model extends CI_Model{
	
	public function do_update($data=NULL, $condition=NULL){
		if($data!=NULL && $condition!=NULL):
			$this->db->update('property', $data, $condition);		
		$this->message->message_afected_rows('updated');
		endif;
	}
	
	public function get_byid($table, $id=NULL){
		if($id!=NULL):
		$this->db->where('id', $id);
		$this->db->limit(1);
		return $this->db->get($table);
		else:
		return FALSE;
		endif;
	}
	
	public function check_data($id = NULL){
		if($id!=NULL):
		$this->db->where('id', $id);
		$this->db->limit(1);
		$get = $this->db->get('property');
		$result = $get->row_array();
		return $result;
		else:
		return FALSE;
		endif;
	}	
}