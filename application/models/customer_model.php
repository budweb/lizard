<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customer_model extends CI_Model{
	
	public function do_insert($data=NULL){
		if($data!=NULL):
			$this->db->insert('customer', $data);
			$this->message->message_afected_rows('inserted');
		endif;
	}
	
	public function do_update($data=NULL, $condition=NULL){
		if($data!=NULL && $condition!=NULL):
		$this->db->update('customer', $data, $condition);
		$this->message->message_afected_rows('updated');
		endif;
	}
	
	public function get_byid($id=NULL){
		if($id!=NULL):
		$this->db->where('id', $id);
		$this->db->limit(1);
		return $this->db->get('customer');
		else:
		return FALSE;
		endif;
	}
	
	public function do_delete($id){
	
		$this->db->delete('customer', array('id' => $id));	
		$this->message->message_afected_rows('deleted');	
	}
	
	//Paginação-------------------------------------------------------------------------------
	public function get_user(){
		return $this->db->get('customer');
	}
	
	public function get_with_pagination($base_url, $num_rows, $per_page=10, $segment=3){
		$this->load->library('pagination');
	
		$config['base_url'] = base_url($base_url);
		$config['total_rows'] = $num_rows;
		$config['per_page'] = $per_page;
		$config['full_tag_open'] = '<ul class="pagination pagination-sm">';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = "<li>";
		$config['num_tag_close'] = "</li>";
		$config['cur_tag_open'] = "<li class='active'><a>";
		$config['cur_tag_close'] = "</a></li>";
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['first_link'] = 'Primeiro';
		$config['last_link'] = 'Último';
		$config['next_link'] = '<span class="glyphicon glyphicon-chevron-right"></span>';
		$config['prev_link'] = '<span class="glyphicon glyphicon-chevron-left"></span>';
	
		$this->pagination->initialize($config);
	
		$inicio = $this->uri->segment($segment) != NULL ?$this->uri->segment($segment) : 0;
		$limit=$per_page.', '.$inicio;
	
		$this->db->limit($per_page, $inicio);
			
		return  $this->pagination->create_links();
	}
	//Paginação-------------------------------------------------------------------------------
	
	//BEGIN:(STANDARD METHODS)==============================================================================
	public function log($action='Ativo', $description='Usuário utilizou o sistema'){
	
		$dt_mysql = $this->format_date->dt_mysql($this->session->userdata('timeZone'), $this->session->userdata('daylightSaving'));
	
		$log['action'] = $action;
		$log['description'] = $description;
		$log['date'] = $dt_mysql;
		$log['id_user'] = $this->session->userdata('id');
	
		if($log['id_user'] != NULL):
		$this->db->insert('log', $log);
		endif;
	}
	
	//END:(STANDARD METHODS)=================================================================================
}