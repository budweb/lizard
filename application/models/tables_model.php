<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tables_model extends CI_Model{
	
public function do_insert(){
		$data = elements(array('name','description'), $this->input->post());
		$data['id_property'] = $this->session->userdata('id_property');
		$this->db->insert('tables', $data);
		$this->message->message_afected_rows('inserted');
	}
public function do_update($id){
		$id = $this->general->url_decode($id);
		$data = elements(array('name','description'), $this->input->post());
		$this->db->where('id', $id);
		$this->db->update('tables', $data);
		$this->message->message_afected_rows('updated');
	}
public function get_update($id){
	$id = $this->general->url_decode($id);
	$this->db->where('id', $id);
	$query = $this->db->get('tables');
	return $query->row();
	}

	
}