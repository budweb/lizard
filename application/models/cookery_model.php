<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cookery_model extends CI_Model{
	
//BEGIN:(STANDARD METHODS)==============================================================================
	public function log($action='Ativo', $description='Usuário utilizou o sistema'){
	
		$dt_mysql = $this->format_date->dt_mysql($this->session->userdata('timeZone'), $this->session->userdata('daylightSaving'));
	
		$log['action'] = $action;
		$log['description'] = $description;
		$log['date'] = $dt_mysql;
		$log['id_user'] = $this->session->userdata('id');
	
		if($log['id_user'] != NULL):
		$this->db->insert('log', $log);
		endif;
	}	
//END:(STANDARD METHODS)=================================================================================
	
	public function do_insert($data=NULL){
		if($data!=NULL):
		$this->db->insert('user', $data);
		$this->message->message_afected_rows('inserted');
		endif;
	}
	
	public function get_cook(){
		$this->db->where('type', '5')->where('status', '1')->where('id_property', $this->session->userdata('id_property'));
		return $this->db->get('user');
	}
	
	public function get_byid($id=NULL){
		if($id!=NULL):
		$this->db->where('id', $id);
		$this->db->limit(1);
		return $this->db->get('user');
		else:
		return FALSE;
		endif;
	}
	
	public function do_delete($condition=NULL){
		if($condition!=NULL):
		$this->db->delete('user', $condition);
		$this->message->message_afected_rows('deleted');
		endif;
	}
	
	public function do_delete_selected($ids){
		if($ids != NULL){
			foreach ($ids as $i => $id):
			$ids[$i] = $this->general->url_decode($id);
			$this->db->where('id', $ids[$i]);
			$id_user = $this->db->get('user')->row_array();
	
			$this->db->delete('log', array('id_user' => $id_user['id']));
			$this->db->delete('user', array('id' => $id_user['id']));
			endforeach;
			$this->message->message_afected_rows('deleted');
		}else{
			$this->message->set_message('Nenhum ítem selecionado', 'warning');
		}
	}
	
	public function do_update($data=NULL, $condition=NULL){
		if($data!=NULL && $condition!=NULL):
		$this->db->update('user', $data, $condition);
		$this->message->message_afected_rows('updated');
		endif;
	}
	
	//Limite de contas
	public function check_data($id = NULL){
		if($id!=NULL):
		$this->db->where('id', $id);
		$this->db->limit(1);
		$get = $this->db->get('user');
		$result = $get->row_array();
		return $result;
		else:
		return FALSE;
		endif;
	}
	
	public function get_all_account($type){
		$this->db->where('type', $type)->where('id_property', $this->session->userdata('id_property'))->where('status', '1');
		return $this->db->get('user');
	}
	
	public function get_num_account_byid($id=NULL){
		if($id!=NULL):
		$this->db->where('id', $id);
		$this->db->limit(1);
		return $this->db->get('property');
		else:
		return FALSE;
		endif;
	}
}