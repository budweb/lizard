<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Property_model extends CI_Model{
	
//BEGIN:(CUSTOMIZE METHODS)==============================================================================
	
	public function get_with_pagination2($table, $base_url, $value=NULL, $per_page=10, $segment=3){
		$this->load->library('pagination');
	
		$value['select'] = @$value['select'] == NULL ? '*' : $value['select'];
		$this->db->select($value['select']);
	
		$this->db->from($table);
	
		if(@$value['like'] != NULL){
			foreach ($value['like'] as $campo => $val){
				$this->db->like($campo, $val);
			}
		}
		if(@$value['where'] != NULL){
			foreach ($value['where'] as $campo => $val){
				$this->db->where($campo, $val);
			}
		}
		if(@$value['join'] != NULL){
			foreach ($value['join'] as $campo){
				$c = explode(",", $campo);
				//$campo[2] = @$campo[2] == NULL ? "INNER" : $campo[1];
				$this->db->join($c[0], $c[1], $c[2]);
			}
		}
		if(@$value['property'] != NULL){
			$this->db->where(@$value['property'][0], @$value['property'][1]);
		}else{
			$this->db->where('id_property', $this->session->userdata('id_property'));
		}
		$q = $this->db->get();
			
		$config['base_url'] = base_url($base_url);
		$config['total_rows'] = $q->num_rows();
		$config['per_page'] = $per_page;
		$config['full_tag_open'] = '<ul class="pagination pagination-sm">';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = "<li>";
		$config['num_tag_close'] = "</li>";
		$config['cur_tag_open'] = "<li class='active'><a>";
		$config['cur_tag_close'] = "</a></li>";
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['first_link'] = 'Primeiro';
		$config['last_link'] = 'Último';
		$config['next_link'] = '<span class="glyphicon glyphicon-chevron-right"></span>';
		$config['prev_link'] = '<span class="glyphicon glyphicon-chevron-left"></span>';
	
		$value['select'] = @$value['select'] == NULL ? '*' : $value['select'];
		$this->db->select($value['select']);
	
		$this->db->from($table);
	
		if(@$value['like'] != NULL){
			$i=0;
			foreach ($value['like'] as $campo => $val){
				if($i==0){$this->db->like($campo, $val);}
				else{$this->db->or_like($campo, $val);};
			}
		}
		if(@$value['where'] != NULL){
			foreach ($value['where'] as $campo => $val){
				$this->db->where($campo, $val);
			}
		}
		if(@$value['join'] != NULL){
			foreach ($value['join'] as $campo){
				$c = explode(",", $campo);
				//$campo[2] = @$campo[2] == NULL ? "INNER" : $campo[1];
				$this->db->join($c[0], $c[1], $c[2]);
			}
		}
		if(@$value['order_by'] != NULL){
			$this->db->order_by(@$value["order_by"]);
		}
	
		if(@$value['property'] != NULL){
			$this->db->where($value['property'][0], $value['property'][1]);
		}else{
			$this->db->where('id_property', $this->session->userdata('id_property'));
		}
	
		$inicio = $this->uri->segment($segment) != NULL ?$this->uri->segment($segment) : 0;
		$this->db->limit($per_page, $inicio);
		$q = $this->db->get();
	
	
		$this->pagination->initialize($config);
			
		return array('result' =>$q, 'pagination' => $this->pagination->create_links());
	}
	
	public function get_with_pagination($base_url, $num_rows, $per_page=10, $segment=3){
		$this->load->library('pagination');
	
		$config['base_url'] = base_url($base_url);
		$config['total_rows'] = $num_rows;
		$config['per_page'] = $per_page;
		$config['full_tag_open'] = '<ul class="pagination pagination-sm">';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = "<li>";
		$config['num_tag_close'] = "</li>";
		$config['cur_tag_open'] = "<li class='active'><a>";
		$config['cur_tag_close'] = "</a></li>";
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['first_link'] = 'Primeiro';
		$config['last_link'] = 'Último';
		$config['next_link'] = '<span class="glyphicon glyphicon-chevron-right"></span>';
		$config['prev_link'] = '<span class="glyphicon glyphicon-chevron-left"></span>';
	
		$this->pagination->initialize($config);
	
		$inicio = $this->uri->segment($segment) != NULL ?$this->uri->segment($segment) : 0;
		$limit=$per_page.', '.$inicio;
	
		$this->db->limit($per_page, $inicio);
			
		return  $this->pagination->create_links();
	}
	
	public function check_data($id = NULL){
		if($id!=NULL):
		$this->db->where('id', $id);
		$this->db->limit(1);
		$get = $this->db->get('property');
		$result = $get->row_array();
		return $result;
		else:
		return FALSE;
		endif;
	}
	
	public function get_all(){
		return $this->db->get('property');
	}	
	
	public function do_insert($data=NULL){
		if($data!=NULL):
		$this->db->insert('property', $data);
		$this->message->message_afected_rows('inserted');
		endif;
	}
	
	//Atualiza os dados da propriedade
	//Utilizado no método save para alterar os dados da propriedade
	//e no método setting para alterar as configurações da propriedade
	public function do_update($data=NULL, $condition=NULL){
		if($data!=NULL && $condition!=NULL):
		$this->db->update('property', $data, $condition);
		$this->message->message_afected_rows('updated');
		endif;
	}
	
	//Alterar o status dos usuários que pertencem a property alterada
	//Utilizado no método save alterar dados propriedades
	public function alter_status($status=NULL, $ative=NULL, $condition_user=NULL, $condition_property=NULL){
		
		$this->db->update('user', $status, $condition_user);
		$this->db->update('property', $ative, $condition_property);
		$this->message->message_afected_rows('updated');		
	}
	
	public function get_byid($id=NULL){
		if($id!=NULL):
		$this->db->where('id', $id);
		$this->db->limit(1);
		return $this->db->get('property');
		else:
		return FALSE;
		endif;
	}
	
	public function check_data_user($id = NULL){
		if($id!=NULL):
			$this->db->where('id_property', $id);
			$query = $this->db->get('user');
			return $query;
		else:
			return FALSE;
		endif;
	}
		
	public function do_delete($condition=NULL){
		
		if($condition!=NULL):			
			$this->db->where('id_property', $condition['id']);
			$id_users = $this->db->get('user');
		
			foreach ($id_users->result_array() as $id_user):
				if(file_exists('public/photos/thumbs/'.$id_user['photo']) && $id_user['photo'] != NULL):
					unlink('public/photos/thumbs/'.$id_user['photo']);
				endif;
				$this->db->delete('log', array('id_user' => $id_user['id']));
				$this->db->delete('user', array('id' => $id_user['id']));				
			endforeach;

			//$this->db->delete('log', array('id_property' => $condition['id']));
			$this->db->delete('stockAux', array('id_property' => $condition['id']));
			$this->db->delete('stock', array('id_property' => $condition['id']));			
			$this->db->delete('tables', array('id_property' => $condition['id']));
			$this->db->delete('categories', array('id_property' => $condition['id']));
			$this->db->delete('itens', array('id_property' => $condition['id']));
			$this->db->delete('demands', array('id_property' => $condition['id']));
			$this->db->delete('customer', array('id_property' => $condition['id']));
			$this->db->delete('log', array('id_property' => $condition['id']));
			$this->db->delete('property', $condition);
			$this->message->message_afected_rows('deleted');
		endif;
	}
	
	public function do_delete_selected($ids){
		if($ids != NULL){
			foreach ($ids as $i => $id){
				$ids[$i] = $this->general->url_decode($id);
			}
			$this->db->where('id_property', $ids);
			$this->db->delete('stockAux');
			$this->db->delete('stock');
			$this->db->delete('tables');
			$this->db->delete('categories');
			$this->db->delete('itens');
			$this->db->delete('demands');
			$this->db->delete('customer');
			$this->db->delete('log');
			
			$this->db->where('id', $ids);
			$this->db->delete('property');
			$this->message->message_afected_rows('deleted');
		}else{
			$this->message->set_message('Nenhum ítem selecionado', 'warning');
		}
	}

	public function owner_name($id){
		$this->db->select("*");
		$this->db->from("property");
		$this->db->where("property.id", $id);
		$this->db->join("user", "user.id_property = property.id and user.type = 0");
		$get = $this->db->get();
		$query = $get->row_array();
		return @$query['firstName'].' '.@$query['lastName'];
	}
	
	//Retorna os dados para a configuração da data 
	//Utilizado no método save para criar uma propriedade
	public function get_admin_timezone($id = NULL){
		if($id!=NULL):
		$this->db->where('id', $id);
		$this->db->limit(1);
		$get = $this->db->get('user');
		$result = $get->row_array();
		return $result;
		else:
		return FALSE;
		endif;
	}
	
	public function do_update_admin($data=NULL, $condition=NULL){
		if($data!=NULL && $condition!=NULL):
			$this->db->update('property', $data, $condition);
		endif;
	}

	//Limpar MANUALMENTE a tabela de LOG de todos os usuários da propriedade 
	public function clear_table_log_manual($id_property){
		$this->db->delete('log', array('id_property' => $id_property));		
	}

	//Recuperar número de registros
	public function num_rows_user_log($id_property){
		
		$this->db->where('id_property', $id_property);
		return $this->db->get('log')->num_rows(); 
	}
	
	//Recupera os dados do proprietário para a configuração da limpeza de LOGS
	//Utilizado na View advanced_set.php eno método setting_advanced()  
	public function get_user_byid($id){
		$this->db->where('id_property', $id)->where('type', 0);
		$this->db->limit(1);
		return $this->db->get('user');
	}
	
	//Alterar as configurações avançadas de limpeza de LOG dos usuários
	//Utilizado no método setting_advanced()
	public function do_update_property_config_date($data=NULL, $condition=NULL){
		if($data!=NULL && $condition!=NULL):
		$this->db->update('property', $data, $condition);
		//$this->message->message_afected_rows('updated');
		endif;	
	}
}