<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stock_model extends CI_Model{
	
	public function get_with_pagination($base_url, $num_rows, $per_page=10, $segment=3){
		$this->load->library('pagination');
	
		$config['base_url'] = base_url($base_url);
		$config['total_rows'] = $num_rows;
		$config['per_page'] = $per_page;
		$config['full_tag_open'] = '<ul class="pagination pagination-sm">';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = "<li>";
		$config['num_tag_close'] = "</li>";
		$config['cur_tag_open'] = "<li class='active'><a>";
		$config['cur_tag_close'] = "</a></li>";
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['first_link'] = 'Primeiro';
		$config['last_link'] = 'Último';
		$config['next_link'] = '<span class="glyphicon glyphicon-chevron-right"></span>';
		$config['prev_link'] = '<span class="glyphicon glyphicon-chevron-left"></span>';
	
		$this->pagination->initialize($config);
	
		$inicio = $this->uri->segment($segment) != NULL ?$this->uri->segment($segment) : 0;
		$limit=$per_page.', '.$inicio;
	
		$this->db->limit($per_page, $inicio);
			
		return  $this->pagination->create_links();
	}
	
	public function get_stock(){
		$this->db->where('id_property', $this->session->userdata('id_property'));
		$this->db->order_by("quantity", "asc");
		return $this->db->get('stock');
	}
	
	public function get_stock_aux($id_stock, $limit=NULL){
		
		$this->db->select('S.id Sid, S.name Sname, S.quantity Squantity, S.description Sdescription, S.quantityMin SquantityMin, S.unid Sunid, A.quantity Aquantity, A.observation Aobservation, A.updated Aupdated, A.color Acolor');
		$this->db->from('stock as S');
		$this->db->where('A.id_stock', $id_stock);
		$this->db->join('stockAux as A', 'A.id_stock = S.id');
		
		$this->db->order_by("A.updated", "desc");
		if($limit != NULL):
			$this->db->limit($limit);
		endif;
		return $this->db->get();
	}	
	
	public function get_ids(){
		
		$this->db->select('id');
		$this->db->from('stock');
		return $this->db->get();
	}
	
	/* public function get_itens(){
			$this->db->where('id_property', $this->session->userdata('id_property'))->where('type', 2);
			return $this->db->get('itens');
	} */
	
	public function get_itens_byid($id_iten){
		$this->db->where('id_property', $this->session->userdata('id_property'))->where('type', 2)->where('id', $id_iten);
		return $this->db->get('itens');
	}
	
	public function do_insert($data=NULL){
		if($data!=NULL):
			$lastUpdated = $data['lastUpdated'];
			$quantity = $data['quantity'];
			$this->db->insert('stock', $data);
			
			$id_stock = $this->db->insert_id();
			$data_stock_aux['id_stock'] = $id_stock;
			$data_stock_aux['id_property'] = $this->session->userdata('id_property');
			$data_stock_aux['quantity'] = $quantity;
			$data_stock_aux['added'] = 0;
			$data_stock_aux['remove'] = 0;
			$data_stock_aux['observation'] = "Cadastrado no estoque";
			$data_stock_aux['color'] = "#FFFF00";
			$data_stock_aux['updated'] = $lastUpdated;
			$this->db->insert('stockAux', $data_stock_aux);
			//$this->message->message_afected_rows('inserted');
			redirect('stock/update/'.$this->general->url_encode($id_stock));
		endif;
	}
	
	public function get_update_stock($id){
		$this->db->where('id', $id);
		$this->db->where('id_property', $this->session->userdata('id_property'));
		$query = $this->db->get('stock');
		return $query->row();
	}
	
	public function do_update($data=NULL, $condition=NULL){
		
		if($data!=NULL && $condition!=NULL):
			$this->db->where('id_property', $this->session->userdata('id_property'))->where('id', $condition['id']);
			$var_stock = $this->db->get('stock')->row_array();
			
			$quantity_old = $var_stock['quantity'];
			$quantity_add = $data['quantity']; 			
			
			if($quantity_add >= 0):
				$quantity_final = $quantity_add + $quantity_old;				 
			elseif($quantity_add < 0):
				$quantity_final = $quantity_old + $quantity_add;
			endif;
			
			$quantity_final<0 ? $quantity_final = 0: $quantity_final = $quantity_final;
			
			$lastUpdated = $data['lastUpdated'];
			$id_stock = $condition['id'];
			$data['quantity'] = $quantity_final;
			
			
			$this->db->update('stock', $data, $condition);
			
			$this->db->where('id_property', $this->session->userdata('id_property'))->where('id', $condition['id']);
			$var_stock_new = $this->db->get('stock')->row_array();
			$quantity_new = $var_stock_new['quantity'];
			
			$quantity_alter = $quantity_old - $quantity_new;
			
			$unid_array = array('Unidade(s)','Kg','g', 'l');
			$unid = $data['unid'];
			
			if($quantity_final > $quantity_old):
				$observation = 'Acrescentado '.$quantity_add.' '.$unid_array[$unid];
				$color = '#00FF00';
				$data_stock_aux['added'] = $quantity_add;
				$data_stock_aux['remove'] = 0;
			elseif($quantity_final < $quantity_old):
				$quantity_add = -$quantity_add;
				$observation = 'Descompensado '.$quantity_alter.' '.$unid_array[$unid];
				$color = '#FF0000';
				$data_stock_aux['added'] = 0;
				$data_stock_aux['remove'] = $quantity_alter;
			else:
				$observation = 'SEM ALTERAÇÃO';
				$color = '#FFFFFF';
				$data_stock_aux['added'] = 0;
				$data_stock_aux['remove'] = 0;
			endif;			
			
			$this->input->post('id_itens') == NULL || 0 ? $id_itens = NULL : $id_itens = $this->input->post('id_itens'); 
			$data_stock_aux['id_itens'] = $id_itens;
			$data_stock_aux['id_stock'] = $id_stock;
			$data_stock_aux['id_property'] = $this->session->userdata('id_property');
			$data_stock_aux['quantity'] = $quantity_final;
			$data_stock_aux['observation'] = $observation;
			$data_stock_aux['color'] = $color;
			$data_stock_aux['updated'] = $lastUpdated;
			$this->db->insert('stockAux', $data_stock_aux);
			//$this->message->message_afected_rows('updated');
			redirect('stock/update/'.$this->general->url_encode($condition['id']), 'refresh');
		endif;
	}
	
	public function do_delete($condition=NULL){
	
		if($condition!=NULL):
			$condition_relativeTable = array('id_stock' => $condition['id']);
			$this->db->delete('stockAux', $condition_relativeTable);
			
			$this->db->delete('stock', $condition);
			$this->message->message_afected_rows('deleted');
		endif;
	}
	
	public function do_delete_selected($ids){
		if($ids != NULL){
			foreach ($ids as $i => $id){
				$ids[$i] = $this->general->url_decode($id);
			}
			
			$this->db->where_in('id_stock', $ids);
			$this->db->delete('stockAux');
						
			$this->db->where_in('id', $ids);
			$this->db->delete('stock');
			$this->message->message_afected_rows('deleted');
		}else{
			$this->message->set_message('Nenhum ítem selecionado', 'warning');
		}
	}
	
	public function alter_stock_obs($id, $quantity, $unid){
		$unid_array = array('Unidade(s)','Kg','g', 'l');
		$this->db->where('id', $id);
		$dif = $this->db->get('stock')->row_array();
		$old_quantity = $dif['quantity'];
		if($quantity > $old_quantity):
			$final = $quantity-$old_quantity;
			return array('Acrescentado '.$final.' '.$unid_array[$unid], '#00FF00');
		elseif($quantity == $old_quantity):
			$final = $quantity-$old_quantity;
			return array('SEM ALTERAÇÃO', '#FFFFFF');
		else:
			$final = $old_quantity-$quantity;
			return array('Descompensado '.$final.' '.$unid_array[$unid], '#FF0000');
		endif;
	}
}