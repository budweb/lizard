<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Demands_model extends CI_Model{

public function get_table(){
		$this->db->where("id_property", $this->session->userdata('id_property'));
		$query = $this->db->get("tables");
		return $query;
	}
public function get_table_in_use(){
		$this->db->select("d.table_fk, u.firstName, u.lastName");
		$this->db->where("d.id_property", $this->session->userdata('id_property'));
		$this->db->where("d.status", 1);
		$this->db->join("user u", "u.id = d.user_fk", "left");
		$query = $this->db->get("demands d");
		return $query;
	}
public function get_table_no_use(){
		
		$property = $this->session->userdata('id_property');
		$sql = "SELECT DISTINCT * FROM tables t 
		WHERE NOT EXISTS 
		(SELECT * 
			FROM demands d 
			WHERE t.id = d.table_fk 
			AND d.status = 1) AND t.id_property = $property"; 
		$query = $this->db->query($sql);
		return $query;
	}
public function do_register_new($id_table, $id_garcon=NULL){
		if($id_table != ""):
		$data['table_fk'] = $id_table;
		endif;

		if($id_garcon == NULL):
		$data['user_fk'] = $this->session->userdata('id');
		else:
		$data['user_fk'] =	$id_garcon;
		endif;

		$data['created'] = $this->format_date->local_date();
		$data['status'] = 1;
		$data['id_property'] = $this->session->userdata('id_property');
		$this->db->insert('demands', $data);
		$this->message->message_afected_rows('inserted');
	}
public function my_attendences(){
	$this->db->select("d.id, t.name, u.firstName, u.lastName");
	$this->db->where('d.user_fk', $this->session->userdata('id'));
	$this->db->where('d.status', 1);
	$this->db->from("demands d");
	$this->db->join("tables t", "t.id = d.table_fk");
	$this->db->join("user u", "u.id = d.user_fk");
	$query = $this->db->get();
	return $query;
	}

public function get_categories(){
		$this->db->where("id_property", $this->session->userdata('id_property'));
		$query = $this->db->get("categories");
		return $query;
}

public function get_itens_per_demands($id){

	/* `demands_view` AS select `di`.`demand_fk` AS `demand_fk`,`i`.`id` AS `id_item`,`i`.`cod` AS `cod`,`i`.`nameEcf` AS `nameEcf`,`i`.`price` AS `price` 
		from ((`demand_item` `di` 
		join `demands` `d` on((`di`.`demand_fk` = `d`.`id`))) 
		join `itens` `i` on((`di`.`item_fk` = `i`.`id`))) 
		order by `di`.`id` desc*/
		$this->db->select("di.id, di.demand_fk AS demand_fk, di.qty AS qty, i.id AS id_item, i.cod AS cod, i.nameEcf AS nameEcf, i.price AS price, i.unid As unid");
		$this->db->from("demand_item di");
		$this->db->where('demand_fk', $id);
		$this->db->join("demands d", "di.demand_fk = d.id");
		$this->db->join("itens i", "di.item_fk = i.id");
		$query = $this->db->get();
		return $query;

}

public function get_this_demands($id){
	$this->db->select("d.id, d.created, d.serviceCost, d.comment, t.name, u.firstName, u.lastName");
	$this->db->where("d.id", $id);
	$this->db->from("demands d");
	$this->db->join("tables t", "t.id = d.table_fk");
	$this->db->join("user u", "u.id = d.user_fk");
	$query = $this->db->get();
	return $query;
}
public function get_this_demands_desk($id){
	$this->db->select("d.id, t.name, d.created, d.serviceCost, d.comment, u.firstName, u.lastName");
	$this->db->where("d.id", $id);
	$this->db->from("demands d");
	$this->db->join("user u", "u.id = d.user_fk");
	$this->db->join("tables t", "t.id = d.table_fk", "left");
	$query = $this->db->get();
	return $query;
}
public function get_itens($get=NULL){
	$this->db->select("i.*, c.name category, c.icon, c.color");
	$this->db->where("i.id_property", $this->session->userdata('id_property'));
	if(@$get["cat"] != NULL){
		$get["cat"] = $get["cat"] != NULL ? $this->general->url_decode($get["cat"]):"";
		$this->db->where("category", $get["cat"]);
	}
	if(@$get["start"]==1){$this->db->limit(30);}
	if(@$get["cod"] != NULL){$this->db->where("i.cod", $get["cod"]);}
	if(@$get["name"] != NULL){
		$this->db->where("`i`.`name` LIKE ", "'%".$get['name']."%'", FALSE);
		//$this->db->like("i.name", $get["name"]);
	}
	$this->db->join("categories c", "c.id = i.category");
	$query = $this->db->get("itens i");
	return $query;
}
public function commission_std(){
	$this->db->select("commission");
	$this->db->where("id", $this->session->userdata("id_property"));
	$this->db->from("property");
	$commission = $this->db->get();
	return $commission;
}
// public function get_num_itens_in_demand($id_demand, $id_item){

// 	$this->db->where("demand_fk", $id_demand);
// 	$this->db->where("item_fk", $id_item);
// 	$query = $this->db->get("demand_item");
// 	$numrows = $query->num_rows();

// 	if($numrows == 1){
// 		$row = $query->row();
// 		$result = $row->qty;
// 	}else{
// 		$result = $numrows;

// 	}

// return $result;

// }
	
}
