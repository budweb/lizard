<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model{
	
//BEGIN:(STANDARD METHODS)==============================================================================
		
	public function log($action ='ativo'){
				
		if($this->session->userdata('type') != 6 && $this->session->userdata('type') != NULL):
					
			$dt_mysql = $this->format_date->dt_mysql($this->session->userdata('timeZone'), $this->session->userdata('daylightSaving'));
			$log['date'] = $dt_mysql;
			$log['id_user'] = $this->session->userdata('id');
			
			if($action == 'insert'):		
				$log['action'] = 'Inserido';
				$log['description'] = 'Criou um novo usuário';
			
				if($log['id_user'] != NULL):
					$this->db->insert('log', $log);
				endif;
			endif;
			if($action == 'update'):
				$log['action'] = 'Alterado';
				$log['description'] = 'Alterou um usuário';
				
				if($log['id_user'] != NULL):
					$this->db->insert('log', $log);
				endif;
			endif;
			if($action == 'updateMe'):
				$log['action'] = 'Alterado';
				$log['description'] = 'Alterou a própria conta de usuário';
			
				if($log['id_user'] != NULL):
					$this->db->insert('log', $log);
				endif;
			endif;
			if($action == 'delete'):
				$log['action'] = 'Deletado';
				$log['description'] = 'Deletou um usuário';
			
				if($log['id_user'] != NULL):
					$this->db->insert('log', $log);
				endif;			
			endif;
			if($action == 'deleteAll'):
				$log['action'] = 'Deletou em massa';
				$log['description'] = 'Deletou um ou mais usuários';
				
				if($log['id_user'] != NULL):
					$this->db->insert('log', $log);
				endif;
			endif;
			if($action == 'ativo'):
				$log['action'] = 'Ativo';
				$log['description'] = 'Utilizando o sistema';
				
				if($log['id_user'] != NULL):
					$this->db->insert('log', $log);
				endif;
			endif;
		endif;
	}	
	
//END:(STANDARD METHODS)================================================================================= 

	
//BEGIN:(CUSTOMIZE METHODS)==============================================================================
	
	public function get_with_pagination2($table, $base_url, $value=NULL, $per_page=10, $segment=3){
		$this->load->library('pagination');
	
		$value['select'] = @$value['select'] == NULL ? '*' : $value['select'];
		$this->db->select($value['select']);
	
		$this->db->from($table);
	
		if(@$value['like'] != NULL){
			foreach ($value['like'] as $campo => $val){
				$this->db->like($campo, $val);
			}
		}
		if(@$value['where'] != NULL){
			foreach ($value['where'] as $campo => $val){
				$this->db->where($campo, $val);
			}
		}
		if(@$value['join'] != NULL){
			foreach ($value['join'] as $campo){
				$c = explode(",", $campo);
				//$campo[2] = @$campo[2] == NULL ? "INNER" : $campo[1];
				$this->db->join($c[0], $c[1], $c[2]);
			}
		}
		if(@$value['property'] != NULL){
			$this->db->where(@$value['property'][0], @$value['property'][1]);
		}else{
			$this->db->where('id_property', $this->session->userdata('id_property'));
		}
		$q = $this->db->get();
			
		$config['base_url'] = base_url($base_url);
		$config['total_rows'] = $q->num_rows();
		$config['per_page'] = $per_page;
		$config['full_tag_open'] = '<ul class="pagination pagination-sm">';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = "<li>";
		$config['num_tag_close'] = "</li>";
		$config['cur_tag_open'] = "<li class='active'><a>";
		$config['cur_tag_close'] = "</a></li>";
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['first_link'] = 'Primeiro';
		$config['last_link'] = 'Último';
		$config['next_link'] = '<span class="glyphicon glyphicon-chevron-right"></span>';
		$config['prev_link'] = '<span class="glyphicon glyphicon-chevron-left"></span>';
	
		$value['select'] = @$value['select'] == NULL ? '*' : $value['select'];
		$this->db->select($value['select']);
	
		$this->db->from($table);
	
		if(@$value['like'] != NULL){
			$i=0;
			foreach ($value['like'] as $campo => $val){
				if($i==0){$this->db->like($campo, $val);}
				else{$this->db->or_like($campo, $val);};
			}
		}
		if(@$value['where'] != NULL){
			foreach ($value['where'] as $campo => $val){
				$this->db->where($campo, $val);
			}
		}
		if(@$value['join'] != NULL){
			foreach ($value['join'] as $campo){
				$c = explode(",", $campo);
				//$campo[2] = @$campo[2] == NULL ? "INNER" : $campo[1];
				$this->db->join($c[0], $c[1], $c[2]);
			}
		}
		if(@$value['order_by'] != NULL){
			$this->db->order_by(@$value["order_by"]);
		}
	
		if(@$value['property'] != NULL){
			$this->db->where($value['property'][0], $value['property'][1]);
		}else{
			$this->db->where('id_property', $this->session->userdata('id_property'));
		}
	
		$inicio = $this->uri->segment($segment) != NULL ?$this->uri->segment($segment) : 0;
		$this->db->limit($per_page, $inicio);
		$q = $this->db->get();
	
	
		$this->pagination->initialize($config);
			
		return array('result' =>$q, 'pagination' => $this->pagination->create_links());
	}
	
	public function get_with_pagination($base_url, $num_rows, $per_page=10, $segment=3){
		$this->load->library('pagination');
	
		$config['base_url'] = base_url($base_url);
		$config['total_rows'] = $num_rows;
		$config['per_page'] = $per_page;
		$config['full_tag_open'] = '<ul class="pagination pagination-sm">';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = "<li>";
		$config['num_tag_close'] = "</li>";
		$config['cur_tag_open'] = "<li class='active'><a>";
		$config['cur_tag_close'] = "</a></li>";
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['first_link'] = 'Primeiro';
		$config['last_link'] = 'Último';
		$config['next_link'] = '<span class="glyphicon glyphicon-chevron-right"></span>';
		$config['prev_link'] = '<span class="glyphicon glyphicon-chevron-left"></span>';
	
		$this->pagination->initialize($config);
	
		$inicio = $this->uri->segment($segment) != NULL ?$this->uri->segment($segment) : 0;
		$limit=$per_page.', '.$inicio;
	
		$this->db->limit($per_page, $inicio);
			
		return  $this->pagination->create_links();
	}
	
	public function check_data($id = NULL){
		if($id!=NULL):
		$this->db->where('id', $id);
		$this->db->limit(1);
		$get = $this->db->get('user');
		$result = $get->row_array();
		return $result;
		else:
		return FALSE;
		endif;
	}
	
	public function get_all(){
		return $this->db->get('user');
	}
	
	public function get_all_account($type){
		$this->db->where('type', $type)->where('id_property', $this->session->userdata('id_property'))->where('status', '1');
		return $this->db->get('user');
	}
	
	public function get_num_account_byid($id=NULL){
		if($id!=NULL):
		$this->db->where('id', $id);
		$this->db->limit(1);
		return $this->db->get('property');
		else:
		return FALSE;
		endif;
	}
	
	public function get_user(){
		$this->db->where('id !=', $this->session->userdata('id'))->where('type !=', '0')->where('type !=', '5')->where('type !=', '6')->where('status', '1')->where('id_property', $this->session->userdata('id_property'));
		return $this->db->get('user');
	}
	
	public function get_owner(){
		$this->db->where('type', 0);
		return $this->db->get('user');
	}
	
	public function do_insert($data=NULL){
		
		if($data!=NULL):
		
		$this->db->insert('user', $data);
			
		$id_log = $this->db->insert_id();
		$log['id_user'] = $id_log;
		$log['action'] = 'Usuário criado';
		$log['date'] = $this->format_date->dt_mysql($this->session->userdata('timeZone'), $this->session->userdata('daylightSaving'));;
		$log['id_property'] = $this->session->userdata('id_property');
		$this->db->insert('log', $log);
			
		$this->message->message_afected_rows('inserted');
		endif;		
	}
	
	public function do_insert_admin($data=NULL){
	
		if($data!=NULL):
		$id_property = $data['id_property'];
		$this->db->insert('user', $data);
			
		$id_log = $this->db->insert_id();
		$log['id_user'] = $id_log;
		$log['action'] = 'Usuário criado';
		$log['date'] = $this->format_date->dt_mysql($this->session->userdata('timeZone_admin'), $this->session->userdata('daylightSaving_admin'));;
		$log['id_property'] = $id_property;
		$this->db->insert('log', $log);
			
		$this->message->message_afected_rows('inserted');
		endif;		
	}
	
	public function do_update($data=NULL, $condition=NULL){
		if($data!=NULL && $condition!=NULL):
		$firstName = $data['firstName'];
		$id_user = $condition['id'];
		$this->db->update('user', $data, $condition);
	
		$log['id_user'] = $id_user;
		$log['action'] = 'Alterou usuário '.$firstName;
		$log['date'] = $this->format_date->dt_mysql($this->session->userdata('timeZone'), $this->session->userdata('daylightSaving'));;
		$log['id_property'] = $this->session->userdata('id_property');
		$this->db->insert('log', $log);
	
		$this->message->message_afected_rows('updated');
		endif;
	}
	
	public function do_removePhoto($data, $condition=NULL){	
		$this->db->where('id', $condition['id']);
		$this->db->limit(1);
		$get = $this->db->get('user');
		$thumb = $get->row_array();
		
		if(file_exists('public/photos/thumbs/'.$thumb['photo']) && $thumb['photo'] !=NULL):
			unlink('public/photos/thumbs/'.$thumb['photo']);
			$this->db->update('user', $data, $condition);
		endif;			
	}
	
	public function alter_pass($data=NULL, $condition=NULL){
		if($data!=NULL && $condition!=NULL):
		$id_user = $condition['id'];
		$this->db->update('user', $data, $condition);
	
		$log['id_user'] = $id_user;
		$log['action'] = 'Alterou a própria senha';
		$log['date'] = $this->format_date->dt_mysql($this->session->userdata('timeZone'), $this->session->userdata('daylightSaving'));;
		$log['id_property'] = $this->session->userdata('id_property');
		$this->db->insert('log', $log);
	
		$this->message->message_afected_rows('updated');
		endif;
	}
	
	public function do_update_admin($data=NULL, $condition=NULL){
		if($data!=NULL && $condition!=NULL):
		$id_property = $data['id_property'];
		$id_user = $condition['id'];
		$this->db->update('user', $data, $condition);
		
		$log['id_user'] = $id_user;
		$log['action'] = 'Usuário alterado pelo administrador';
		$log['date'] = $this->format_date->dt_mysql($this->session->userdata('timeZone_admin'), $this->session->userdata('daylightSaving_admin'));;
		$log['id_property'] = $id_property;
		$this->db->insert('log', $log);
		
		$this->message->message_afected_rows('updated');
		endif;
	}
	
	public function get_byid($id=NULL){
		if($id!=NULL):
		$this->db->where('id', $id);
		$this->db->limit(1);
		return $this->db->get('user');
		else:
		return FALSE;
		endif;
	}
	
	public function do_delete($condition=NULL){
		
		if($condition!=NULL):
			$this->db->where('id', $condition['id']);
			$this->db->limit(1);
			$get = $this->db->get('user');
			$thumb = $get->row_array();
		
			if(file_exists('public/photos/thumbs/'.$thumb['photo']) && $thumb['photo'] !=NULL):
				unlink('public/photos/thumbs/'.$thumb['photo']);
			endif;
			$condition_log = array('id_user' => $condition['id']);
			$this->db->delete('log', $condition_log);
			
			$condition_demands = array('user_fk' => $condition['id']);
			$this->db->delete('demands', $condition_demands);
			
			$this->db->delete('user', $condition);
			$this->message->message_afected_rows('deleted');
		endif;
	}
	
	public function do_delete_selected($ids){
		if($ids != NULL){
			foreach ($ids as $i => $id):
				$ids[$i] = $this->general->url_decode($id);
				$this->db->where('id', $ids[$i]);
				$id_user = $this->db->get('user')->row_array();
				
				if(file_exists('public/photos/thumbs/'.$id_user['photo']) && $id_user['photo'] != NULL):
					unlink('public/photos/thumbs/'.$id_user['photo']);
				endif;
				
				//$this->db->where('user_fk', $id_user['id']);
				//$user_demands = $this->db->get('demands')->num_rows();
				//if($user_demands != 0):
					$this->db->delete('demands', array('user_fk' => $id_user['id']));
				//endif;
				
				$this->db->delete('log', array('id_user' => $id_user['id']));
				$this->db->delete('user', array('id' => $id_user['id']));		
				
			endforeach;			
		$this->message->message_afected_rows('deleted');
		}else{
			$this->message->set_message('Nenhum ítem selecionado', 'warning');
		}
	}

	public function property_fantasy_name($id){
		$this->db->select("*");
		$this->db->from("user");
		$this->db->where("user.id_property", $id);
		$this->db->join("property", "property.id = user.id_property");
		$get = $this->db->get();
		$query = $get->row_array();
		return @$query['fantasyName'];
	}
	
	//Validação do vormulário de cadastro do Admim	
	public function get_all_owner($id_property){
		$this->db->where('type', 0)->where('id_property', $id_property)->where('status', '1');
		return $this->db->get('user');
	}

	public function get_user_byid($id_property=NULL){
		if($id_property!=NULL):
		$this->db->where('id_property', $id_property)->where('type', 0);
		$this->db->limit(1);
		return $this->db->get('user')->row_array();
		else:
		return FALSE;
		endif;
	}
	public function get_user_per_id($id){
		$this->db->where('id', $id);
		$user = $this->db->get('user')->row();
		return $user;
	}
	
	public function get_property_byid($id_property=NULL){
		if($id_property!=NULL):
		$this->db->where('id', $id_property);
		$this->db->limit(1);
		return $this->db->get('property')->row_array();
		else:
		return FALSE;
		endif;
	}
	
	public function do_reset_pass($data=NULL, $condition=NULL){
		if($data!=NULL && $condition!=NULL):
		$this->db->update('user', $data, $condition);
		endif;
	}
}