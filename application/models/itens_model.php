<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Itens_model extends CI_Model{
	
	public function get_stock_by_id($id=NULL){
		if($id!=NULL):
			$this->db->where('id_itens', $id);
			$this->db->limit(1);
			return $this->db->get('stock')->row();
		else:
			return FALSE;
		endif;
	}
	
	public function cod_exists($id=NULL){
		if($id != NULL){
		$this->db->where('id !=', $id);
		}
		$this->db->where('id_property', $this->session->userdata('id_property'));
		$this->db->where('cod', $this->input->post()["cod"]);
		$q = $this->db->get('itens');
		
		$num_rows = $q->num_rows();
		
		if($num_rows > 0){
			$this->message->set_message('Este código já está sendo utilizado!', 'warning');
			return false;
		}else{
			return 1;
		}
		
	}
	public function new_cod_search(){
		$this->db->select_max("cod");
		$this->db->where('id_property', $this->session->userdata('id_property'));
		$q = $this->db->get('itens');
		$result = $q->row();
		$result->cod++;

		return $result;
	
		
	
	}
	public function get_update_itens($id){
		$this->db->where('id', $id);
		$this->db->where('id_property', $this->session->userdata('id_property'));
		$query = $this->db->get('itens');
		return $query->row();
	}
	public function do_insert_itens(){
		$data = elements(array('name', 'nameEcf', 'description', 'type', 'unid', 'price', 'cost', 'margin', 'productionTime', 'category'), $this->input->post());
		$data['id_property'] = $this->session->userdata('id_property');
		$data['cod'] = $this->input->post()['cod']!="" ? $this->input->post()['cod'] : $this->new_cod_search()->cod;
		$this->db->insert('itens', $data);
		
		$this->message->message_afected_rows('inserted');

		if($this->input->post('controlAtive') == TRUE):
			$this->do_insert_item_stock();
		else:
			if($this->db->affected_rows()>0):
				$id = $this->db->insert_id();					
				$id = $this->general->url_encode($id);
				redirect(base_url().'itens/update/'.$id, 'refresh');
				$this->new_cod_search();										
			else:
				redirect(base_url().'itens/', 'refresh');
			endif;
		endif;
					
	}
	public function do_update_itens($id){
		$data = elements(array('name', 'nameEcf', 'description', 'type', 'unid', 'price', 'cost', 'margin', 'productionTime', 'category'), $this->input->post());
		$data['cod'] = $this->input->post()['cod']!="" ? $this->input->post()['cod'] : $this->new_cod_search()->cod;
		$this->db->where('id', $id);
		$this->db->update('itens', $data);
		
		$crtAtive = $this->input->post('controlAtive');
		$crtStock = $this->input->post('ctrlStock');
		$type = $this->input->post('type');

		if($type == 0 && $crtStock == FALSE):
			$crtAtive = FALSE;
		elseif($type == 2 && $crtAtive == FALSE):
			$crtStock == FALSE;
		endif;			

		if($crtAtive == TRUE):
			$data_stock['quantityMin'] = $this->input->post('quantityMin');
			$data_stock['controlAtive'] = $this->input->post('controlAtive') == TRUE ? 1 : 0;
			if($this->verfic_stock($id) == 0):
				$this->link_insert_stock($id);				
			else:
				$data_stock['name'] = $this->input->post('name');
				$data_stock['description'] = $this->input->post('description');
				$this->db->where('id_itens', $id);
				$this->db->update('stock', $data_stock);
				$this->message->message_afected_rows('updated');				
			endif;
		else:			
			$condition_del_stock = array('id_itens' => $id);
			$this->db->delete('stockAux', $condition_del_stock);			
			$this->db->delete('stock', $condition_del_stock);
			$this->message->message_afected_rows('deleted');				
		endif;		
	}
	
	public function get(){
		$this->db->select('*, I.id Iid, I.name Iname, C.name Cname');
		$this->db->where('I.id_property', $this->session->userdata('id_property'));
		$this->db->from('itens as I');
		$this->db->join('categories as C', 'C.id = I.category', 'left');
		
		$this->filters();
		$this->db->order_by("category", "asc");
		$query = $this->db->get();
		
		return $query;
	}
	
	public function filters(){

		if(isset($this->input->post()["search"])):
			$search = $this->input->post()["search"];
			$this->session->set_flashdata('search-item', $search);
 			redirect(base_url('itens/'), 'location');
		else:
			if($this->session->flashdata('search-item') != ""):
				$search = $this->session->flashdata('search-item');
				//$this->session->keep_flashdata('search');
				$this->session->set_flashdata('search-item', $search);
			endif;
			
		endif;
		
		
		
		if(@$search != ""):
			$this->db->like('I.name', $search);
			$this->db->or_like('C.name', $search);
		endif;
		//$this->session->keep_flashdata('filter');
	}
	
	
	
/*----	 Categories   ------*/
	
	
	public function do_insert_categories(){
		$data = elements(array('name','description', 'icon', 'color'), $this->input->post());
		$data["id_property"] = $this->session->userdata('id_property');
		$this->db->insert('categories', $data);
		
		$this->message->message_afected_rows('inserted');
	}
	
	public function do_update_categories($id){
		$data = elements(array('name','description', 'icon', 'color'), $this->input->post());
		$this->db->where('id', $id);
		$this->db->update('categories', $data);
		
		$this->message->message_afected_rows('updated');
	}
	public function get_update_categories($id){
		$this->db->where('id', $id);
		$query = $this->db->get('categories');
		$row = $query->row();
		return $row;
	}
	
	public function do_insert_item_stock(){
			$lastUpdated = $this->format_date->dt_mysql($this->session->userdata('timeZone'), $this->session->userdata('daylightSaving'));
			
			$id_itens = $this->db->insert_id();
			$data['quantityMin'] = $this->input->post('quantityMin');
			$data['controlAtive'] = $this->input->post('controlAtive') == TRUE ? 1 : 0;
			$data['quantity'] = 0;					
			$data['name'] = $this->input->post('name');
			$data['unid'] = $this->input->post('unid');
			$data['description'] = $this->input->post('description');
			$data['lastUpdated'] = $lastUpdated;
			$data['id_itens'] = $id_itens;
			$data['id_property'] = $this->session->userdata('id_property');
			$this->db->insert('stock', $data);
			
			$id_stock = $this->db->insert_id();
			$data_stock_aux['id_stock'] = $id_stock;
			$data_stock_aux['id_itens'] = $id_itens;
			$data_stock_aux['id_property'] = $this->session->userdata('id_property');
			$data_stock_aux['quantity'] = 0;
			$data_stock_aux['added'] = 0;
			$data_stock_aux['remove'] = 0;
			$data_stock_aux['observation'] = "Cadastrado no estoque";
			$data_stock_aux['color'] = "#FFFF00";
			$data_stock_aux['updated'] = $lastUpdated;
			$this->db->insert('stockAux', $data_stock_aux);

			$this->message->message_afected_rows('inserted');

			if($this->db->affected_rows()>0):									
				$id_itens = $this->general->url_encode($id_itens);
				redirect(base_url().'itens/update/'.$id_itens, 'refresh');
				$this->new_cod_search();										
			else:
				redirect(base_url().'itens/', 'refresh');
			endif;		
	}

	public function link_insert_stock($id = NULL){
			$lastUpdated = $this->format_date->dt_mysql($this->session->userdata('timeZone'), $this->session->userdata('daylightSaving'));
			
			$data['quantityMin'] = $this->input->post('quantityMin');
			$data['controlAtive'] = $this->input->post('controlAtive') == TRUE ? 1 : 0;
			$data['quantity'] = 0;					
			$data['name'] = $this->input->post('name');
			$data['unid'] = $this->input->post('unid');
			$data['description'] = $this->input->post('description');
			$data['lastUpdated'] = $lastUpdated;
			$data['id_itens'] = $id;
			$data['id_property'] = $this->session->userdata('id_property');
			$this->db->insert('stock', $data);
			
			$id_stock = $this->db->insert_id();
			$data_stock_aux['id_stock'] = $id_stock;
			$data_stock_aux['id_itens'] = $id;
			$data_stock_aux['id_property'] = $this->session->userdata('id_property');
			$data_stock_aux['quantity'] = 0;
			$data_stock_aux['added'] = 0;
			$data_stock_aux['remove'] = 0;
			$data_stock_aux['observation'] = "Cadastrado no estoque";
			$data_stock_aux['color'] = "#FFFF00";
			$data_stock_aux['updated'] = $lastUpdated;
			$this->db->insert('stockAux', $data_stock_aux);	

			$this->message->message_afected_rows('inserted');				
	}

	public function verfic_stock($id = NULL){
		$this->db->where('id_itens', $id);
		$row = $this->db->get('stock')->num_rows();
		return $row;
	}

	public function do_delete_itens($id, $redirect){
	
		$id = $this->general->url_decode($id);

		$tables = array('stockAux', 'stock');
		$this->db->where('id_itens', $id);
		$this->db->delete($tables);

		$this->db->delete('itens', array('id' => $id));
		
		$this->message->message_afected_rows('deleted');
		redirect(base_url($redirect), 'refresh');
	
	}
	
	public function do_deleteLot_itens($ids, $table, $redirect){
		
		if($ids != NULL){
			foreach ($ids as $i => $id){
				$ids[$i] = $this->general->url_decode($id);
			}

			$this->db->where_in('id_itens', $ids);
			$this->db->delete('stock');

			$this->db->where_in('id_itens', $ids);
			$this->db->delete('stockAux');

			$this->db->where_in('id', $ids);
			$this->db->delete($table);			

			$this->message->message_afected_rows('deleted');
		}else{
			$this->message->set_message('Nenhum ítem selecionado', 'warning');
		}
		redirect(base_url($redirect), 'refresh');
	}
	
}