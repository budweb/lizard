<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model{
	
	public function get_config_byid($id=NULL){
		if($id!=NULL):
		$this->db->where('id_user', $id);
		$this->db->limit(1);
		return $this->db->get('admin');
		else:
		return FALSE;
		endif;
	}
	
	public function get_byid($table, $id=NULL){
		if($id!=NULL):
		$this->db->where('id', $id);
		$this->db->limit(1);
		return $this->db->get($table);
		else:
		return FALSE;
		endif;
	}
	
	public function do_update($data=NULL, $condition=NULL){
		if($data!=NULL && $condition!=NULL):
			$this->db->update('user', $data, $condition);
		$this->message->message_afected_rows('updated');
		endif;
	}
	
	public function do_insert($data=NULL){
		if($data!=NULL):
		$this->db->insert('user', $data);
		$this->message->message_afected_rows('inserted');
		endif;
	}
	
	public function do_delete($condition=NULL){
		
		if($condition!=NULL):
			$this->db->where('id', $condition['id']);
			$this->db->limit(1);
			$get = $this->db->get('user');
			$thumb = $get->row_array();
		
			if(file_exists('public/photos/thumbs/'.$thumb['photo']) && $thumb['photo'] !=NULL):
				unlink('public/photos/thumbs/'.$thumb['photo']);
			endif;
			$condition_log = array('id_user' => $condition['id']);
			$this->db->delete('log', $condition_log);
			
			$condition_demands = array('user_fk' => $condition['id']);
			$this->db->delete('demands', $condition_demands);
			
			$this->db->delete('user', $condition);
			$this->message->message_afected_rows('deleted');
		endif;
	}
		
	public function check_data($id = NULL){
		if($id!=NULL):
		$this->db->where('id', $id);
		$this->db->limit(1);
		$get = $this->db->get('user');
		$result = $get->row_array();
		return $result;
		else:
		return FALSE;
		endif;
	}

	public function get_user(){
		$this->db->where('type', '6')->where('id !=', $this->session->userdata('id'));
		return $this->db->get('user');
	}
	
	public function get_with_pagination($base_url, $num_rows, $per_page=10, $segment=3){
		$this->load->library('pagination');
	
		$config['base_url'] = base_url($base_url);
		$config['total_rows'] = $num_rows;
		$config['per_page'] = $per_page;
		$config['full_tag_open'] = '<ul class="pagination pagination-sm">';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = "<li>";
		$config['num_tag_close'] = "</li>";
		$config['cur_tag_open'] = "<li class='active'><a>";
		$config['cur_tag_close'] = "</a></li>";
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['first_link'] = 'Primeiro';
		$config['last_link'] = 'Último';
		$config['next_link'] = '<span class="glyphicon glyphicon-chevron-right"></span>';
		$config['prev_link'] = '<span class="glyphicon glyphicon-chevron-left"></span>';
	
		$this->pagination->initialize($config);
	
		$inicio = $this->uri->segment($segment) != NULL ?$this->uri->segment($segment) : 0;
		$limit=$per_page.', '.$inicio;
	
		$this->db->limit($per_page, $inicio);
			
		return  $this->pagination->create_links();
	}
}
