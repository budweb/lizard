<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {
	
	public function __construct(){
		parent::__construct();		
		$this->session_logged->logged($this->session->userdata('logged'));	
		$this->session_logged->access_permission_user();
		$this->load->model('admin_model', 'admin');		
	}
	
	public function index(){

	$dados = array(
			'titulo' => 'Administrador',
			'page' => 'admin/main',
			'links' => array('admin', 'modules')
	);
	$this->load->view('index', $dados);
	}
	
	public function list_admin(){
		
		$q = $this->admin->get_user();
		$pag = $this->admin->get_with_pagination('admin/list_admin',$q->num_rows(), 2);
		
		$query = $this->admin->get_user();
		
		$dados = array(
				'titulo' => 'Masters',
				'page' => 'admin/list',
				'users' => $query,
				'pagination' => $pag,
				'links' => array('admin', 'modules')
		);
		$this->load->view('index', $dados);
	}
	
	public function add(){
	
		$this->validate();
		$dados = array(	'titulo' => 'Novo master',
				'page' =>'admin/form',
				'scripts' => array('general')
		);
		$this->load->view('index', $dados);
	}

	public function update(){
	
		$this->validate();
		$dados = array(	'titulo' => 'Alterar os dados de ',
				'page' =>'admin/form',
				'scripts' => array('general')
		);
		$this->load->view('index', $dados);
	}
	
	public function setting(){
	
		$this->set();
		
		$dados = array(
				'titulo' => 'Configurações',
				'page' => 'admin/settings/index',
				'scripts' => array('general')
				
		);
		$this->load->view('index', $dados);
	}
	
	public function edit_pass(){
	
		$dados = elements(array('password'), $this->input->post());
		$id = $this->general->url_decode($this->input->post('id'));
		$oldPass = md5($this->input->post('oldPass'));
		$dados['password'] = md5($dados['password']);
	
		$result = $this->admin->check_data($id);
		if($result['password'] != $oldPass):
		$this->session->set_flashdata('oldPass', 'A senha antiga está incorreta');
		current_url();
		else:
		$this->form_validation->set_rules('oldPass', 'SENHA ANTIGA', 'required');
		$this->form_validation->set_rules('password', 'NOVA SENHA', 'required');
		$this->form_validation->set_message('matches', 'As senhas não correspondem');
		$this->form_validation->set_rules('rPass', 'REPITA A SENHA', 'required|matches[password]');
	
		$this->form_validation->set_error_delimiters('<small style="color:#f80d1a;font-size: 0.7em;font-style: italic;">*', '</small>');
		
		if($this->form_validation->run()== TRUE):
		$this->admin->do_update($dados, array('id' => $id));
		//$this->customer->log('Alterado', 'Alterou a sua senha de usuário');
		redirect('admin');
		endif;
		endif;
	
		$dados = array(	'titulo' => 'Alterar Senha',
				'page' =>'admin/editpass',
				'scripts' => array('general')
	
		);
		$this->load->view('index', $dados);
	}
	
	public function my_account(){
		$this->validate();
		$dados = array(
				'titulo' => 'Editar Proprietário - ',
				'page' =>'admin/form',
				'scripts' => array('general')
		);
		$this->load->view('index', $dados);
	}
	
	public function validate(){
	
		$id = $this->general->url_decode($this->input->post('id'));
			
		$this->form_validation->set_rules('firstName', 'NOME', 'required|max_length[50]|ucwords');
		$this->form_validation->set_rules('lastName', 'SOBRENOME', 'max_length[50]|ucwords');
		$this->form_validation->set_rules('ddd', 'DDD', 'required|max_length[10]');
		$this->form_validation->set_rules('phone', 'TELEFONE', 'required|max_length[50]');
		$this->form_validation->set_rules('cpf', 'CPF', 'required|max_length[250]|callback_cpf_unique');
		$this->form_validation->set_rules('email', 'E-MAIL', 'trim|required|max_length[100]|strtolower|valid_email|callback_email_unique');
		if($id == NULL):
		$this->form_validation->set_rules('email', 'E-MAIL', 'trim|required|max_length[100]|strtolower|valid_email|is_unique[user.email]');
		$this->form_validation->set_rules('cpf', 'CPF', 'required|max_length[250]|is_unique[user.cpf]');
		$this->form_validation->set_rules('password', 'SENHA', 'required');
		$this->form_validation->set_message('matches', 'As senhas não correspondem');
		$this->form_validation->set_rules('passwordr', 'REPITA A SENHA', 'required|matches[password]');
		endif;
	
		if($this->form_validation->run()== TRUE):
		$this->save();
		endif;
	}
	
	public function delete(){
	
		if($this->general->url_decode($this->uri->segment(3))>0):
			$this->admin->do_delete(array('id' => $this->general->url_decode($this->uri->segment(3))));
		endif;		
		redirect(base_url().'admin//list_admin', 'refresh');
	}
	
	public function save(){
		$id = $this->general->url_decode($this->input->post('id'));
	
		$dados = elements(array('firstName','lastName', 'email', 'ddd', 'phone', 'cpf'), $this->input->post());
				
	
		$photo = $this->photo_upload();
	
		if($id != NULL):
			if($this->input->post('photo') == ('0' || 0) && $photo == FALSE):
				$result = $this->admin->check_data($id);
				$dados['photo'] = $result['photo'];
			else:
				$result = $this->admin->check_data($id);
				if(file_exists('public/photos/thumbs/'.$result['photo'])):
					if($result['photo'] != NULL):
						unlink('public/photos/thumbs/'.$result['photo']);
					endif;
				endif;
				$dados['photo'] = $photo;
			endif;			
			$this->admin->do_update($dados, array('id' => $id));
			if($this->db->affected_rows()>0):
				if($id == $this->session->userdata('id')):
					redirect('admin/');
				else:
					redirect('admin/list_admin');
				endif;
			else:
				redirect(current_url(), 'refresh');
			endif;
			
		else:
			$dados['type'] = 6;
			$dados['id_property'] = 0;
			$dados['userName'] = NULL;
			$dados['timeZone'] = $this->session->userdata('timeZone_admin');
			$dados['daylightSaving'] = $this->session->userdata('daylightSaving_admin');
			$dados['created'] = $this->format_date->dt_mysql($this->session->userdata('timeZone_admin'), $this->session->userdata('daylightSaving_admin'));	
			$dados['password'] = md5($this->input->post('password'));
			$dados['status'] = 1;		
			$dados['photo'] = $photo;
			$this->admin->do_insert($dados);	
			redirect(base_url('admin/list_admin'), 'refresh');
		endif;
	}
	
	public function set(){
	
		$this->form_validation->set_rules('timezones', 'Fuso Horário', 'required');
		if($this->form_validation->run()== TRUE):
		$dados['timeZone'] = $this->input->post('timezones');
		$dados['daylightSaving'] = $this->input->post('daylightSaving') == TRUE ? 1 : 0;
			
		$this->admin->do_update($dados, array('id' => $this->input->post('id')));
		redirect(base_url().'admin/setting/', 'refresh');
		endif;
		
	}
	
	public function photo_upload(){
	
		$this->load->library('image_lib');
		$this->load->helper('file');
			
		$config['upload_path'] = './public/photos/tmp/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '100';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
		$config['encrypt_name'] = TRUE;
	
		$this->load->library('upload', $config);
		if(!$this->upload->do_upload('photo')):
			$error = $this->upload->display_errors();
			$this->session->set_flashdata('error_photo', $error);
			return FALSE;
		else:
		$file_data = $this->upload->data();
			
		//Criar THUMB
		$config['image_library'] = 'gd2';
		$config['source_image']	= $file_data['full_path'];
		$config['new_image'] = './public/photos/thumbs/';
		$config['create_thumb'] = TRUE;
		$config['maintain_ratio'] = FALSE;
		$config['quality'] = 100;
		$config['width'] = 150;
		$config['height'] = 150;
	
		$this->image_lib->initialize($config);
		$this->image_lib->resize();
		$this->image_lib->clear();
	
		unlink('public/photos/tmp/'.$file_data['file_name']);
		chmod('public/photos/thumbs/'.$file_data['raw_name'].'_thumb'.$file_data['file_ext'], 0777);
	
		$full_path = base_url().'public/photos/thumbs/'.$file_data['raw_name'].'_thumb'.$file_data['file_ext'];
		return 	$file_data['raw_name'].'_thumb'.$file_data['file_ext'];
		endif;
	}
	
	public function cpf_unique() {
	
		$query = $this->db->select('*')->where('id !=', $this->general->url_decode($this->input->post('id')))->where('cpf', $this->input->post('cpf'))->limit(1)->get('user');
		if ($query->num_rows() > 0):
		$this->form_validation->set_message('cpf_unique', 'Esse CPF já está cadastrado');
		return FALSE;
		else:
		return TRUE;
		endif;
	}
	
	public function email_unique() {
	
		$query = $this->db->select('*')->where('id !=', $this->general->url_decode($this->input->post('id')))->where('email', $this->input->post('email'))->limit(1)->get('user');
		if ($query->num_rows() > 0):
		$this->form_validation->set_message('email_unique', 'Esse E-Mail já está cadastrado');
		return FALSE;
		else:
		return TRUE;
		endif;
	}
}