<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->session_logged->logged($this->session->userdata('logged'));
		$this->session_logged->access_permission_admin();
		$this->load->model('user_model', 'user');	
	}
	
	public function index(){
		$this->session_logged->load_mod();
		
		$q = $this->user->get_user();
		$pag = $this->user->get_with_pagination('user/index',$q->num_rows(), 10);

		$query = $this->user->get_user();
		
		$dados = array(
				'titulo' => 'Usuários',
				'page' => 'user/index',
				'users' => $query,
				'pagination' => $pag,
				'scripts' => array('general')
		);		
		$this->load->view('index', $dados);
	}

	public function my_account(){
		$this->validate();
		$dados = array(	
				'titulo' => 'Alterar a minha conta de usuário',
				'page' =>'user/form',
				'scripts' => array('general')				
		);
		$this->load->view('index', $dados);
	}
	
	public function add(){		
		$this->session_logged->load_mod();
		$this->validate();			
		$dados = array(	'titulo' => 'Novo usuário',
						'page' =>'user/form',
						'scripts' => array('general', 'user')						
		);
		$this->load->view('index', $dados);
	}
	
	public function update(){
		$this->session_logged->load_mod();
		if($this->input->post('function') == 'cad_user'):
			$this->validate();
		elseif($this->input->post('function') == 'reset_pass'):
			$this->save();
		endif;
		
		$dados = array(	'titulo' => 'Alterar outra conta de usuário',
				'page' =>'user/form',
				'scripts' => array('general', 'user')
		);
		$this->load->view('index', $dados);
	}	
	
	public function save(){	
		if($this->input->post('function') == 'cad_user'):
			$id = $this->general->url_decode($this->input->post('id'));
				
			$dados = elements(array('firstName','lastName', 'email', 'ddd', 'phone', 'cpf', 'type'), $this->input->post());
						
			$photo = $this->photo_upload();
				
			if($id != NULL):
			
				if($this->input->post('cpf_bd') == $this->input->post('password_bd')):
					$dados['password'] = md5($this->input->post('cpf'));
				endif;
			
				if($photo == FALSE):
					$result = $this->user->check_data($id);
					$resultPhoto = $result['photo'];
					$dados['photo'] = $resultPhoto;				
				else:
					$result = $this->user->check_data($id);
					if(file_exists('public/photos/thumbs/'.$result['photo'])):
						if($result['photo'] != NULL):	
							unlink('public/photos/thumbs/'.$result['photo']);
						endif;				
					endif;
					$dados['photo'] = $photo;
				endif;				
				$this->user->do_update($dados, array('id' => $id));			
				if($this->db->affected_rows()>0):
					if($id == $this->session->userdata('id')):			
						redirect('main');
					else:
						redirect('user');			
					endif;				
				else:				
					redirect(current_url(), 'refresh');
				endif;						
			else:		
				$dados = elements(array('firstName','lastName', 'email', 'ddd', 'phone', 'type', 'cpf'), $this->input->post());
				
				$dt_mysql = $this->format_date->dt_mysql($this->session->userdata('timeZone'), $this->session->userdata('daylightSaving'));
			
				$dados['created'] = $dt_mysql;					
				$dados['password'] = md5($this->input->post('cpf'));
				$dados['status'] = 1;
				$dados['id_property'] = $this->session->userdata('id_property');
				$dados['photo'] = $photo;
				$this->user->do_insert($dados);
				//$this->user->log('insert');
				redirect('user');
			endif;
		elseif($this->input->post('function') == 'reset_pass'):
			
			$id = $this->general->url_decode($this->input->post('id'));
		
			$cpf = $this->user->get_byid($id)->row_array();
		
			$data['password'] = md5($cpf['cpf']);
			$this->user->do_reset_pass($data, array('id' => $id));
			current_url();
		endif;
	}
	
	public function validate(){
		
		$id = $this->general->url_decode($this->input->post('id'));
			
		$this->form_validation->set_rules('firstName', 'NOME', 'required|max_length[50]|ucwords');
		$this->form_validation->set_rules('lastName', 'SOBRENOME', 'max_length[50]|ucwords');
		$this->form_validation->set_rules('ddd', 'DDD', 'required|max_length[10]');
		$this->form_validation->set_rules('phone', 'TELEFONE', 'required|max_length[50]');
		$this->form_validation->set_rules('type', 'CONTA', 'max_length[50]required|callback_type_check');
		$this->form_validation->set_rules('cpf', 'CPF', 'required|max_length[250]|callback_cpf_unique');
		$this->form_validation->set_rules('email', 'E-MAIL', 'trim|required|max_length[100]|strtolower|valid_email|callback_email_unique');
		if($id == NULL):		    
			$this->form_validation->set_rules('email', 'E-MAIL', 'trim|required|max_length[100]|strtolower|valid_email|is_unique[user.email]');
			$this->form_validation->set_rules('cpf', 'CPF', 'required|max_length[250]|is_unique[user.cpf]');
			//$this->form_validation->set_rules('password', 'SENHA', 'required');
			//$this->form_validation->set_message('matches', 'As senhas não correspondem');
			//$this->form_validation->set_rules('passwordr', 'REPITA A SENHA', 'required|matches[password]');
		endif;
		
		$this->form_validation->set_error_delimiters('<small style="color:#f80d1a;font-size: 0.7em;font-style: italic;">*', '</small>');
		
		if($this->form_validation->run()== TRUE):
			$this->save();		
		endif;		
	}
	
	public function delete(){
		$this->session_logged->load_mod();
		if($this->general->url_decode($this->uri->segment(3))>0):
		$this->user->do_delete(array('id' => $this->general->url_decode($this->uri->segment(3)))); 
		endif;
		redirect(base_url().'user/', 'refresh');		
	}

	public function photo_upload(){
		
		$this->load->library('image_lib');
		$this->load->helper('file');
			
		$config['upload_path'] = './public/photos/tmp/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '100';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
		$config['encrypt_name'] = TRUE;

		$this->load->library('upload', $config);
		if(!$this->upload->do_upload('photo')):
    		$error = $this->upload->display_errors();
			$this->session->set_flashdata('error_photo', $error);
			return FALSE;					
		else:
			$file_data = $this->upload->data();
					
			//Criar THUMB
			$config['image_library'] = 'gd2';
			$config['source_image']	= $file_data['full_path'];
			$config['new_image'] = './public/photos/thumbs/';
			$config['create_thumb'] = TRUE;
			$config['maintain_ratio'] = FALSE;
			$config['quality'] = 100;
			$config['width'] = 150;
			$config['height'] = 150;
				
			$this->image_lib->initialize($config);
			$this->image_lib->resize();
			$this->image_lib->clear();
		
			unlink('public/photos/tmp/'.$file_data['file_name']);		
			chmod('public/photos/thumbs/'.$file_data['raw_name'].'_thumb'.$file_data['file_ext'], 0777);
		
			$full_path = base_url().'public/photos/thumbs/'.$file_data['raw_name'].'_thumb'.$file_data['file_ext'];
		return 	$file_data['raw_name'].'_thumb'.$file_data['file_ext'];					
     	endif;
	}
	
	public function remove_photo(){
		$id = $this->general->url_decode($this->uri->segment(3));
		$dados['photo'] = NULL;
		$this->user->do_removePhoto($dados, array('id' => $id));
		if($id == $this->session->userdata("id")):
			redirect('user/my_account/'.$this->general->url_encode($id));				
		else:
			redirect('user/update/'.$this->general->url_encode($id));
		endif;
	}
	
	public function deletens(){
		$this->session_logged->load_mod();
		@$ids = $this->input->post()["checkEdit"];		
		$this->user->do_delete_selected($ids);
		redirect(base_url().'user/', 'refresh');
	}
	
	public function edit_pass(){
		
		$dados = elements(array('password'), $this->input->post());
		$id = $this->general->url_decode($this->input->post('id'));
		$oldPass = md5($this->input->post('oldPass'));
		$dados['password'] = md5($dados['password']);
		
		$result = $this->user->check_data($id);
		if($result['password'] != $oldPass):
			$this->session->set_flashdata('oldPass', '*A senha antiga está incorreta');			
			current_url();
		else:
			$this->form_validation->set_rules('oldPass', 'SENHA ANTIGA', 'required');
			$this->form_validation->set_rules('password', 'NOVA SENHA', 'required');
			$this->form_validation->set_message('matches', 'As senhas não correspondem');
			$this->form_validation->set_rules('rPass', 'REPITA A SENHA', 'required|matches[password]');
			
			$this->form_validation->set_error_delimiters('<small style="color:#f80d1a;font-size: 0.7em;font-style: italic;">*', '</small>');
			
			if($this->form_validation->run()== TRUE):
				$this->user->alter_pass($dados, array('id' => $id));
				//$this->user->log('update');
				redirect(base_url().'main/', 'refresh');
			endif;
		endif;		
		
		$dados = array(	'titulo' => 'Alterar Senha',
				'page' =>'user/editpass',
				'scripts' => array('general')
		
		);
		$this->load->view('index', $dados);
	}
	
	public function type_check(){
		$id = $this->general->url_decode($this->input->post('id'));
		
		$query_user = $this->user->check_data($id);
		$query = $this->user->get_all_account($this->input->post('type'));
		$num_account = $this->user->get_num_account_byid($this->session->userdata('id_property'))->row_array();
		
		if(($query_user['type'] != $this->input->post('type')) || $id == NULL):
			if($this->input->post('type')== NULL):
				$this->form_validation->set_message('type_check', 'Selecione um tipo');
				return FALSE;
			elseif($query->num_rows() >= $num_account['numOwner'] && $this->input->post('type') == 0):
				$this->form_validation->set_message('type_check', 'Limite de contas PROPRIETÁRIO foi atingido');
				return FALSE;
			elseif($query->num_rows() >= $num_account['numAdministrative'] && $this->input->post('type') == 1):
				$this->form_validation->set_message('type_check', 'Limite de contas ADMINISTRATIVO foi atingido');
				return FALSE;
			elseif($query->num_rows() >= $num_account['numMaitre'] && $this->input->post('type') == 2):
				$this->form_validation->set_message('type_check', 'Limite de contas GERENTE foi atingido');
				return FALSE;
			elseif($query->num_rows() >= $num_account['numWaiter'] && $this->input->post('type') == 3):
				$this->form_validation->set_message('type_check', 'Limite de contas GARÇOM foi atingido');
				return FALSE;
			elseif($query->num_rows() >= $num_account['numDesk'] && $this->input->post('type') == 4):
				$this->form_validation->set_message('type_check', 'Limite de contas CAIXA foi atingido');
				return FALSE;			
			else:
				return TRUE;
			endif;
		endif;
	}	

	public function cpf_unique() {
		
		$query = $this->db->select('*')->where('id !=', $this->general->url_decode($this->input->post('id')))->where('cpf', $this->input->post('cpf'))->limit(1)->get('user');
		if ($query->num_rows() > 0):
			$this->form_validation->set_message('cpf_unique', 'Esse CPF já está cadastrado');
			return FALSE;
		else:
			return TRUE;
		endif;	
	}
	
	public function email_unique() {
		
		$query = $this->db->select('*')->where('id !=', $this->general->url_decode($this->input->post('id')))->where('email', $this->input->post('email'))->limit(1)->get('user');
		if ($query->num_rows() > 0):
		$this->form_validation->set_message('email_unique', 'Esse E-Mail já está cadastrado');
		return FALSE;
		else:
		return TRUE;
		endif;
	}
}
