<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_property extends CI_Controller {
	
	public function __construct(){
		parent::__construct();		
		$this->session_logged->logged($this->session->userdata('logged'));
		$this->session_logged->access_permission_user();
		$this->load->model('property_model', 'property');
	}	
	
	//ADMIN - Página de listagem de Propriedades
	
	public function index(){		
		
		$q = $this->property->get_all();
		$pag = $this->property->get_with_pagination('admin_property/index',$q->num_rows(), 10);

		$query = $this->property->get_all();
				
		$dados = array(
				'titulo' => 'Propriedades / Estabelecimentos',
				'page' => 'admin/property/index',
				'propertys' => $query,
				'pagination' => $pag,
				'scripts' => array('general'),
				'links' => array('admin')
		);
		$this->load->view('index', $dados);
	}
		
	//======================================================
	
	//ADMIN - Página de adição e alteração de Propriedades
	
	public function add(){
				
		$this->validate();		
		
		$dados = array(
				'titulo' => 'Nova propriedade',
				'page' => 'admin/property/cad_property',
				'scripts' => array('general'),
				'links' => array('admin', 'modules')
		);
		$this->load->view('index', $dados);
	}
	
	public function update(){
	
		$this->validate();
		
		$dados = array(
				'titulo' => 'Alterar os dados de ',
				'page' => 'admin/property/cad_property',
				'scripts' => array('general'),
				'links' => array('admin')
		);
		$this->load->view('index', $dados);
	}
	
	public function validate(){
		
		$id = $this->general->url_decode($this->input->post('id'));
				
		$this->form_validation->set_rules('name', 'RAZÃO SOCIAL', 'required|max_length[150]|ucwords');
		$this->form_validation->set_rules('fantasyName', 'NOME FANTASIA', 'required|max_length[150]|ucwords');
		$this->form_validation->set_rules('address', 'ENDEREÇO', 'required|max_length[150]|ucwords');
		$this->form_validation->set_rules('district', 'BAIRRO', 'required|max_length[150]|ucwords');
		$this->form_validation->set_rules('zipCode', 'CEP', 'max_length[50]|numeric');
		//$this->form_validation->set_rules('cnpj', 'CNPJ', 'required|max_length[150]|callback_cnpj_unique');
		$this->form_validation->set_rules('ddd', 'DDD', 'required|max_length[10]|numeric');
		$this->form_validation->set_rules('phone', 'TELEFONE', 'required|max_length[50]|numeric');
		$this->form_validation->set_rules('city', 'CIDADE', 'required|max_length[100]');
		$this->form_validation->set_rules('state', 'ESTADO', 'required|max_length[50]');
		$this->form_validation->set_rules('manager', 'RESPONSÁVEL', 'required|max_length[100]');
		$this->form_validation->set_rules('email', 'E-MAIL', 'required|trim|max_length[150]|strtolower|valid_email');
		$this->form_validation->set_rules('webSite', 'HOME PAGE', 'prep_url|max_length[150]');
		//$this->form_validation->set_rules('stateRegistration', 'Insc. Estadual', 'max_length[100]');
		//$this->form_validation->set_rules('municipalRegistration', 'Insc. Municipal', 'max_length[100]');
		$this->form_validation->set_rules('filial', 'Filial', 'required|max_length[10]');
		//$this->form_validation->set_rules('ative', 'Disponibilidade', 'required|numeric');

		$this->form_validation->set_error_delimiters('<small style="color:#f80d1a;font-size: 0.7em;font-style: italic;">*', '</small>');
		
		if($this->form_validation->run() == TRUE):
			$this->save();
		endif;
	}

	public function cnpj_unique() {
	
		$query = $this->db->select('*')->where('id !=', $this->general->url_decode($this->input->post('id')))->where('cnpj', $this->input->post('cnpj'))->limit(1)->get('property');
		if ($query->num_rows() > 0):
		$this->form_validation->set_message('cnpj_unique', 'Esse CNPJ já está cadastrado');
		return FALSE;
		else:
		return TRUE;
		endif;
	}
	
	public function save(){
		
		$id = $this->general->url_decode($this->input->post('id'));
		
		$data = elements(array('name', 'fantasyName', 'address', 'district', 'zipCode', 'cnpj', 'ddd', 'phone', 'city', 'state', 'manager', 'email', 'webSite', 'stateRegistration', 'municipalRegistration', 'filial'), $this->input->post());

		$this->input->post('basic') == TRUE ? $data['basic'] = 1 : $data['basic'] = 0;
		$this->input->post('automaticService') == TRUE ? $data['automaticService'] = 1 : $data['automaticService'] = 0;
		$this->input->post('stock') == TRUE ? $data['stock'] = 1 : $data['stock'] = 0;
		$this->input->post('financialControl') == TRUE ? $data['financialControl'] = 1 : $data['financialControl'] = 0;
		
		if($id == NULL):
			$data_admin = $this->property->get_admin_timezone($this->session->userdata('id'));
			
			$data['ative'] = 1;
			$data['timeZone'] = $data_admin['timeZone'];
			$data['daylightSaving'] = $data_admin['daylightSaving'];
			$data['commission'] = 1;
			$data['redirectAutomatic'] = 0;
			
			$data['numOwner'] = 1;
			$data['numAdministrative'] = 1;
			$data['numMaitre'] = 1;
			$data['numWaiter'] = 1;
			$data['numDesk'] = 1;
			$data['numCook'] = 1;
			
			$dt_mysql = $this->format_date->dt_mysql($data_admin['timeZone'], $data_admin['daylightSaving']);
			$data['created'] = $dt_mysql;
			$data['cleaningTime'] = 7776000;
			$unix = mysql_to_unix($dt_mysql);
			$data['initialCount'] = $unix;
			$data['dateTmp'] = $unix + 7776000;
			$data['tomorrow'] = 1;
			
			$this->property->do_insert($data);		
			redirect(base_url('admin_property/index/'), 'refresh');
		else:
						
			$this->property->do_update($data, array('id' => $id));
			redirect(base_url('admin_property/update/'.$this->general->url_encode($id)), 'refresh');
		endif;
	}
	
	//======================================================
	
	//ADMIN - Remoção de Propriedades
	
	public function delete(){
	
		if($this->general->url_decode($this->uri->segment(3))>0):
		$id_del = $this->general->url_decode($this->uri->segment(3));
		
		$this->property->do_delete(array('id' => $id_del));
		endif;
		redirect(base_url('admin_property/'), 'refresh');
	}
	
	//======================================================
	
	//ADMIN - Remoção em massa de Propriedades
	
	public function deletens(){
		$ids = $this->input->post()["checkEdit"];
		$this->load->model('user_model', 'user');
		$this->property->do_delete_selected($ids);
		//redirect(base_url('admin_property/'), 'refresh');
	}
	
	//======================================================

	//ADMIN - Página de adição e alteração de Propriedades
	
	public function setting(){
		
		$this->set();
		
		$dados = array(
				'titulo' => 'Configurações de ',
				'page' => 'admin/property/setting',
				'scripts' => array('general')
		);
		$this->load->view('index', $dados);	
	}
	
	public function set(){
		
		$id = $this->general->url_decode($this->input->post('id'));
		
		$this->form_validation->set_rules('timezones', 'FUSO', 'required');
		$this->form_validation->set_rules('numAdministrative', 'Administrativo', 'numeric');
		$this->form_validation->set_rules('numMaitre', 'Gerente', 'numeric');
		$this->form_validation->set_rules('numWaiter', 'Garçon', 'numeric');
		$this->form_validation->set_rules('numDesk', 'Caixa', 'numeric');
		$this->form_validation->set_rules('numCook', 'Cozinha', 'numeric');
						
		if($this->form_validation->run() == TRUE):
			$data = elements(array('numAdministrative','numMaitre', 'numWaiter', 'numDesk', 'numCook'), $this->input->post());
			
			$this->input->post('basic') == TRUE ? $data['basic'] = 1 : $data['basic'] = 0;
			$this->input->post('automaticService') == TRUE ? $data['automaticService'] = 1 : $data['automaticService'] = 0;
			$this->input->post('stock') == TRUE ? $data['stock'] = 1 : $data['stock'] = 0;
			$this->input->post('financialControl') == TRUE ? $data['financialControl'] = 1 : $data['financialControl'] = 0;
		
			$data['timeZone'] = $this->input->post('timezones');
			$data['daylightSaving'] = $this->input->post('daylightSaving') == TRUE ? 1 : 0;
			$data['commission'] = $this->input->post('commission')== TRUE ? 1 : 0;
	
			$this->property->do_update($data, array('id' => $id));
			redirect(base_url('admin_property/setting/'.$this->general->url_encode($id)), 'refresh');
		endif;
	}

	public function advanced_set(){
		
		$this->setting_advanced();
		
		$dados = array(
				'titulo' => 'Configurações Avançadas de ',
				'page' => 'admin/property/advanced_set',
				'scripts' => array('general'),
				'links' => array('admin')
		);
		$this->load->view('index', $dados);
	}
	
	public function setting_advanced(){
	
		$id = $this->general->url_decode($this->input->post('id'));
		
		if($this->input->post('clearLog') == 'auto'):
			if($this->input->post('tomorrow')==TRUE):
				$data_config_log = $this->property->get_byid($id)->row_array();			
				$dt_mysql = $this->format_date->dt_mysql($data_config_log['timeZone'], $data_config_log['daylightSaving']);
				$unix = mysql_to_unix($dt_mysql);
				$data_date['dateTmp'] = $unix + $this->input->post('cleaningTime');
				$data_date['cleaningTime'] = $this->input->post('cleaningTime');
				$data_date['tomorrow'] = $this->input->post('tomorrow') == TRUE ? 1 : 0;
				$this->property->do_update_property_config_date($data_date, array('id' => $id));
				current_url();
			else:
				$data_config_log = $this->property->get_byid($id)->row_array();
				$initialCount = $data_config_log['initialCount'];
				$data_date['dateTmp'] = $initialCount + $this->input->post('cleaningTime');
				$data_date['cleaningTime'] = $this->input->post('cleaningTime');
				$data_date['tomorrow'] = $this->input->post('tomorrow') == TRUE ? 1 : 0;
				$this->property->do_update_property_config_date($data_date, array('id' => $id));
				current_url();				
			endif;			
		elseif($this->input->post('clearLog') == 'manual'):
			$id_property = $this->input->post('id_property');
			$this->property->clear_table_log_manual($id_property);
			current_url();
				
		elseif($this->input->post('clearLog') == 'ative'):
			$ative['ative'] = $this->input->post('ative');
			$status['status'] = $this->input->post('ative');
			$this->property->alter_status($status, $ative, array('id_property' => $id), array('id' => $id));
			redirect(base_url('admin_property/advanced_set/'.$this->general->url_encode($id)), 'refresh');
		endif;
	}
}