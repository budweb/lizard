<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Checkout extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->session_logged->logged($this->session->userdata('logged'));
		$this->session_logged->access_permission_admin();
		$this->session_logged->load_mod();
		//$this->session_logged->config_mod_bd('checkout');
				
		//$this->load->model('base_model', 'base');
		
		$this->load->helper('financier');
		$this->load->helper('text');
		
		date_default_timezone_set("America/Fortaleza");
		
	}
	

public function index(){
		$dados = array(
				'titulo' => 'Escolha sua mesa',
				'links' => array('demands'),
				'scripts' => array('general') 
		);
		$this->load->view('template/default/header.php', $dados);
		$this->load->view('demands/select-table', array('titulo' => 'Escolha Mesa'));
		$this->load->view('template/default/footer.php');
	}




	
	

public function command(){
	$this->load->model('demands_model', 'demands_model');
	
	$id_demand = $this->general->url_decode( $this->uri->segment(3) );
	
	$this->table->set_template(array ( 'table_open'  => '<table class="table">' ));
	$this->table->set_heading('Cód <br /> qtde', 'Referência <br /> Preço', 'Subtotal');

	//recebe lista do banco de dados
	$itens = $this->demands_model->get_itens_per_demands($id_demand); //lista de itens

	$this->db->where('demand_fk', $id_demand); //lista de adicionais
	$additionals = $this->db->get('additional');

	$this->db->select("serviceCost"); // habilita os 10% do garçon
	$this->db->where('id', $id_demand);
	$serviceCost = $this->db->get('demands');

	$commandParam = array("itens" => $itens, "additionals" => $additionals, "serviceCost" => $serviceCost);
	$header = array(
				'titulo' => 'Checkout',
				'links' => array('checkout'),
				'scripts' => array('mask', 'checkout') 
		);

	$this->load->view('template/default/header.php', $header);
	$this->load->view('checkout/command', $commandParam);
	$this->load->view('template/default/footer.php');
	

}
public function screen_printer(){


	$header = array(
				'titulo' => 'Tela de impressão',
				'links' => array('checkout'),
				'scripts' => array('mask', 'checkout') 
		);
	$this->load->view('template/default/header.php', $header);
	$this->load->view('checkout/screen_printer');
	$this->load->view('template/default/footer.php');

}
public function printer(){
	$this->load->model('demands_model', 'demands_model');
	$this->load->model('user_model', 'user');
	
	$id_demand = $this->general->url_decode( $this->uri->segment(3) );
	
	$this->table->set_template(array ( 'table_open'  => '<table class="table">' ));

	//recebe lista do banco de dados
	$itens = $this->demands_model->get_itens_per_demands($id_demand); //lista de itens

	$this->db->where('demand_fk', $id_demand); //lista de adicionais
	$additionals = $this->db->get('additional');

	//seleciona o demands
	$this->db->select("id, user_fk, serviceCost, money, finished"); // habilita os 10% do garçon
	$this->db->where('id', $id_demand);
	$demands = $this->db->get('demands');

	//seleciona o property
	$this->db->where("id", $this->session->userdata('id_property'));
	$property = $this->db->get('property');


	$commandParam = array(	"itens" => $itens,
							"additionals" => $additionals,
							"demands" => $demands,
							"property" => $property );
	
	$this->load->view('checkout/print', $commandParam);
	

}

public function finished(){
	$id_demand = $this->general->url_decode($this->input->get("id_demands"));
	$total = $this->input->get("total");
	$paymethod = $this->input->get("paymethod");
	$money = $this->input->get("dinheiro");

	//echo $id_demand." ".$total." ".$paymethod;

	$data = array("total" => $total,
				  "paymethod" => $paymethod,
				  "finished" => $this->format_date->local_date(),
				  "status" => 0,
				  "money" => $money
				);

	$this->db->where("id", $id_demand);
	$this->db->update("demands", $data);
	

	  redirect("checkout/screen_printer/".$this->input->get('id_demands'));


}

}