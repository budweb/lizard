<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->library("background");	
		$this->load->model('login_model', 'login');
	}
	
	public function index(){
		
		$userName = $this->input->post('userName');
		$senha = md5($this->input->post('password'));		
		
		$this->form_validation->set_rules('userName', 'LOGIN', 'required|max_length[100]');
		$this->form_validation->set_rules('password', 'SENHA', 'required|max_length[100]');
		
		if($this->form_validation->run() == TRUE):	
		
			//Retorna os dados do usuário			
			$result = $this->login->validate_bd($userName,$senha);
			
			//Retorna ID da tabela Property
			$property = $this->login->property_data($result['id']);	
			
			if($result):
				//DADOS DO USUÁRIO
				$this->session->set_userdata(array(
						'logged' => TRUE,						
						'id'  => $result['id'],
						'firstName'  => $result['firstName'],
						'lastName' => $result['lastName'],
						'type' => $result['type'],				
						'password' => $result['password'],
						'photo' => $result['photo'],						
						'email' => $result['email'],
						'userName' => $result['userName'],
						'status' => $result['status'],
						'created' => $result['created'],
						'id_property' => $result['id_property'],
						'daylightSaving_admin' => $result['daylightSaving'],
						'timeZone_admin' => $result['timeZone'],						
						'automaticService' => $property['automaticService'],
						'dateTmp' => $property['dateTmp'],												
						'cleaningTime' => $property['cleaningTime'],
						'daylightSaving' => $property['daylightSaving'],
						'timeZone' => $property['timeZone'],
						'basic' => $property['basic'],
						'automaticService' => $property['automaticService'],
						'stock' => $property['stock'],
						'financialControl' => $property['financialControl']
				));	
								
				//Registra o LOG
				//$this->login->log('Login', $result['type'], $property['timeZone'], $property['daylightSaving']);
				
				if($result['type']==6):				
					redirect('admin');				
				elseif($result['type']==5 && $property['redirectAutomatic']==1): 
					redirect('cook_monitor');
				else:
					redirect('main');
				endif;
							
			endif;			
		endif;
		
		$dados = array(
				'titulo' => 'Logar-se',
				'page' => 'login',
				'bg' => $this->background->bg_login("assets/images/bg-login")
		);
		$this->load->view('index', $dados);
	}	
     
     public function logout(){
     	//$this->login->log('Logout', $this->session->userdata('type'), $this->session->userdata('timeZone'), $this->session->userdata('daylightSaving'));
     	$this->session->sess_destroy();     	
     	redirect(base_url().'login');
     }    
}