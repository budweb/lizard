<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Financial extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->session_logged->logged($this->session->userdata('logged'));
		$this->session_logged->access_permission_admin();
		$this->session_logged->load_mod();
							
		//$this->load->model('financial_model', 'financial');		
	}
	
	public function index(){
				
		$dados = array(
				'titulo' => 'Gráficos',
				'page' => 'financial/index',
				'scripts' => array('Chart', 'canvasjs.min', 'financial'),
				'links' => array('financial')
		);		
		$this->load->view('index', $dados);
	}
}