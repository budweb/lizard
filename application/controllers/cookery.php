<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cookery extends CI_Controller {
	
	public function __construct(){
		parent::__construct();		
		$this->session_logged->logged($this->session->userdata('logged'));
		$this->session_logged->access_permission_admin();
		$this->load->model('cookery_model', 'cook');
		$this->session_logged->load_mod();
		//$this->session_logged->config_mod_bd('cookery');
	}
	
	public function index(){
		
		$query = $this->cook->get_cook();
		
		$dados = array(
				'titulo' => 'Contas',
				'page' => 'cookery/index',
				'scripts' => array('general'),
				'cook' => $query
		);
		$this->load->view('index', $dados);			
	}
	
	public function add(){
		
		$data['firstName'] = $this->input->post('userName');
		$data['userName'] = $this->input->post('userName');
		$data['password'] = md5($this->input->post('password'));
		$data['type'] = 5;
		$data['status'] = 1;
		$data['id_property'] = $this->session->userdata('id_property');
		$data['photo'] = 'cook01.jpg';		
		
		if($this->input->post('description') == NULL):
		$data['description'] = 'Acesso aos pedidos';
		else:
			$data['description'] = $this->input->post('description');
		endif;
		
		$this->form_validation->set_rules('userName', 'USERNAME', 'required|is_unique[user.userName]');
		$this->form_validation->set_rules('description', 'DESCRIÇÃO', 'max_length[50]');
		$this->form_validation->set_rules('password', 'SENHA', 'required');
		$this->form_validation->set_message('matches', 'As senhas não correspondem');
		$this->form_validation->set_rules('rPass', 'REPITA A SENHA', 'required|matches[password]');
		$this->form_validation->set_rules('type', 'CONTA', 'required|callback_type_check');
		
		$this->form_validation->set_error_delimiters('<small style="color:#f80d1a;font-size: 0.7em;font-style: italic;">*', '</small>');
		
		if($this->form_validation->run()== TRUE):
		$this->cook->do_insert($data);
		redirect(base_url().'cookery/', 'refresh');
		endif;	
		
		$dados = array(
				'titulo' => 'Nova conta',
				'page' => 'cookery/cook_account',
				'scripts' => array('general')
		);
		$this->load->view('index', $dados);
	}
	
	public function type_check(){
		$id = $this->general->url_decode($this->input->post('id'));
	
		$query_user = $this->cook->check_data($id);
		$query = $this->cook->get_all_account($this->input->post('type'));
		$num_account = $this->cook->get_num_account_byid($this->session->userdata('id_property'))->row_array();
	
		if(($query_user['type'] != $this->input->post('type')) || $id == NULL):
			if($query->num_rows() >= $num_account['numCook']):
				$this->form_validation->set_message('type_check', 'Limite de contas COZINHA foi atingido');
				return FALSE;
			else:
				return TRUE;
			endif;
		endif;
	}
	
	public function update(){
		$id = $this->general->url_decode($this->input->post('id'));
		
		$data['userName'] = $this->input->post('userName');
		$data['password'] = md5($this->input->post('password'));
	
		$this->form_validation->set_rules('userName', 'LOGIN', 'required|callback_username_unique');
		$this->form_validation->set_rules('description', 'DESCRIÇÃO', 'max_length[50]');
		$this->form_validation->set_rules('password', 'SENHA', 'required');
		$this->form_validation->set_message('matches', 'As senhas não correspondem');
		$this->form_validation->set_rules('rPass', 'REPITA A SENHA', 'required|matches[password]');
	
		$this->form_validation->set_error_delimiters('<small style="color:#f80d1a;font-size: 0.7em;font-style: italic;">*', '</small>');
			
		if($this->form_validation->run()== TRUE):
		$this->cook->do_update($data, array('id' => $id));
		redirect(base_url().'cookery/', 'refresh');
		endif;
			
		$dados = array(	
				'titulo' => 'Alterar conta',
				'page' =>'cookery/cook_account',
				'scripts' => array('general')	
		);
		$this->load->view('index', $dados);
	}
	
	public function delete(){
	
		if($this->general->url_decode($this->uri->segment(3))>0):
		$this->cook->do_delete(array('id' => $this->general->url_decode($this->uri->segment(3)))); //Utilizando o apelido 'crud'
		endif;
		redirect(base_url().'cookery/', 'refresh');
	}
	
	public function deletens(){		
		@$ids = $this->input->post()["checkEdit"];		
		$this->cook->do_delete_selected($ids);
		redirect(base_url().'cookery/', 'refresh');
	}
	
	public function username_unique() {
		$query = $this->db->select('*')->where('id !=', $this->general->url_decode($this->input->post('id')))->where('userName', $this->input->post('userName'))->limit(1)->get('user');
		if ($query->num_rows() > 0):
		$this->form_validation->set_message('username_unique', 'Esse usuário já existe');
		return FALSE;
		else:
		return TRUE;
		endif;
	}
}
