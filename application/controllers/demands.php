<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Demands extends CI_Controller {

	public function __construct(){
		parent::__construct();

		$this->session_logged->logged($this->session->userdata('logged'));
		$this->session_logged->access_permission_admin();
		$this->session_logged->load_mod();
				
		$this->load->model('base_model', 'base');
		$this->load->model('demands_model', 'model');
		$this->load->helper('financier');
		$this->load->helper('text');

		//$this->load->library('format_date');
		
		
		date_default_timezone_set("America/Fortaleza");
		
	}
	

public function index(){
		$dados = array(
				'titulo' => 'Escolha sua mesa',
				'links' => array('demands'),
				'scripts' => array('general') 
		);
		$this->load->view('template/default/header.php', $dados);
		$this->load->view('demands/select-table', array('titulo' => 'Escolha Mesa'));
		$this->load->view('template/default/footer.php');
	}
public function ajax_select_table(){
		$query = $this->model->get_table();
		$table_in_use = $this->model->get_table_in_use();	
		$dados = array(
				'table_in_use' => $table_in_use,
				'query' => $query
		);
		$this->load->view('demands/ajax-select-table', $dados);			
	}
	
public function register_new(){
	$id_table = $this->uri->segment(3);
	$id_table = $this->general->url_decode($id_table);

	
	$this->model->do_register_new($id_table);
	$id = $this->db->insert_id();
	$id = $this->general->url_encode($id);

	if($id_table != ""){
	redirect(base_url('demands/select_itens/'.$id), 'refresh');
	}else{
		redirect(base_url('demands/select_itens/'.$id.'/desk'), 'refresh');
	}
}

public function register_new_desk(){

	$this->db->select("id");
	$this->db->where("table_fk", NULL);
	$this->db->where("status", 1);

	$get = $this->db->get("demands");
	$demands = $get->row();
	$num_rows = $get->num_rows();

	if($num_rows > 0){
		$id = $this->general->url_encode($demands->id);
		redirect(base_url('demands/select_itens/'.$id.'/desk'), 'refresh');
	}else{
		$this->register_new();
	}
	
}

public function link_table_garcon(){

	
	$this->db->where("id_property", $this->session->userdata('id_property'));
	$this->db->where("type", 3); //tipo garcon
	$garcons = $this->db->get("user");

	$tables_no_use = $this->model->get_table_no_use(0);

	$dados = array(
				'titulo' => 'Ligue o Garçon a uma mesa',
				'links' => array('demands'),
				'scripts' => array('general', 'comanda') 
		);

	$this->load->view('template/default/header.php', $dados);
	$this->load->view('demands/link_table_garcon.php', array("garcons" => $garcons, "tables" => $tables_no_use));
	$this->load->view('template/default/footer.php');
}

public function register_new_table_garcon(){
	$id_garcon = $this->input->post("garcon");
	$id_garcon = $this->general->url_decode($id_garcon);

    $id_table = $this->input->post("table");
	$id_table = $this->general->url_decode($id_table);

	
	$this->model->do_register_new($id_table, $id_garcon);
	$id = $this->db->insert_id();
	$id = $this->general->url_encode($id);

	redirect(base_url('demands/select_itens/'.$id), 'refresh');
	

}
public function myattendances(){
	$query = $this->model->my_attendences();
	$row = $query->row();
	
	$dados = array(
			'titulo' => 'Atendimentos de '.@$row->firstName." ".@$row->lastName,
			'links' => array('demands'),
			'query' => $query
	);
	$this->load->view('template/default/header.php', $dados);
	$this->load->view('demands/my-attendences', $dados);
	$this->load->view('template/default/footer.php');
}

public function select_itens(){


	$commission_std = $this->model->commission_std();
	$commission_std = $commission_std->row();
	
	$categories = $this->model->get_categories();
	
	$id_demands = $this->uri->segment(3);
	$id_demands = $this->general->url_decode($id_demands);

	if($this->session->userdata("type") != 4):
		
		$demands = $this->model->get_this_demands($id_demands);
	else:
		
		$demands = $this->model->get_this_demands_desk($id_demands);
	endif;
	$demands = $demands->row();

	
	$header = array(
		'titulo' => 'Comanda '.str_pad($demands->id, 4, "0", STR_PAD_LEFT),
		'scripts' => array('comanda', 'mask')
	);
	$content = array(
			'titulo' => 'Comanda '.str_pad($demands->id, 5, "0", STR_PAD_LEFT),
			'categories' => $categories,
			'demands' => $demands,
			'comission_std' => $commission_std
	);
	$this->load->view('template/default/header.php', $header);

	
if(is_mobile()){

	$this->load->view('demands/select-itens-mobile', $content);

}else{
	// $this->load->view('demands/select-itens-mobile', $content);

	$this->load->view('demands/select-itens', $content);
}
	
	$this->load->view('template/default/footer.php');
}

public function ajax_select_itens(){
	$categories = $this->model->get_categories();

	$id_cat = $this->uri->segment(3);
	$id_demand = $this->general->url_decode($this->input->get("demand"));
	
	$get = $this->input->get(NULL, TRUE);
	$query = $this->model->get_itens($get);

	$dados = array(
			'titulo' => '',
			'query' => $query,
			'id_demand' => $id_demand
	);
	$this->load->view('demands/ajax-select-itens', $dados);
}

	

public function register_itens(){
	$id_itens = $this->general->url_decode( $this->uri->segment(3) );
	$id_demands = $this->general->url_decode( $this->uri->segment(4) );
	$qty = $this->uri->segment(5);

	//get price
	$this->db->select("price");
	$this->db->where("id", $id_itens);
	$row =  $this->db->get("itens")->row();
	$price = $row->price;



	if(@$qty == NULL){

		//verificar quantidade 
		$this->db->where('item_fk', $id_itens);
		$get = $this->model->get_itens_per_demands($id_demands);

		if($get->num_rows() < 1){
			$qty = 1;

			$data = array(
			'demand_fk' => $id_demands,
			'item_fk' => $id_itens,
			'qty' => $qty,
			'price' => $price
		);

		$this->db->insert('demand_item', $data);

		}else{
			$result = $get->row();
			$qty = $result->qty + 1;

			$this->db->where("id", $result->id);
			$this->db->update('demand_item', array('qty' => $qty, 'price' => $price));
		
		}
	}else{
		
		$data = array(
			'demand_fk' => $id_demands,
			'item_fk' => $id_itens,
			'qty' => $qty,
			'price' => $price
			);
		$this->db->insert('demand_item', $data);

	}

	
	
	
	
}
public function ajax_demand_list(){
	$id_demand = $this->general->url_decode( $this->uri->segment(3) );
	
	$this->table->set_template(array ( 'table_open'  => '<table class="table demand_list">' ));
	$this->table->set_heading('Cód <br /> qtde', 'Referência <br /> Preço', 'Subtotal', '');


	//Recebe dados da tabela view para seleção de demands e itens
	// $this->db->where('demand_fk', $id_demand);
	// $query = $this->db->get('demands_view');
	$query = $this->model->get_itens_per_demands($id_demand);
	
	$this->load->view('demands/ajax-demand-list', array("query" => $query, "id_demand" => $id_demand));

}

function delete_itens_demand(){
	$id = $this->general->url_decode( $this->uri->segment(3) );

	$this->db->where("id", $id);
	$this->db->delete('demand_item');
	
}
function delete_additional(){
	$id = $this->general->url_decode( $this->uri->segment(3) );

	$this->db->where("id", $id);
	$this->db->delete('additional');

}

function add_additional(){
	$demand_fk = $this->general->url_decode( $this->input->get("id") );
	
	$description = $this->input->get("acresc") == "plus" ? "Acresc. ".$this->input->get("descr"):"Desc. ".$this->input->get("descr");
	$price = $this->input->get("acresc") == "plus" ? $this->input->get("value"): -$this->input->get("value");
	$data = array(
		"description" => $description,
		"price" => $price,
		"demand_fk" => $demand_fk
	);
	$this->db->insert('additional', $data);
	echo $this->db->affected_rows();
}

function ajax_service_tax(){
	$id_demand = $this->general->url_decode( $this->uri->segment(3));
	$serviceCost = $this->uri->segment(4);
	$serviceCost = $serviceCost == 0 ? NULL: $serviceCost;
	$this->db->where("id", $id_demand);
	$this->db->update('demands', array('serviceCost' => $serviceCost));
	
	echo $this->db->affected_rows();
}

function ajax_comment(){
	$id = $this->general->url_decode($this->input->get("id"));
	$text = $this->input->get("text");

	$this->db->where("id" , $id);
	$this->db->update("demands", array("comment" => $text));
	

}

}
