<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tables extends CI_Controller {
	
	public function __construct(){
		parent::__construct();		
		$this->session_logged->logged($this->session->userdata('logged'));
		$this->session_logged->access_permission_admin();
		$this->session_logged->load_mod();
		//$this->session_logged->config_mod_bd('tables');
				
		$this->load->model('base_model', 'base');
		$this->load->model('tables_model', 'tables');
		
	}
	
	public function index(){
		
		$this->db->where("id_property", $this->session->userdata('id_property'));
		$this->db->order_by("id ASC, name ASC");
		$query = $this->db->get('tables');
		
		$dados = array(
				'titulo' => 'Mesas',
				'scripts' => array('general'),
				'query' => $query
		);
		$this->load->view('template/default/header', $dados);
		$this->load->view('tables/index', $dados);
		$this->load->view('template/default/footer');		
	}
	
	public function add(){
		if($this->input->post() != ""):
			$this->tables->do_insert();
			redirect(base_url().'tables/', 'refresh');
		endif;
		$dados = array(
				'titulo' => 'Nova Mesa',
				'scripts' => array('general')
		);
		$this->load->view('template/default/header', $dados);
		$this->load->view('tables/form', $dados);
		$this->load->view('template/default/footer');	
	}
	
	public function update(){
		$id = $this->uri->segment(3);
			
		if($this->input->post() != ""):
			$this->tables->do_update($id);
			redirect(base_url('tables/'), 'refresh');		
		endif;
		
		$row = $this->tables->get_update($id);
		$dados = array(
				'titulo' => 'Editar Mesa '.$row->name,
				'scripts' => array('general'),
				'row' => $row
		);
		$this->load->view('template/default/header', $dados);
		$this->load->view('tables/form', $dados);
		$this->load->view('template/default/footer');	
		
	}
	
	public function delete(){
		$this->base->do_delete($this->uri->segment(3), 'tables', 'tables/');
	}
	
	public function deleteLot(){
		$ids = $this->input->post()["checkEdit"];
		$this->base->do_deleteLot($ids, 'tables', 'tables/');
	}
}
