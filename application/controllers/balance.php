<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Balance extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->session_logged->logged($this->session->userdata('logged'));
		$this->session_logged->access_permission_admin();
		$this->session_logged->load_mod();
		$this->load->model('balance_model', 'model');
		$this->load->helper("financier_helper");
		
	}
	

public function index(){

	$date = $this->format_date->local_date("%Y-%m-%d");
	$this->db->where("created >=", $date." 00:00:00");
	$this->db->where("created <=", $date." 23:59:59");
	$query = $this->model->get_balance();

	$balance = $this->model->prev_balance($date." 00:00:00");

		$dados = array(
				'titulo' => 'Movimento diário',
				'links' => array('balance'),
				'scripts' => array('general') 
		);
		$this->load->view('template/default/header.php', $dados);
		$this->load->view('balance/index.php', array('titulo' => 'Movimento diário', 'query' => $query, 'balance' => $balance));
		$this->load->view('template/default/footer.php');
	 }

public function yesterday(){

	$today = $this->format_date->local_date("%Y-%m-%d")." 00:00:00";
	$date_unix = mysql_to_unix($today) - 86400;
	$date = mdate("%Y-%m-%d", $date_unix);
	
	$this->db->where("created >=", $date." 00:00:00");
	$this->db->where("created <=", $date." 23:59:59");
	$query = $this->model->get_balance();

	$balance = $this->model->prev_balance($date." 00:00:00");

		$dados = array(
				'titulo' => 'Movimento diário',
				'links' => array('balance'),
				'scripts' => array('general') 
		);
		$this->load->view('template/default/header.php', $dados);
		$this->load->view('balance/index.php', array('titulo' => 'Movimento diário', 'query' => $query, 'balance' => $balance));
		$this->load->view('template/default/footer.php');
	 }

public function personality(){

	$date = @$this->input->post('date');
	if($date == NULL){
		$date = $this->format_date->local_date("%Y-%m-%d");
	}
	$this->db->where("created >=", $date." 00:00:00");
	$this->db->where("created <=", $date." 23:59:59");
	$query = $this->model->get_balance();

	$balance = $this->model->prev_balance($date." 00:00:00");

		$dados = array(
				'titulo' => 'Movimento diário',
				'links' => array('balance'),
				'scripts' => array('general') 
		);
		$this->load->view('template/default/header.php', $dados);
		$this->load->view('balance/index.php', array('titulo' => 'Movimento diário', 'query' => $query, 'balance' => $balance));
		$this->load->view('template/default/footer.php');

	 }


}