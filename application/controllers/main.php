<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {
	
	public function __construct(){
		parent::__construct();		
		$this->session_logged->logged($this->session->userdata('logged'));
		$this->session_logged->access_permission_admin();		
	}
	
	public function index(){

		$this->clear_table_log();
		
		
		$dados = array(
				'titulo' => 'Main',
				'links' => array("modules"),				
				'page' => 'main',
								
		);
		$this->load->view('index', $dados);			
	}
	
	//Remoção programada de LOG
	public function clear_table_log(){
		if($this->session->userdata('cleaningTime') != 0):
	
			$intervalo = $this->session->userdata('cleaningTime');
			$dt_mysql = $this->format_date->dt_mysql($this->session->userdata('timeZone'), $this->session->userdata('daylightSaving')== 1 ? TRUE : FALSE);
			$data_atual = mysql_to_unix($dt_mysql);
			$data_clear = $this->session->userdata('dateTmp');
	
			if($data_atual >= $data_clear):
			
				$this->db->delete('log', array('id_property' => $this->session->userdata('id_property')));
		
				$newData['initialCount'] = $data_clear;
				$newData['dateTmp'] = ($data_clear + $intervalo);
				$this->db->update('property', $newData, array('id' => $this->session->userdata('id_property')));
			endif;
		endif;
	}
}
