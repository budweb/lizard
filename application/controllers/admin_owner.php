<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_owner extends CI_Controller {
	
	public function __construct(){
		parent::__construct();		
		$this->session_logged->logged($this->session->userdata('logged'));
		$this->session_logged->access_permission_user();
		$this->load->model('user_model', 'user');
	}
		
	//ADMIN - Página de listagem de Proprietários
	
	public function index(){		
		
		$q = $this->user->get_owner();
		$pag = $this->user->get_with_pagination('admin_owner/index',$q->num_rows(), 10);

		$query = $this->user->get_owner();
		$property = $this->db->get('property');
		
		$dados = array(
				'titulo' => 'Proprietários / Responsáveis',
				'page' => 'admin/owner/index',
				'owners' => $query,
				'property' => $property,
				'pagination' => $pag,
				'scripts' => array('general'),
				'links' => array('admin')
		);
		$this->load->view('index', $dados);
	}
		
	//======================================================
	
	//ADMIN - Página de adição e alteração de Proprietários
	
	public function add(){
				
		$this->validate();
		$query = $this->db->get('property');
		
		$dados = array(
				'titulo' => 'Novo proprietário',
				'page' => 'admin/owner/cad_owner',
				'scripts' => array('general'),
				'query' => $query
		);
		$this->load->view('index', $dados);
	}
	
	public function update(){
		if($this->input->post('function') == 'cad_owner'):
			$this->validate();
		elseif($this->input->post('function') == 'reset_pass'):
			$this->save();
		endif;
		$query = $this->db->get('property');
	
		$dados = array(
				'titulo' => 'Alterar o cadastro de ',
				'page' => 'admin/owner/cad_owner',
				'scripts' => array('general'),
				'query' => $query
		);
		$this->load->view('index', $dados);
	}
	
	public function cpf_unique() {
		$query = $this->db->select('*')->where('id !=', $this->general->url_decode($this->input->post('id')))->where('cpf', $this->input->post('cpf'))->limit(1)->get('user');
		if ($query->num_rows() > 0):
		$this->form_validation->set_message('cpf_unique', 'Esse CPF já está cadastrado');
		return FALSE;
		else:
		return TRUE;
		endif;
	}
	
	public function email_unique() {
		$query = $this->db->select('*')->where('id !=', $this->general->url_decode($this->input->post('id')))->where('email', $this->input->post('email'))->limit(1)->get('user');
		if ($query->num_rows() > 0):
		$this->form_validation->set_message('email_unique', 'Esse E-Mail já está cadastrado');
		return FALSE;
		else:
		return TRUE;
		endif;
	}
	
	public function validate(){
		
		$id = $this->general->url_decode($this->input->post('id'));
		if($id == NULL):
			$is_unique_cpf = '|is_unique[user.cpf]';
			$is_unique_email = '|is_unique[user.email]';
		else:
			$is_unique_cpf = '|callback_cpf_unique';
			$is_unique_email = '|callback_email_unique';
		endif;
		
		$this->form_validation->set_rules('firstName', 'NOME', 'required|max_length[50]|ucwords');
		$this->form_validation->set_rules('lastName', 'SOBRENOME', 'max_length[50]|ucwords');
		$this->form_validation->set_rules('ddd', 'DDD', 'required|max_length[10]');
		$this->form_validation->set_rules('phone', 'TELEFONE', 'required|max_length[50]');
		$this->form_validation->set_rules('email', 'E-MAIL', 'trim|required|max_length[100]|strtolower|valid_email'.$is_unique_email);
		$this->form_validation->set_rules('cpf', 'CPF', 'required|max_length[250]'.$is_unique_cpf);
		$this->form_validation->set_rules('id_property', 'PROPRIEDADE', 'required');
		$this->form_validation->set_rules('type', 'CONTA', 'callback_type_check');
		
		$this->form_validation->set_error_delimiters('<small style="color:#f80d1a;font-size: 0.7em;font-style: italic;">*', '</small>');
		
		if($this->form_validation->run() == TRUE):
			$this->save();
		endif;
	}

	public function type_check(){
		$id = $this->general->url_decode($this->input->post('id'));
		
		$query = $this->user->get_all_owner($this->input->post('id_property'));
	
		if($id ==NULL):
			if($query->num_rows() >= 1):
				$this->form_validation->set_message('type_check', 'Limite de contas PROPRIETÁRIO foi atingido');
				return FALSE;		
			else:
				return TRUE;
			endif;
		endif;
	}
	
	public function save(){
		
		if($this->input->post('function') == 'cad_owner'):
		
		$id = $this->general->url_decode($this->input->post('id'));
		
		$data = elements(array('firstName','lastName', 'email', 'ddd', 'phone', 'cpf', 'id_property'), $this->input->post());
				
		if($id == NULL):
			$data_property = $this->user->get_property_byid($this->input->post('id_property'));
			
			$data['status'] = $data_property['ative'];
			$data['type'] = 0;
			$data['password'] = md5($this->input->post('cpf'));
			
			$dt_mysql = $this->format_date->dt_mysql($data_property['timeZone'], $data_property['daylightSaving']);
			$data['created'] = $dt_mysql;			
								
			$this->user->do_insert_admin($data);		
			redirect(base_url('admin_owner/index/'), 'refresh');
		else:
			if($this->input->post('cpf_bd') == $this->input->post('password_bd')):
				$data['password'] = md5($this->input->post('cpf'));
			endif;
		
			$this->user->do_update_admin($data, array('id' => $id));
			redirect(base_url('admin_owner/index/'), 'refresh');
			
		endif;

		elseif($this->input->post('function') == 'reset_pass'):
			
		$id = $this->general->url_decode($this->input->post('id'));
		
		$cpf = $this->user->get_byid($id)->row_array();
		
		$data['password'] = md5($cpf['cpf']);
		$this->user->do_reset_pass($data, array('id' => $id));
		current_url();
		
		endif;
	}
	
	//======================================================
	
	//ADMIN - Remoção de Proprietários
	
	public function delete(){
	
		if($this->general->url_decode($this->uri->segment(3))>0):
			$this->user->do_delete(array('id' => $this->general->url_decode($this->uri->segment(3))));
		endif;
		redirect(base_url('admin_owner/'), 'refresh');
	}
	
	//======================================================
	
	//ADMIN - Remoção em massa de Proprietários
	
	public function deletens(){
		@$ids = $this->input->post()["checkEdit"];		
		$this->user->do_delete_selected($ids);
		redirect(base_url('admin_owner/'), 'refresh');
	}
	
	//======================================================

	public function reset_pass(){		
		
	} 
}