<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends CI_Controller {
	
	public function __construct(){
		parent::__construct();	
		$this->load->model('settings_model', 'settings');
		$this->session_logged->logged($this->session->userdata('logged'));
		$this->session_logged->access_permission_admin();
	}

	public function index(){
		
		$this->set();
		$dados = array(
				'titulo' => 'Configurações',
				'page' => 'settings/index'								
		);
		$this->load->view('index', $dados);
	}
	
	public function set(){
		
		if($this->session->userdata("type") == 0 || $this->session->userdata("type") == 1):	
			$this->form_validation->set_rules('timezones', 'Fuso Horário', 'required');
			if($this->form_validation->run()== TRUE):
				$dados['timeZone'] = $this->input->post('timezones');
				$dados['daylightSaving'] = $this->input->post('daylightSaving') == TRUE ? 1 : 0;
				$dados['commission'] = $this->input->post('commission') == TRUE ? 1 : 0;		
				
				$this->settings->do_update($dados, array('id' => $this->session->userdata('id_property')));
				redirect(base_url().'settings/', 'refresh');
			endif;
		endif;
		
		if($this->session->userdata("type") == 5):
		
		$this->form_validation->set_rules('val', 'val', 'max_length[50]');
			if($this->form_validation->run()== TRUE):
			$this->input->post('redirectAutomatic') == TRUE ? $ra = '1' : $ra = '0';
			$data['redirectAutomatic'] = $ra;				
				
				$this->settings->do_update($data, array('id' => $this->session->userdata('id_property')));
				redirect(base_url().'settings/', 'refresh');
			endif;
		endif;				
	}
}
