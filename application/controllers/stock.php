<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stock extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->session_logged->logged($this->session->userdata('logged'));
		$this->session_logged->access_permission_admin();
		$this->session_logged->load_mod();
							
		$this->load->model('stock_model', 'stock');		
	}
	
	public function index(){
		
		$q = $this->stock->get_stock();
		$pag = $this->stock->get_with_pagination('stock/index',$q->num_rows(), 10);

		$query = $this->stock->get_stock();
		
		$dados = array(
				'titulo' => 'Estoque',
				'page' => 'stock/index',
				'stocks' => $query,
				'pagination' => $pag,
				'search' => $this->session->flashdata('search-item'),				
				'scripts' => array('general')
		);		
		$this->load->view('index', $dados);
	}
	
	public function add(){
		
		$this->validate();
		/* $item_ass = $this->stock->get_itens(); */
		$dados = array(	'titulo' => 'Novo produto',
						'page' =>'stock/form',
						'scripts' => array('stock'),
						/* 'query' => $item_ass, */
						//'scripts' => array('general')
		);
		$this->load->view('index', $dados);
	}
	
	public function update(){
		$this->validate();
		$id = $this->general->url_decode($this->uri->segment(3));
		
		/* $item_ass = $this->stock->get_itens(); */
		$row = $this->stock->get_update_stock($id, 1);
		
		$q = $this->stock->get_stock_aux($id, 1);
				
		$dados = array(	'titulo' => 'Atualização do estoque',
				'page' =>'stock/form',
				'new_quantity' => $this->input->post('quantity'),
				'scripts' => array('stock'),
				/* 'query' => $item_ass, */
				'row' => $row,
				'regStock' => $q,				
		);
		$this->load->view('index', $dados);
	}
	
	public function validate(){
	
		$id = $this->input->post('id');
			
		$this->form_validation->set_rules('name', 'NOME', 'required|max_length[50]|ucwords');
		$this->form_validation->set_rules('quantity', 'QUANTIDADE', 'max_length[10]');
		$this->form_validation->set_rules('unid', 'UNIDADE', 'max_length[10]|numeric');
		$this->form_validation->set_rules('quantityMin', 'QUANTIDADE MÍNIMA', 'max_length[10]|numeric');
			
		$this->form_validation->set_error_delimiters('<small style="color:#f80d1a;font-size: 0.7em;font-style: italic;">*', '</small>');
	
		if($this->form_validation->run()== TRUE):
		$this->save();
		endif;
	}
	
	public function save(){
		
		$id = $this->input->post('id');
		$data = elements(array('name', 'quantity', 'unid', 'description', 'quantityMin', 'id_itens'), $this->input->post());
		
		if($data['quantityMin'] < 0):
			$data['quantityMin'] = 0;
		else:
			$data['quantityMin'] = $this->input->post('quantityMin');
		endif;		
		
		if($id == NULL):
			$data['id_property'] = $this->session->userdata('id_property');
			$data['lastUpdated'] = $this->format_date->dt_mysql($this->session->userdata('timeZone'), $this->session->userdata('daylightSaving'));
			$this->stock->do_insert($data);					
		else:
			$data['lastUpdated'] = $this->format_date->dt_mysql($this->session->userdata('timeZone'), $this->session->userdata('daylightSaving'));
			$this->stock->do_update($data, array('id' => $id));			
		endif;
	}
	
	public function report_stock(){
		$id = $this->general->url_decode($this->uri->segment(3));
		
		$query = $this->stock->get_stock_aux($id);
		
		$dados = array(
				'titulo' => 'Histórico completo',
				'page' => 'stock/report_stock',
				'stocks' => $query,								
				'search' => $this->session->flashdata('search-item'),
				'scripts' => array('stock')
		);
		$this->load->view('index', $dados);
	}
	
	public function delete(){
		if($this->general->url_decode($this->uri->segment(3))>0):
			$this->stock->do_delete(array('id' => $this->general->url_decode($this->uri->segment(3))));
		endif;
		redirect('stock', 'refresh');
	}
	
	public function deletens(){
		@$ids = $this->input->post()["checkEdit"];		
		$this->stock->do_delete_selected($ids);
		redirect(base_url().'stock/', 'refresh');		
	}
}