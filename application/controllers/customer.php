<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//======================================================
//Módulo de Administração de Usuários - MOD: USER

class Customer extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->session_logged->logged($this->session->userdata('logged'));
		$this->session_logged->access_permission_admin();
		$this->session_logged->load_mod();
		//$this->session_logged->config_mod_bd('customer');
		
		$this->load->model('customer_model', 'customer');		
	}
	public function index(){
		
		$q = $this->customer->get_user();
		$pag = $this->customer->get_with_pagination('customer/index',$q->num_rows(), 2);
		
		$query = $this->customer->get_user();
			
		$dados = array(
				'titulo' => 'Clientes',
				'page' => 'customer/index',
				'customers' => $query,
				'pagination' => $pag,
				'scripts' => array('general')
		);
		$this->load->view('index', $dados);
	}
	
	public function add(){
		
		$this->validate();
		$dados = array(	'titulo' => 'Novo cliente',
				'page' =>'customer/form',
				'scripts' => array('general')
		);
		$this->load->view('index', $dados);
	}
	
	public function update(){
	
		$this->validate();
		$dados = array(	'titulo' => 'Alterar os dados de ',
				'page' =>'customer/form',
				'scripts' => array('general')	
		);
		$this->load->view('index', $dados);
	}
	
	public function validate(){
		
		$this->form_validation->set_rules('name', 'NOME', 'required|max_length[100]|ucwords');
		$this->form_validation->set_rules('ddd', 'DDD', 'required|max_length[10]');
		$this->form_validation->set_rules('phone', 'TELEFONE', 'required|max_length[50]');
		$this->form_validation->set_rules('address', 'ENDEREÇO', 'max_length[100]|required');
		$this->form_validation->set_rules('number', 'NÚMERO', 'max_length[10]|required|numeric');
		$this->form_validation->set_rules('district', 'BAIRRO', 'required|max_length[100]');
		$this->form_validation->set_rules('complement', 'COMPLEMENTO', 'max_length[100]');
		$this->form_validation->set_rules('city', 'CIDADE', 'required|max_length[100]');
		$this->form_validation->set_rules('state', 'ESTADO', 'max_length[50]');
		$this->form_validation->set_rules('zipCode', 'CEP', 'max_length[100]');
		$this->form_validation->set_rules('email', 'E-MAIL', 'trim|required|max_length[100]|strtolower|valid_email');
		
		$this->form_validation->set_error_delimiters('<small style="color:#f80d1a;font-size: 0.7em;font-style: italic;">*', '</small>');
		
		if($this->form_validation->run() == TRUE):
			$this->save();
		endif;
	}
	
	public function save(){
		
		$id = $this->general->url_decode($this->input->post('id'));
		
		$data = elements(array('name', 'ddd', 'phone', 'address', 'number', 'district', 'complement', 'city', 'state', 'zipCode', 'email'), $this->input->post());
		
		
		if($id == NULL):
			$data['created'] = $this->format_date->dt_mysql($this->session->userdata('timeZone'), $this->session->userdata('daylightSaving'));
			$data['id_property'] = $this->session->userdata('id_property');
			
			$this->customer->do_insert($data);
			redirect('customer');
		else:
			$this->customer->do_update($data, array('id' => $id));
			redirect('customer');
		endif;
	}
	
	public function delete(){
	
		if($this->general->url_decode($this->uri->segment(3))>0):
			$this->customer->do_delete($this->general->url_decode($this->uri->segment(3)));
		endif;
		redirect(base_url().'customer/', 'refresh');
	}
}