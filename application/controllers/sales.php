<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sales extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->session_logged->logged($this->session->userdata('logged'));
		$this->session_logged->access_permission_admin();
		$this->session_logged->load_mod();
		
		$this->load->helper("financier_helper");
		$this->load->model('sales_model', 'model');		
		
	}
	

public function index(){

	
	$start = $this->input->get("dateStart");
	$end = $this->input->get("dateEnd");

	if($start == NULL and $end == NULL):
		$this->db->where("finished >=", $this->format_date->local_date("%Y:%m:%d")." 00:00:00");
		$this->db->where("finished <=", $this->format_date->local_date("%Y:%m:%d")." 23:59:59");
	endif;
	if($start != NULL):
		$this->db->where("finished >=", $start." 00:00:00");
	endif;
	if($end != NULL):
		$this->db->where("finished <=", $end." 23:59:59");
	endif;

	$query = $this->model->get_sales();

		$dados = array(
				'titulo' => 'Vendas',
				'links' => array('sales'),
				'scripts' => array('general') 
		);
		$this->load->view('template/default/header.php', $dados);
		$this->load->view('sales/index.php', array('titulo' => 'Vendas', 'query' => $query));
		$this->load->view('template/default/footer.php');
	 }

public function register(){

	$last_data = $this->model->get_date_last_register();

	
	$start = $last_data != NULL? $last_data : "01-01-1970 00:00:00";
	$end = $this->format_date->local_date("%Y:%m:%d %H:%i:%s");


		$this->db->where("finished >=", $start);
		$this->db->where("finished <=", $end);
	

	$query = $this->model->get_sales();

		$dados = array(
				'titulo' => 'Vendas',
				'links' => array('sales'),
				'scripts' => array('general') 
		);
		$this->load->view('template/default/header.php', $dados);
		$this->load->view('sales/register.php', array('titulo' => 'Vendas', 'query' => $query, 'date_time' => $end));
		$this->load->view('template/default/footer.php');
	 }

public function register_new_sale(){

	$created = $this->input->get("created");
	$value = $this->input->get("value");
	$id_property = $this->session->userdata('id_property');
	$type = 2;
	$description = "Vendas registradas";
	
	$data = array("description" => $description,
				   "value" => $value,
				   "type" => $type,
				   "created" =>  $created,
				   "id_property" => $id_property);

	if($value > 0){
	$this->db->insert('balance', $data); 
	}
	if($this->db->affected_rows() != 0){
		$this->message->set_message("Registrado com sucesso!", $type="success");
		redirect('/sales/', 'refresh');		  
	}else{
		$this->message->set_message("Vendas não registradas", $type="danger");
		redirect('/sales/', 'refresh');
	}
		
	 }


}