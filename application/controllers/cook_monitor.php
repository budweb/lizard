<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//======================================================
//Módulo de Administração de Usuários - MOD: USER

class Cook_monitor extends CI_Controller {
	
public function __construct(){
		parent::__construct();
		$this->load->helper("financier");
		$this->load->model("cook_monitor_model", "model");
		$this->session_logged->logged($this->session->userdata('logged'));
		$this->session_logged->access_permission_admin();
		
	}
	
public function index(){
	$this->session_logged->load_mod();
	
	
	$dados = array(
			'titulo' => 'Monitor de Cozinha',
			'links' => array('cook_monitor'),
			'scripts' => array("general", "cook_monitor")
	);
	$this->load->view('template/default/header.php', $dados);
	$this->load->view('cook_monitor/index.php', $dados);
	$this->load->view('template/default/footer.php');
}

public function ajax_demands(){

	$query = $this->model->demands();
	$this->load->view('cook_monitor/ajax_demands.php', array('query' => $query));
	
}
public function ajax_local_time(){
	echo mdate("%H:%i:%s");
	
}

public function ajax_status_iten(){
	$id = $this->general->url_decode($this->uri->segment(3));
	$get_status = $this->model->get_status($id);
	
	$status = $get_status->status;
	if($status == 0){
		$this->model->update_status_doing($id);
	}elseif ($status == 1){
		$this->model->update_status_done($id);
	}
	echo $status;
	
}

public function garcon_monitor(){
	$this->session_logged->load_mod();
	$dados = array(
			'titulo' => 'Monitor de Cozinha',
			'links' => array('cook_monitor'),
			'scripts' => array("general", "cook_monitor")
	);
	$this->load->view('template/default/header.php', $dados);
	$this->load->view('cook_monitor/garcon_monitor.php', $dados);
	$this->load->view('template/default/footer.php');

}
public function ajax_garcon_monitor(){
	$query = $this->model->demands($this->session->userdata("id"));
	$this->load->view('cook_monitor/ajax_garcon_monitor.php', array('query' => $query));
	
}


public function desk_monitor(){
	$this->session_logged->load_mod();
	$dados = array(
			'titulo' => 'Monitor do Caixa',
			'links' => array('cook_monitor'),
			'scripts' => array("general", "cook_monitor")
	);
	$this->load->view('template/default/header.php', $dados);
	$this->load->view('cook_monitor/desk_monitor.php', $dados);
	$this->load->view('template/default/footer.php');

}

public function ajax_desk_monitor(){
	$type = $this->input->get("type");
	if($type=="all"){
		$query = $this->model->demands();
	}else{
		$query = $this->model->demands($this->session->userdata("id"));
	}
	
	$this->load->view('cook_monitor/ajax_desk_monitor.php', array('query' => $query));
	
}

}

