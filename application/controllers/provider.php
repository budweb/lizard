<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Provider extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->session_logged->logged($this->session->userdata('logged'));
		$this->session_logged->access_permission_admin();
		$this->load->model('provider_model', 'provider');	
	}
	
	public function index(){
				
		$dados = array(
				'titulo' => 'Usuários',
				'page' => 'provider/index',
				'scripts' => array('general')
		);		
		$this->load->view('index', $dados);
	}
}