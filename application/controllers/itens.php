<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Itens extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->session_logged->logged($this->session->userdata('logged'));		
		$this->session_logged->access_permission_admin();
		$this->session_logged->load_mod();
						
		$this->load->helper("financier");
		$this->load->model('base_model', 'base');
		$this->load->model('itens_model', 'itens');			
	}
	
	public function index(){	
		$pag = $this->base->get_with_pagination('itens/index', $this->itens->get()->num_rows());
		$query = $this->itens->get();
		
		$dados = array(
				'titulo' => 'Itens',
				'links' => array('category'),
				'scripts' => array('general'),
				'search' => $this->session->flashdata('search-item'),
				'query' => $query,
				'pagination' => $pag
		);
		$this->load->view('template/default/header', $dados);
		$this->load->view('itens/index', $dados);
		$this->load->view('template/default/footer');
	}
	

	
	public function add(){
		if($this->input->post() != ""):
			if($this->itens->cod_exists() OR $this->input->post()['cod'] == ""):
				$this->itens->do_insert_itens();				
			
				
			endif;
		endif;
		$id_verific = $this->general->url_decode($this->uri->segment(3));
		$item_stock = $this->itens->get_stock_by_id($id_verific);
		$query = $this->itens->new_cod_search();
		
		//$this->itens->new_cod_search();
		$dados = array(
				'titulo' => 'Novo item',
				'links' => array('category'),
				'scripts' => array('mask', 'itens','general', 'stock'),
				'row' => $query,
				'item_stock' => $item_stock
		);
		$this->load->view('template/default/header', $dados);
		$this->load->view('itens/form', $dados);
		$this->load->view('template/default/footer');
	}
	
	
	public function update(){
		
		$id = $this->general->url_decode($this->uri->segment(3));
		
		if($this->input->post() != ""):
			if($this->itens->cod_exists($id) OR $this->input->post()['cod'] == ""):			
				$this->itens->do_update_itens($id);
			endif;
			redirect(base_url().'itens/update/'.$this->general->url_encode($id), 'refresh');
		
		endif;
		$item_stock = $this->itens->get_stock_by_id($id);
		$row = $this->itens->get_update_itens($id);
		$dados = array(
				'titulo' => 'Editar item '.$row->name,
				'links' => array('category'),
				'scripts' => array('mask', 'itens', 'general', 'stock'),
				'row' => $row,
				'item_stock' => $item_stock
		);
		$this->load->view('template/default/header', $dados);
		$this->load->view('itens/form', $dados);
		$this->load->view('template/default/footer');
		
	}
	
	public function delete(){
		$this->itens->do_delete_itens($this->uri->segment(3), 'itens/');
	}
	
	public function deleteLot(){
		$ids = @$this->input->post()["checkEdit"];
		$this->itens->do_deleteLot_itens($ids, 'itens', 'itens/');
		
	}
	
	
	
	

	
	
	
/* 	--------------  Categorias   ------------------   */

	public function categories(){
		
		$this->db->where('id_property', $this->session->userdata('id_property'));
		$query = $this->db->get('categories');
		$pag = $this->base->get_with_pagination('itens/categories', $query->num_rows());
		
		$this->db->where('id_property', $this->session->userdata('id_property'));
		$query = $this->db->get('categories');
		$dados = array(
				'titulo' => 'Categorias',
				'links' => array('category'),
				'scripts' => array('general'),
				'query' => $query,
				'pagination' => $pag
		);
		$this->load->view('template/default/header', $dados);
		$this->load->view('itens/categories', $dados);
		$this->load->view('template/default/footer');
	}
	
	public function addCategories(){
		// 		Add Category
		if($this->input->post() != ""){
			$this->itens->do_insert_categories();
			
			if($this->db->affected_rows()>0):
				$id = $this->db->insert_id();
				$id = $this->general->url_encode($id);
				redirect(base_url().'itens/updateCategories/'.$id, 'refresh');
			else:
				redirect(base_url().'itens/categories', 'refresh');
			endif;
		}
		// 		end Add Category
		$dados = array(
				'titulo' => 'Nova Categoria',
				'links' => array('category','select_categories'),
				'scripts' => array('itens', 'general')
		);
		$this->load->view('template/default/header', $dados);
		$this->load->view('itens/categories-form', $dados);
		$this->load->view('template/default/footer');
	}
	
	
	
	public function updateCategories(){
		// 		Update category
		$id = $this->uri->segment(3);
		$id = $this->general->url_decode($id);
	
		if($this->input->post() != ""){
			$this->itens->do_update_categories($id);
			redirect(base_url().'itens/updateCategories/'.$this->general->url_encode($id), 'refresh');
		}
		// 		End Update category
	
		$row = $this->itens->get_update_categories($id);
	
		$page = "itens/categories-form";
		//$page =  @$row->id == NULL || @$row->id_property != $this->session->userdata('id_property')? "500.php" : $page;
		$dados = array(
				'titulo' => 'Alterar Categorias',
				'links' => array('category','select_categories'),
				'scripts' => array('itens', 'general'),
				'rowUp' => @$row
		);
	
		$this->load->view('template/default/header', $dados);
		$this->load->view($page, $dados);
		$this->load->view('template/default/footer');
	}
	
	public function catDelete(){
		$this->base->do_delete($this->uri->segment(3), 'categories', 'itens/categories/');
	}
	public function catDeleteLot(){
		$ids = @$this->input->post()["checkEdit"];
		$this->base->do_deleteLot($ids, 'categories', 'itens/categories/');
	}
	
	
	
	
	public function sprint(){
	
		$letras = array("A", "B", "C", "D", "E");
	
		$x = 0;
		foreach($letras as $letra){
	
			$y = 0;
			for($i=1; $i<12; $i++){
				echo ".category-sm-".$letra.$i."{";
				echo "background-position:".$x."px ".$y."px}";
				$y = $y-50;
			}
			echo "<br />";
			$x = $x-75;
		}
	
	
	
	}
}

