<?php 
$id = $this->general->url_decode($this->uri->segment(3));

if($id!=NULL):
$row = $this->admin->get_byid('user', $id)->row();
endif;
?>
<form action="" method="POST" enctype="multipart/form-data" id="form_user">
	<div class="row">
	<fieldset>
		<legend><h2><?php echo $titulo.'<b>'.@$row->firstName.' '.@$row->lastName.'</b>';?></h2></legend>
		<div class="row">
			<div class="col-md-12">
				<?php				 
					control("savebt");
					$this->session->userdata('id') == $id ? control("other", array('label' => 'Cancelar', 'href'=>'admin', 'classIcon'=>'glyphicon glyphicon-th-large')) : control("other", array('label' => 'Listar', 'href'=>'admin/list_admin', 'classIcon'=>'glyphicon glyphicon-th-list'));										
				?>				
			</div>
		</div>
	<div class="col-md-8">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
     				<?php
  						echo form_hidden('id', $this->general->url_encode(@$row->id));
  						echo form_label('Nome'); 
  						echo form_input(array('name'=>'firstName', 'id'=>'firstName', 'placeholder'=>'Nome', 'class'=>'form-control'), set_value('firstName', @$row->firstName), 'autofocus'); 
  						echo form_error("firstName");
  					?>
				</div>
			</div>
	<div class="col-md-6">
		<div class="form-group">			
			<?php 
				echo form_label('Sobrenome');
				echo form_input(array('name'=>'lastName', 'id'=>'lastName', 'placeholder'=>'Sobrenome', 'class'=>'form-control'), set_value('lastName', @$row->lastName));
				echo form_error("lastName");
			?> 			
		</div>
	</div>
		</div>
	
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<?php 
					echo form_label('E-Mail');
					echo form_input(array('name'=>'email', 'id'=>'email', 'placeholder'=>'E-Mail', 'class'=>'form-control'), set_value('email', @$row->email));
					echo form_error("email");
				?>
  			</div>
		</div>
		<div class="col-md-2">
				<div class="form-group">
				<?php 
					echo form_label('DDD');
					echo form_input(array('name'=>'ddd', 'id'=>'ddd', 'placeholder'=>'DDD', 'class'=>'form-control'), set_value('ddd', @$row->ddd));
					echo form_error("ddd");
				?>  
				</div>
		</div>
	<div class="col-md-4">
		<div class="form-group">
			<?php 
				echo form_label('Telefone');
				echo form_input(array('name'=>'phone', 'id'=>'phone', 'placeholder'=>'Telefone', 'class'=>'form-control'), set_value('phone', @$row->phone));
				echo form_error("phone");
			?>			
		</div>
	</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
				<?php 
					echo form_label('CPF');
					echo form_input(array('name'=>'cpf', 'id'=>'cpf', 'placeholder'=>'CPF', 'class'=>'form-control'), set_value('cpf', @$row->cpf));
					echo form_error("cpf");
				?>
			</div>
		</div>
		
	<div class="col-md-3">
		<div class="form-group">
			<?php 
				if($id==NULL):
					echo form_label('Senha');
					echo form_password(array('name'=>'password', 'id'=>'password', 'placeholder'=>'Senha', 'class'=>'form-control'));
					echo form_error("password");
				endif;
			?>
  		</div>
	</div>
		
	<div class="col-md-3">
		<div class="form-group">
			<?php 
				if($id==NULL):
					echo form_label('Repita a senha');
					echo form_password(array('name'=>'passwordr', 'id'=>'passwordr', 'placeholder'=>'Repita a Senha', 'class'=>'form-control'));
					echo form_error("passwordr");
				endif;
			?>  
  		</div>
	</div>
	</div>
	<div class="row">
		<div class="col-md-3 col-md-offset-9">
		</div>
	</div>
	</div>		
	<div id="photo-user" class="col-md-4">
		<a class="dropdown-toggle" onclick="photo.click()" data-toggle="dropdown">
			<img src="<?php if($id == NULL || $row->photo == (NULL || '0' || $row->photo == base_url().'public/photos/thumbs/')){echo base_url().'public/images/no-photo.jpg';} else{echo base_url().'public/photos/thumbs/'.@$row->photo;} ?>" class="img-rounded" id="file_user" alt="" />
		</a>
		<?php if(@$id != NULL):?>
			<div class="form-group hidden"> 
				<input id="photo" name="photo" class="input-file" type="file">			
			</div>
		<?php
			endif;
			echo form_error("photo"); 
			if($this->session->flashdata('error_photo')):
				echo $this->session->flashdata('error_photo');
			endif;
		?> 	
	</div>		
	</fieldset>
	</div>
</form>