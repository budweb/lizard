<div class="row">
	<form class="form" method="post" action="">
		<div class="col-md-10 col-md-offset-1">
			<h1><b><?php echo $titulo; ?></b></h1>
			<?php $this->message->quick_message_generate(); ?>
			<div class="row">
				<div class="col-md-12">
					<?php 	
						control("add", array('label'=> 'Novo')); 
						control("selectAll");
						control("deleteLot", array('href' => 'admin_property/deletens/'));
					?>
				</div>
			</div>
			<?php
				$this->property->get_byid();
							
				$this->table->set_template(array ( 'table_open'  => '<table class="table">'));
				$this->table->set_heading(array('data' =>  '', 'class' => 'hidden-xs'), 'Bar/Restaurante', 'Telefone', 'Proprietário', '', '', '', '');
				foreach(@$propertys->result() as $propertys):
															
					@$propertys->ative == 0 ? $registerDesative = 'registerDesative' : $registerDesative = '';
									
					$this->table->add_row(
							array('data' => checkEdit($this->general->url_encode(@$propertys->id)), 'class' => 'hidden-xs '.$registerDesative),
							array('data' => @$propertys->fantasyName, 'class' => $registerDesative),
							array('data' => '('.@$propertys->ddd.') '.@$propertys->phone, 'class' => $registerDesative),
							array('data' => $this->property->owner_name(@$propertys->id), 'class' => $registerDesative),
							updateLink("admin_property/update/".$this->general->url_encode(@$propertys->id)),
							configLink("admin_property/setting/".$this->general->url_encode(@$propertys->id)),
							deleteLink("admin_property/delete/".$this->general->url_encode(@$propertys->id), @$propertys->fantasyName));
				endforeach;
				echo $this->table->generate();
				echo modal();
				if(@$pagination)echo @$pagination;
			?>
		</div>
	</form>
</div>
