<?php 
$id = $this->general->url_decode($this->uri->segment(3));

if($id!=NULL):
$row = $this->property->get_byid($id)->row();
endif;
?>
<form action="" method="POST" enctype="multipart/form-data" id="form_user">
	<div class="row">
	<fieldset>
		<legend><h2><?php echo $titulo.'<b>'.@$row->fantasyName.'</b>';?></h2></legend>
		<?php $this->message->quick_message_generate(); ?>
		<div class="row">
			<div class="col-md-4">
				<?php					
					control("savebt");
					control("other", array('label' => 'Listar', 'href'=>'admin_property/', 'classIcon'=>'glyphicon glyphicon-th-list'));																				
				?>								
			</div>			
		</div>
		
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
     				<?php
  						echo form_hidden('id', $this->general->url_encode(@$row->id));
  						echo form_label('Razão Social'); 
  						echo form_input(array('name'=>'name', 'id'=>'name', 'placeholder'=>'Razão Social', 'class'=>'form-control'), set_value('name', @$row->name), 'autofocus'); 
  						echo form_error("name");
  					?>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
				<?php 
					echo form_label('Nome Fantasia');
					echo form_input(array('name'=>'fantasyName', 'id'=>'fantasyName', 'placeholder'=>'Nome Fantasia', 'class'=>'form-control'), set_value('fantasyName', @$row->fantasyName));
					echo form_error("fantasyName");
				?>  
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
     				<?php
  						echo form_label('Responsável'); 
  						echo form_input(array('name'=>'manager', 'id'=>'manager', 'placeholder'=>'Responsável', 'class'=>'form-control'), set_value('manager', @$row->manager)); 
  						echo form_error("manager");
  					?>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
     				<?php
  						echo form_label('Endereço'); 
  						echo form_input(array('name'=>'address', 'id'=>'address', 'placeholder'=>'Endereço', 'class'=>'form-control'), set_value('address', @$row->address)); 
  						echo form_error("address");
  					?>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
				<?php 
					echo form_label('Bairro');
					echo form_input(array('name'=>'district', 'id'=>'district', 'placeholder'=>'Bairro', 'class'=>'form-control'), set_value('district', @$row->district));
					echo form_error("district");
				?>  
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
				<?php 
					echo form_label('CEP');
					echo form_input(array('name'=>'zipCode', 'id'=>'zipCode', 'placeholder'=>'CEP', 'class'=>'form-control'), set_value('zipCode', @$row->zipCode));
					echo form_error("zipCode");
				?>  
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
				<?php 
					echo form_label('Cidade');
					echo form_input(array('name'=>'city', 'id'=>'city', 'placeholder'=>'Cidade', 'class'=>'form-control'), set_value('city', @$row->city));
					echo form_error("city");
				?>  
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">		
					<?php 
						echo form_label('Estado');
						$options = array('Acre'=>'AC', 'Alagoas'=>'AL', 'Amapá'=>'AP', 'Amazonas'=>'AM', 'Bahia'=>'BA', 'Ceará'=>'CE', 'Distrito Federal'=>'DF', 'Espírito Santo'=>'ES', 'Goiás'=>'GO', 'Maranhão'=>'MA', 'Mato Grosso'=>'MT', 'Mato Grosso do Sul'=>'MS', 'Minas Gerais'=>'MG', 'Pará'=>'PA', 'Paraíba'=>'PB', 'Paraná'=>'PR', 'Pernambuco'=>'PE', 'Piauí'=>'PI', 'Rio de Janeiro'=>'RJ', 'Rio Grande do Norte'=>'RN', 'Rio Grande do Sul'=>'RS', 'Rondônia'=>'RO', 'Roraima'=>'RR', 'Santa Catarina'=>'SC', 'São Paulo'=>'SP', 'Sergipe'=>'SE', 'Tocantins'=>'TO');
						$attr = 'id="state" class="form-control"';				
						$config = array(set_value('state', @$row->state));
				
						echo form_dropdown('state', $options, $config, $attr);
						echo form_error("state");
			 		?>
				</div>		
			</div>
			<div class="col-md-1">
				<div class="form-group">
				<?php 
					echo form_label('DDD');
					echo form_input(array('name'=>'ddd', 'id'=>'ddd', 'placeholder'=>'DDD', 'class'=>'form-control'), set_value('ddd', @$row->ddd));
					echo form_error("ddd");
				?>  
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
				<?php 
					echo form_label('Telefone');
					echo form_input(array('name'=>'phone', 'id'=>'phone', 'placeholder'=>'Telefone', 'class'=>'form-control'), set_value('phone', @$row->phone));
					echo form_error("phone");
				?>  
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
				<?php 
					echo form_label('E-Mail');
					echo form_input(array('name'=>'email', 'id'=>'email', 'placeholder'=>'E-Mail', 'class'=>'form-control'), set_value('email', @$row->email));
					echo form_error("email");
				?>  
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="form-group">
     				<?php
  						echo form_label('CNPJ'); 
  						echo form_input(array('name'=>'cnpj', 'id'=>'cnpj', 'placeholder'=>'CNPJ', 'class'=>'form-control'), set_value('cnpj', @$row->cnpj)); 
  						echo form_error("cnpj");
  					?>
				</div>
			</div>			
			
			<div class="col-md-3">
				<div class="form-group">
				<?php 
					echo form_label('Home Page');
					echo form_input(array('name'=>'webSite', 'id'=>'webSite', 'placeholder'=>'Site da empresa', 'class'=>'form-control'), set_value('webSite', @$row->webSite));
					echo form_error("webSite");
				?>  
				</div>
			</div>			
			<div class="col-md-3">
				<div class="form-group">
     				<?php
  						echo form_label('Insc. Municipal'); 
  						echo form_input(array('name'=>'municipalRegistration', 'id'=>'municipalRegistration', 'placeholder'=>'Inscrição Municipal', 'class'=>'form-control'), set_value('municipalRegistration', @$row->municipalRegistration)); 
  						echo form_error("municipalRegistration");
  					?>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
				<?php 
					echo form_label('Insc. Estadual');
					echo form_input(array('name'=>'stateRegistration', 'id'=>'stateRegistration', 'placeholder'=>'Inscrição Estadual', 'class'=>'form-control'), set_value('stateRegistration', @$row->stateRegistration));
					echo form_error("stateRegistration");
				?>  
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">		
					<?php 
						echo form_label('Filial');
						$options = array('Matriz'=>'Matriz', 'Filial'=>'Filial');
						$attr = 'id="filial" class="form-control"';				
						$config = array(set_value('filial', @$row->filial));
				
						echo form_dropdown('filial', $options, $config, $attr);
						echo form_error("filial");
			 		?>
				</div>		
			</div>			
		<?php if($id == NULL):?>		
		<div class="col-md-6 col-md-offset-1">
		<span>Módulos adicionais</span>
		<hr/>
		
		<div class="row">
			<div class="col-md-6">
				<?php					
					$checkbox_basic = array(
						'name'=> 'basic[]',
						'class' => 'basic',
						'value' => @$row->basic,
						'checked' => @$row->id == NULL ? TRUE : @$row->basic
					);
					echo form_checkbox($checkbox_basic);
					echo form_label('Básicos', 'basic[]');					
				?>				
				</div>
				
				<div class="col-md-6">
				<?php					
					$checkbox_automaticService = array(
						'name'=> 'automaticService[]',
						'class' => 'automaticService',
						'value' => @$row->automaticService,
						'checked' => @$row->id == NULL ? TRUE : @$row->automaticService
					);
					echo form_checkbox($checkbox_automaticService);
					echo form_label('Atendimento Automatizado', 'automaticService[]');					
				?>				
				</div>			
				
				<div class="col-md-6">				
				<?php					
					$checkbox_stock = array(
						'name'=> 'stock[]',
						'class' => 'stock',
						'value' => @$row->stock,
						'checked' => @$row->id == NULL ? TRUE : @$row->stock
					);
					echo form_checkbox($checkbox_stock);
					echo form_label('Controle de Estoque', 'stock[]');					
				?>				
				</div>			
				
				<div class="col-md-6">
				<?php					
					$checkbox_financialControl = array(
						'name'=> 'financialControl[]',
						'class' => 'financialControl',
						'value' => @$row->financialControl,
						'checked' => @$row->id == NULL ? TRUE : @$row->financialControl
					);
					echo form_checkbox($checkbox_financialControl);
					echo form_label('Análise Financeira', 'financialControl[]');					
				?>				
				</div>
				</div>
			</div>	
			<?php endif;?>						
		</div>
		
		
		
				
	</fieldset>
	</div>
	
	
				
			
</form>