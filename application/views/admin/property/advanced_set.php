<?php 
	$id = $this->general->url_decode($this->uri->segment(3));
	@$row = $this->property->get_user_byid($id)->row();
	@$row_property = $this->property->get_byid($id)->row();
?>
<form action="" method="POST" enctype="multipart/form-data" role="form">
	<fieldset>
		<legend><h2><?php echo $titulo.'<b>'.@$row_property->fantasyName.'</b>'; ?></h2></legend>
		<?php $this->message->quick_message_generate(); ?>
		
		<div class="col-md-12" id="left_col"> 
			<div class="row"> <!-- Begin ROW -->
					<div class="col-md-7">	
						<?php				
							control("button", array('classIcon' => 'glyphicon glyphicon-ok', 'label'=>'Configurar'));
							control('other', array('href'=>'admin_property/setting/'.$this->uri->segment(3), 'classIcon'=>'glyphicon glyphicon-arrow-left', 'label'=>'Configurações'));
							control('other', array('href'=>'admin_property/', 'classIcon'=>'glyphicon glyphicon-th-list', 'label'=>'Listar'));
						?>				
					</div>
			</div> <!-- End ROW -->			
		</div>
		
		<div class="col-md-12">
			<h2><label>Status da Propriedade</label></h2>
			<hr/>
		</div>
		<div class="col-md-2 ">			
			
				
				<div class="form-group">		
					<?php
						echo form_hidden('id', $this->general->url_encode($id));
						echo form_hidden('clearLog', 'ative');
						$options = array('1'=>'ATIVADA', '0'=>'DESATIVADA');
						$attr = 'id="ative" class="form-control selectAtiveProperty"';				
						$config = array(set_value('ative', @$row_property->ative));
						echo form_dropdown('ative', $options, $config, $attr);
						echo form_error("ative");
			 		?>
				</div>
				</div>
	</fieldset>
</form>

<form action="" method="POST" enctype="multipart/form-data" role="form">
	<div class="col-md-12">
			<h2><label>Limpeza dos registros de LOG dos Usuários</label></h2>
			<hr/>
	</div>
	
	<div class="col-md-3">
		<div class="form-group">		
			<?php 
				echo form_hidden('clearLog', 'auto');
				echo form_hidden('id', $this->general->url_encode($id));
										
				$options = array('0'=>'Nunca', '2678400'=>'Um mês', '7776000'=>'Três mêses', '15638400'=>'Seis mêses', '23587200'=>'Nove mêses', '31536000'=>'Um Ano');
				$attr = 'id="cleaningTime" class="form-control"';				
				$config = array(set_value('cleaningTime', @$row_property->cleaningTime));
				echo form_dropdown('cleaningTime', $options, $config, $attr);
				
				$checkbox = array(
						'name'=> 'tomorrow[]',
						'class' => 'checkEdit',
						'value' => @$row_property->tomorrow == '1' ? TRUE : FALSE,
						'checked' => @$row_property->tomorrow
				);
				echo form_checkbox($checkbox);
				echo form_label('Contagem a partir da data atual', 'tomorrow[]');
				
				echo form_error("cleaningTime");						
			?>			 		
		</div>								
	</div>
	<div class="col-md-0">
		<?php control("button", array('classIcon' => 'glyphicon glyphicon-dashboard', 'label'=>'Limpeza de LOGs programada'));?>
	</div>	
</form>

<div class="col-md-3">
	<form action="" method="POST" enctype="multipart/form-data" role="form">
		<?php
			echo form_hidden('clearLog', 'manual');
			echo form_hidden('id_property', $id);

			@$numrows_logs = $this->property->num_rows_user_log(@$id);			
			if(@$numrows_logs != 0):
				echo '<div class="col-md-12">';
				control("button", array('classIcon' => 'glyphicon glyphicon-remove-sign', 'label'=>'Limpar registros de LOG'));
				echo '</div>';
			endif;		
		?>
	</form>			
</div>
<div class="col-md-12">

	<?php 
		echo '(Próxima limpeza: ';
		$datestring = "%d/%m/%Y";
		$time = $row_property->dateTmp;
		if($row_property->cleaningTime == 0):
			echo '<b class="noClearLog">SEM DATA PROGRAMADA</b>)';
		else:
			echo '<b class="clearLog">'.mdate($datestring, $time).'</b>)';
		endif;
	
		echo '<h4>';
			if(@$numrows_logs == 0):
				echo '<span class="emptyRegister">SEM REGISTROS</span>';
			else:
				echo '<span class="numLogs">'.$numrows_logs.' registros de LOG</span>';
			endif;
			echo '</h4>';
	?>
</div>