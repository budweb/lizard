<?php 
	$id = $this->general->url_decode($this->uri->segment(3));
	$row = $this->property->get_byid($id)->row(); 
?>

<form action="" method="POST" enctype="multipart/form-data" role="form">
	<fieldset>
		<legend><h2><?php echo $titulo.'<b>'.@$row->fantasyName.'</b>'; ?></h2></legend>
		<?php $this->message->quick_message_generate(); ?>
				
		<div class="col-md-6" id="left_col"> <!-- Begin LEFT_COL -->
			<div class="row">
					<div class="col-md-7">	
						<?php				
							control("button", array('classIcon' => 'glyphicon glyphicon-ok', 'label'=>'Configurar'));
							control("other", array('label' => 'Configurações Avançadas', 'href'=>'admin_property/advanced_set/'.$this->general->url_encode($id), 'classIcon'=>'glyphicon glyphicon-th'));
							control('other', array('href'=>'admin_property/', 'classIcon'=>'glyphicon glyphicon-th-list', 'label'=>'Listar'));
						?>				
					</div>
			</div> <!-- End ROW -->			
			
			<div class="col-md-12">
				<h2><label>Localização</label></h2>
				<hr/>
			</div>
			
			<div class="col-md-10">
				<?php
					echo form_hidden('id', $this->general->url_encode(@$row->id));
					$attributes = array(
							'class' => '',
					);
     				echo form_label('Fuso horário', 'timezones', $attributes);
					echo timezone_menu(@$row->timeZone,"form-control");
				?>
				</div>
				
			<div class="col-md-10">
				<?php 
					$checkbox = array(
						'name'=> 'daylightSaving[]',
						'class' => 'checkEdit',
						'value' => @$row->daylightSaving == '1' ? TRUE : FALSE,
						'checked' => @$row->daylightSaving
					);
					echo form_checkbox($checkbox);
					echo form_label('Horário de verão', 'daylightSaving[]');
				?>
			</div>
			
			<div class="col-md-12">
				<h2><label>Módulos</label></h2>
				<hr/>
			</div>
			
			<div class="col-md-12">
			
			<div class="col-md-6">
				<?php					
					$checkbox_basic = array(
						'name'=> 'basic[]',
						'class' => 'basic',
						'value' => @$row->basic,
						'checked' => @$row->basic
					);
					echo form_checkbox($checkbox_basic);
					echo form_label('Básicos', 'basic[]');					
				?>				
				</div>			
			
				<div class="col-md-6">
				<?php					
					$checkbox_automaticService = array(
						'name'=> 'automaticService[]',
						'class' => 'automaticService',
						'value' => @$row->automaticService,
						'checked' => @$row->automaticService
					);
					echo form_checkbox($checkbox_automaticService);
					echo form_label('Atendimento Automatizado', 'automaticService[]');					
				?>				
				</div>
				
				<div class="col-md-6">
				<?php					
					$checkbox_stock = array(
						'name'=> 'stock[]',
						'class' => 'stock',
						'value' => @$row->stock,
						'checked' => @$row->stock
					);
					echo form_checkbox($checkbox_stock);
					echo form_label('Controle de Estoque', 'stock[]');					
				?>				
				</div>				
				
				<div class="col-md-6">
				<?php					
					$checkbox_financialControl = array(
						'name'=> 'financialControl[]',
						'class' => 'financialControl',
						'value' => @$row->financialControl,
						'checked' => @$row->financialControl
					);
					echo form_checkbox($checkbox_financialControl);
					echo form_label('Análise Financeira', 'financialControl[]');					
				?>				
				</div>
								
			</div>			
					
		</div> <!-- End LEFT_COL -->
		
		<div class="col-md-6" id="right_col"> <!-- Begin RIGHT_COL -->			
			<div class="col-md-12">
				<h2><label>Disponibilizar quantidade contas</label></h2>
				<hr/>
			</div>
			
			<div class="col-md-12">
				<?php
				//PRIMEIRA COLUNA DE CONTAS
					echo '<div class="col-md-4">';
						echo '<div class="col-md-12">';
							echo form_label('<h4>Administrativa: </h4>');
							echo '<div class="col-md-12">';
								echo form_input(array('type'=>'number', 'name'=>'numAdministrative', 'id'=>'numAdministrative', 'placeholder'=>'Nº', 'class'=>'form-control'), set_value('numAdministrative', @$row->numAdministrative));
								echo form_error("numAdministrative");
								echo '</br>';
							echo '</div>';
							echo form_label('<h4>Gerente(Maitre): </h4>');
							echo '<div class="col-md-12">';
								echo form_input(array('type'=>'number', 'name'=>'numMaitre', 'id'=>'numMaitre', 'placeholder'=>'Nº', 'class'=>'form-control'), set_value('numAdministrative', @$row->numMaitre));
								echo form_error("numMaitre");								
							echo '</div>';
						echo '</div>';
					echo '</div>';
					
				//SEGUNDA COLUNA DE CONTAS
					echo '<div class="col-md-4">';
						echo '<div class="col-md-12">';
							echo form_label('<h4>Garçon: </h4>');
							echo '<br/>';
							echo '<div class="col-md-12">';
								echo form_input(array('type'=>'number', 'name'=>'numWaiter', 'id'=>'numWaiter', 'placeholder'=>'Nº', 'class'=>'form-control'), set_value('numWaiter', @$row->numWaiter));
								echo form_error("numWaiter");
								echo '</br>';
							echo '</div>';
							echo form_label('<h4>Caixa: </h4>');
							echo '<div class="col-md-12">';
								echo form_input(array('type'=>'number', 'name'=>'numDesk', 'id'=>'numDesk', 'placeholder'=>'Nº', 'class'=>'form-control'), set_value('numDesk', @$row->numDesk));
								echo form_error("numDesk");								
							echo '</div>';
						echo '</div>';
					echo '</div>';
					
					//TERCEIRA COLUNA DE CONTAS
					echo '<div class="col-md-4">';
						echo '<div class="col-md-12">';
								echo form_label('<h4>Cozinha: </h4>');
								echo '<div class="col-md-12">';
									echo form_input(array('type'=>'number', 'name'=>'numCook', 'id'=>'numCook', 'placeholder'=>'Nº', 'class'=>'form-control'), set_value('numCook', @$row->numCook));
									echo form_error("numCook");
									echo '</br>';
								echo '</div>';
								$attributes = array(
										'class' => 'numOwner',
										'style' => 'color: #607B8B;',
								);
								echo form_label('<h4>Proprietário: </h4>', 'numOwner', $attributes);
								echo '<div class="col-md-12">';
									echo form_input(array('type'=>'number', 'name'=>'numOwner', 'id'=>'numOwner', 'placeholder'=>'Nº', 'class'=>'form-control', 'style' => 'color: #838B8B;'), set_value('numOwner', @$row->numOwner), 'disabled');
									echo form_error("numOwner");									
								echo '</div>';
							echo '</div>';
						echo '</div>';
					echo '</div>';					
				?>
				<div class="col-md-12">
					<h2><label>Cobranças adicionais</label></h2>
					<hr/>
				</div>
				<div class="col-md-10">
				<?php					
					$checkbox = array(
						'name'=> 'commission[]',
						'class' => 'commission',
						'value' => @$row->commission == '1' ? TRUE : FALSE,
						'checked' => @$row->commission
					);
					echo form_checkbox($checkbox);
					echo form_label('Comissão do garçon (10% por padrão)', 'commission[]');
				?>				
			</div>				
			</div>
													
		</div>	<!-- End RIGHT_COL -->	
									
	</fieldset>
</form>