<?php
	$row = $this->admin->get_byid('user', $this->session->userdata('id'))->row(); 	
?>

<form action="set" method="POST">
	<fieldset>
		<legend><h2><?php echo $titulo;?></h2></legend>
		<?php $this->message->quick_message_generate(); ?>
		
		<div class="col-md-6" id="left_col"> <!-- Begin LEFT_COL -->
			<div class="row">
					<div class="col-md-7">	
						<?php
							echo form_hidden('id', $this->session->userdata('id'));	
							control("button", array('classIcon' => 'glyphicon glyphicon-ok', 'label'=>'Configurar'));
							control("other", array('label' => 'Cancelar', 'href'=>'admin', 'classIcon'=>'glyphicon glyphicon-th-large'));
						?>				
					</div>
			</div> <!-- End ROW -->			
			
			<div class="col-md-12">
				<h2><label>Localidade</label></h2>
				<hr/>
			</div>
			
			<div class="col-md-10">
				<?php
					$attributes = array(
							'class' => '',
					);
     				echo form_label('Fuso horário', 'timezones', $attributes);
					echo timezone_menu(@$row->timeZone,"form-control");
				?>
				</div>
				
			<div class="col-md-10">
				<?php 
					$checkbox = array(
						'name'=> 'daylightSaving[]',
						'class' => 'checkEdit',
						'value' => @$row->daylightSaving == '1' ? TRUE : FALSE,
						'checked' => @$row->daylightSaving
					);
					echo form_checkbox($checkbox);
					echo form_label('Horário de verão', 'daylightSaving[]');					
				?>
			</div>
			
			<div class="col-md-12">
				<h2><label>Outras Configurações</label></h2>
				<hr/>
			</div>
			
			<div class="col-md-10">
				<div class="form-group">		
					<?php 
						/*$options[' ']='Selecione';
						 foreach(@$query->result() as $property):
							$options[$property->id] = $property->fantasyName.' - '.$property->address;					
						endforeach; 

						$disabled = @$query->num_rows() == 0 ? 'disabled' : ''; 
						$attr = 'id="id_property" class="form-control"'.$disabled;				
						$config = array(set_value('id_property', @$row->id_property)); 
				
						echo form_dropdown('id_property', $options, $config, $attr);					
						echo form_error("id_property");
						echo form_error("type");*/
			 		?>			 		
				</div>				
			</div>		
		</div> <!-- End LEFT_COL -->
		
		<div class="col-md-6" id="left_col"> <!-- Begin RIGTH_COL -->
						
			<div class="col-md-12">
				<h2><label>Outras Configurações</label></h2>
				<hr/>
			</div>
			
			<div class="col-md-10">
				<?php
					/* $attributes = array(
							'class' => '',
					);
     				echo form_label('Fuso horário', 'timezones', $attributes);
					echo timezone_menu(@$row->timeZone,"form-control"); */
				?>
				</div>
				
			<div class="col-md-10">
				<?php 
					/* $checkbox = array(
						'name'=> 'daylightSaving[]',
						'class' => 'checkEdit',
						'value' => @$row->daylightSaving == '1' ? TRUE : FALSE,
						'checked' => @$row->daylightSaving
					);
					echo form_checkbox($checkbox);
					echo form_label('Horário de verão', 'daylightSaving[]'); */					
				?>
			</div>
			
			<div class="col-md-12">
				<h2><label>Outras Configurações</label></h2>
				<hr/>
			</div>
			
			<div class="col-md-10">
				<div class="form-group">		
					<?php 
						/*$options[' ']='Selecione';
						 foreach(@$query->result() as $property):
							$options[$property->id] = $property->fantasyName.' - '.$property->address;					
						endforeach; 

						$disabled = @$query->num_rows() == 0 ? 'disabled' : ''; 
						$attr = 'id="id_property" class="form-control"'.$disabled;				
						$config = array(set_value('id_property', @$row->id_property)); 
				
						echo form_dropdown('id_property', $options, $config, $attr);					
						echo form_error("id_property");
						echo form_error("type");*/
			 		?>			 		
				</div>				
			</div>		
		</div> <!-- End RIGTH_COL -->	
										
	</fieldset>
</form>