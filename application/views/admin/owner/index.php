<div class="row">
	<form class="form" method="post" action="">
		<div class="col-md-10 col-md-offset-1">
			<h1><b><?php echo $titulo; ?></b></h1>
			<?php $this->message->quick_message_generate(); ?>
			<div class="row">
				<div class="col-md-12">
					<?php 	
						control("add", array('label'=> 'Novo')); 
						control("selectAll");
						control("deleteLot", array('href' => 'admin_owner/deletens/'));
					?>
				</div>
			</div>
			<?php						
				$this->table->set_template(array ( 'table_open'  => '<table class="table">' ));
				$this->table->set_heading(array('data' =>  '', 'class' => 'hidden-xs'), 'Nome', 'Telefone', 'Bar/Restaurante', '', '');
				foreach(@$owners->result() as $owners):
					
					@$owners->status == 0 ? $registerDesative = 'registerDesative' : $registerDesative = '';
						
					$this->table->add_row(
							array('data' => checkEdit($this->general->url_encode(@$owners->id)), 'class' => 'hidden-xs '.$registerDesative),
							array('data' => @$owners->firstName.' '.@$owners->lastName, 'class' => $registerDesative),
							array('data' => '('.@$owners->ddd.') '.@$owners->phone, 'class' => $registerDesative),
							array('data' => $this->user->property_fantasy_name(@$owners->id_property), 'class' => $registerDesative),								
							updateLink("admin_owner/update/".$this->general->url_encode(@$owners->id)),
							deleteLink("admin_owner/delete/".$this->general->url_encode(@$owners->id), @$owners->firstName));
				endforeach;
				echo $this->table->generate();
				echo modal();
				if(@$pagination)echo @$pagination;
			?>
		</div>
	</form>
</div>