<?php 
$id = $this->general->url_decode($this->uri->segment(3));

if($id!=NULL):
$row = $this->user->get_byid($id)->row();
endif;
?>
<form action="" method="POST" enctype="multipart/form-data" id="form_user">
	<div class="row">
	<fieldset>
		<legend><h2><?php echo $titulo.'<b>'.@$row->firstName.' '.@$row->lastName.'</b>';?></h2></legend>
		<div class="row">
			<div class="col-md-3 col-md-offset-2">
				<?php
					control("savebt");
					control("other", array('label' => 'Listar', 'href'=>'admin_owner/', 'classIcon'=>'glyphicon glyphicon-th-list'));										
				?>				
			</div>
		</div>
	<div class="col-md-8 col-md-offset-2">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
     				<?php
     					echo form_hidden('function', 'cad_owner');
     					echo form_hidden('cpf_bd', md5(@$row->cpf));
     					echo form_hidden('password_bd', @$row->password);
  						echo form_hidden('id', $this->general->url_encode(@$row->id));
  						echo form_hidden('type', 0);
  						echo form_label('Nome'); 
  						echo form_input(array('name'=>'firstName', 'id'=>'firstName', 'placeholder'=>'Nome', 'class'=>'form-control'), set_value('firstName', @$row->firstName), 'autofocus'); 
  						echo form_error("firstName");
  					?>
				</div>
			</div>
	<div class="col-md-6">
		<div class="form-group">
			<div class="form-group">
				<?php 
					echo form_label('Sobrenome');
					echo form_input(array('name'=>'lastName', 'id'=>'lastName', 'placeholder'=>'Sobrenome', 'class'=>'form-control'), set_value('lastName', @$row->lastName));
					echo form_error("lastName");
				?>  
			</div>
		</div>
	</div>
		</div>
	
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<?php 
					echo form_label('E-Mail');
					echo form_input(array('name'=>'email', 'id'=>'email', 'placeholder'=>'E-Mail', 'class'=>'form-control'), set_value('email', @$row->email));
					echo form_error("email");
				?>
  			</div>
		</div>
		<div class="col-md-2">
				<div class="form-group">
				<?php 
					echo form_label('DDD');
					echo form_input(array('name'=>'ddd', 'id'=>'ddd', 'placeholder'=>'DDD', 'class'=>'form-control'), set_value('ddd', @$row->ddd));
					echo form_error("ddd");
				?>  
				</div>
		</div>		
	<div class="col-md-4">
		<div class="form-group">
			<div class="form-group">
				<?php 
					echo form_label('Telefone');
					echo form_input(array('name'=>'phone', 'id'=>'phone', 'placeholder'=>'Telefone', 'class'=>'form-control'), set_value('phone', @$row->phone));
					echo form_error("phone");
				?>
			</div>
		</div>
	</div>
	</div>
	
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<?php 
					echo form_label('CPF');
					echo form_input(array('name'=>'cpf', 'id'=>'cpf', 'placeholder'=>'CPF', 'class'=>'form-control'), set_value('cpf', @$row->cpf));
					echo form_error("cpf");
				?>
			</div>
		</div>		
		<div class="col-md-8">
			<div class="form-group">		
				<?php
					echo form_label('Propriedade (Bar ou Restaurante)');
					
					$options[' ']='Selecione';
					foreach(@$query->result() as $property):
						$options[$property->id] = $property->fantasyName.' ('.$property->filial.')'.' - '.$property->address;					
					endforeach;

					$disabled = @$query->num_rows() == 0 ? 'disabled' : ''; 
					$attr = 'id="id_property" class="form-control"'.$disabled;				
					$config = array(set_value('id_property', @$row->id_property));
				
					echo form_dropdown('id_property', $options, $config, $attr);					
					echo form_error("id_property");
					echo form_error("type");
				 ?>
			</div>		
		</div>	
	</div>
	
	</div>		
	</fieldset>	
	</div>
</form>
<?php if(@$row->id != NULL):?>
	<form action="" method="POST" enctype="multipart/form-data" id="form_user">	
		<div class="col-md-offset-2">
			<?php
				echo form_hidden('function', 'reset_pass');
				echo form_hidden('id', $this->uri->segment(3));
				if(@$row->password == md5(@$row->cpf)):
					$label = 'Senha Insegura';
					$classIcon = 'glyphicon glyphicon-alert';
					$other_disabled = 'disabled';
					$style = 'background-color:red;'; 
				else:
					$label = 'Resetar a Senha';
					$classIcon = 'glyphicon glyphicon-lock';
					$other_disabled = NULL;
					$style = NULL;
				endif;
				control("button", array('label' => $label, 'href'=>'admin_owner/', 'classIcon'=>$classIcon, 'other'=>$other_disabled, 'style'=>$style)); 
			?>
		</div>
	</form>
<?php endif;?>