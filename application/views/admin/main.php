<div class="row">
	<?php $this->message->quick_message_generate(); ?>
	<div class="col-xs-6 col-sm-4 col-md-3 block">	
		<a href="<?php echo base_url('admin_property/'); ?>" class="modules property">
			<label>Propriedades</label>
		</a>
	</div>
	
	<div class="col-xs-6 col-sm-4 col-md-3 block">	
		<a href="<?php echo base_url('admin_owner/'); ?>" class="modules owner">
			<label>Proprietários</label>
		</a>
	</div>
	
	<div class="col-xs-6 col-sm-4 col-md-3 block">	
		<a href="<?php echo base_url('admin/list_admin'); ?>" class="modules admin">
			<label>Masters</label>
		</a>
	</div>	
	
	<div class="col-xs-6 col-sm-4 col-md-3 block">
		<a href="<?php echo base_url('admin/setting'); ?>" id="config" class="modules config">
			<label>Configurações</label>
		</a>
	</div>	
	
	</div>		