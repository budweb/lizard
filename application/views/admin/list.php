<div class="row">
	<form class="form" method="post" action="">
		<div class="col-md-8 col-md-offset-2">
			<h1><b><?php echo $titulo; ?></b></h1>
			<?php $this->message->quick_message_generate(); ?>
			<div class="row">
				<div class="col-md-12">
					<?php 	
						control("add", array('label'=> 'Novo'));
					?>
				</div>
			</div>
			<?php
				$this->table->set_template(array ( 'table_open'  => '<table class="table">' ));
				$this->table->set_heading('Nome', 'E-Mail', 'Telefone', '', '');
				foreach($users->result() as $users):
					
					$this->table->add_row(
							$users->firstName.' '.$users->lastName,
							$users->email,
							'('.$users->ddd.') '.$users->phone,
							updateLink("admin/update/".$this->general->url_encode($users->id)),
							deleteLink("admin/delete/".$this->general->url_encode($users->id), $users->firstName));
				endforeach;
				echo $this->table->generate();
				echo modal();
				if(@$pagination)echo @$pagination;
			?>
		</div>
	</form>
</div>