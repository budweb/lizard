<?php  

$url = $this->uri->segment(3); 


?>
<div role="tabpanel">

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" <?php echo $url != "all"? "class='active'": ""; ?> ><a href="<?php echo base_url("cook_monitor/desk_monitor"); ?>">Caixa</a></li>
    <li role="presentation" <?php echo $url == "all"? "class='active'": ""; ?>><a href="<?php echo base_url("cook_monitor/desk_monitor/all"); ?>" >Todos</a></li>
  </ul>

  <!-- Tab panes -->

  <div class="tab-content">
<?php 

  if($url != "all"):
?>
    <div role="tabpanel" class="tab-pane active" id="desk">

		<div class="row">
		<div class="col-md-12" type="ajaxrefresh" time="15000" load="<?php echo base_url('cook_monitor/ajax_desk_monitor'); ?>">



		</div>
		</div>
    </div>
<?php else: ?>


    <div role="tabpanel" class="tab-pane active" id="all">

		<div class="row">
		<div class="col-md-12" type="ajaxrefresh" time="15000" load="<?php echo base_url('cook_monitor/ajax_desk_monitor?type=all'); ?>">



		</div>
		</div>

    </div>
  <?php endif; ?>
  </div>

</div>

