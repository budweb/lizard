<?php
foreach($query->result() as $row):
$q = $this->model->itens_of_demands($row->id);//itens do pedido
if($q->num_rows > 0): //Só mostra se existir algum ítem

$a = false;
foreach($q->result() as $r){
	if($r->status_di != 2){
		$a = true;
		break;
	}
}
if($a):
?>
<div class="panel panel-primary">
  <div class="panel-heading">

    <?php echo str_pad($row->id, 5, "0", STR_PAD_LEFT); ?>
    <?php echo ", Mesa ".$row->tableName; ?>
    <?php echo ", ".mdate("%d/%m/%Y %H:%i",mysql_to_unix($row->created)); ?>
    <?php echo ", ".$row->firstName." ".$row->lastName;?>
  </div>
    
  <div class="panel-body">

  
  <ul class="list-group">
	 <?php
	
	foreach($q->result() as $r):
	$statusClasse[""] = "toDo"; $statusLabel[""] = "Em espera";
	$statusClasse[0] = "toDo"; $statusLabel[0] = "Em espera";
	$statusClasse[1] = "doing";$statusLabel[1] = "Em produção";
	$statusClasse[2] = "done"; $statusLabel[2] = "Pronto!";
	?>
  <li class="list-group-item iten" url="<?php echo base_url("cook_monitor/ajax_status_iten/".$this->general->url_encode($r->id_di)); ?>">
  <span class="led <?php echo $statusClasse[$r->status_di]?>"></span>
  <?php echo $r->name;?> 
  <small><i>(<?php echo $statusLabel[$r->status_di]?>)</i></small>
<b><?php 
if($r->status_di==0){

  $productionTime = $r->productionTime;
   $productionTimeSplit = explode(":", $productionTime);
   $productionTimeUnix = $productionTimeSplit[0] * 3600 + $productionTimeSplit[1] * 60 + $productionTimeSplit[2];
   $productionTimeUnix = $productionTimeUnix*$r->qtde-3600;

  echo mdate("%H:%i:%s", $productionTimeUnix);
}elseif($r->status_di==1){
   $startTime = mysql_to_unix($r->startProduction);
   
   $productionTime = $r->productionTime;
   $productionTimeSplit = explode(":", $productionTime);
   $productionTimeUnix = $productionTimeSplit[0] * 3600 + $productionTimeSplit[1] * 60 + $productionTimeSplit[2];
   $productionTimeUnix = $productionTimeUnix*$r->qtde;
   
   $productionFinalTime = $startTime + $productionTimeUnix;
   $nowTime = mysql_to_unix($this->format_date->local_date());

   if($productionFinalTime >= $nowTime){
      $countTime =  $productionFinalTime -$nowTime - 3600;
      $hour = mdate("%H:%i:%s", $countTime);
      $negative = "0";
      $class = "";

   }else{
      $countTime =  $nowTime - $productionFinalTime - 3600;
      $hour = "-".mdate("%H:%i:%s", $countTime);
      $negative = "1";
      $class="negative";
   }

   echo "<span class='time ".$class."' negative='".$negative."'>".$hour."</span>";
  
}elseif($r->status_di==2){

  
  $startTime = mysql_to_unix($r->startProduction);
   
  $finishedTime = mysql_to_unix($r->finishedProduction);

   if($startTime >= $finishedTime){
      $countTime =  $startTime - $finishedTime - 3600;
      echo mdate("%H:%i:%s", $countTime);
   }else{
      $countTime =  $finishedTime - $startTime - 3600;
      echo mdate("%H:%i:%s", $countTime);
   }


}

?></b>
    <?php if($r->qtde > 1): ?>
    <span class="badge"><?php echo $r->qtde." ".unid($r->unid); ?></span>
    <br />
 <i class="text-success"> <small><?php echo $r->description;?></small></i>
    </li>
    <?php endif;?>
    <?php endforeach;?>
  </ul>
  
  
  </div>
   <div class="panel-footer">
<?php echo $row->comment; ?>
   </div>
</div>
<?php 
endif;
endif;
endforeach;

echo script_tag('public/js/cook_monitor.js'); // recarrega cook_monitor.js
?>