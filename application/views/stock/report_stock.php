<?php $produto = $stocks->row(); ?>
<div class="row">
<form class='form' method="post" action="">

<div class="col-md-10 col-md-offset-1">
<h1><?php echo $titulo; ?></h1>
 <div class="row"> 
 <div class="col-md-12">
<?php 
$item_ass = $this->stock->get_itens_byid(@$produto->Sid_itens)->row();
control("list", array('label' => 'Estoque', 'href'=>'stock/'));
control('back');
?>
</div>
</div>
<?php
$unid = array('Unidade(s)','Kg','g', 'l');
@$produto->Sdescription == NULL ? $descProd = NULL : $descProd = '('.@$produto->Sdescription.')';
echo'<hr/>';
echo '<h3><b>'.@$produto->Sname.'</b> <small>'.$descProd.'</small></h3>'.'<h5><b>Quantidade mínima:</b> <i>'.@$produto->SquantityMin.' '.$unid[@$produto->Sunid].'</i></h5>';
echo'<hr/>';

$this->table->set_template(array ( 'table_open'  => '<table class="table">' ));
				$this->table->set_heading('Data', array('data' => 'Hora', 'class' => 'hidden-xs'), 'Quantidade', array('data' =>  'Observação', 'class' => 'hidden-xs'));
				foreach(@$stocks->result() as $stock):
					if($stock->Aquantity <= $stock->SquantityMin && $stock->Aquantity != 0):$color='#FFFF00';elseif($stock->Aquantity == 0):$color='#FF0000';elseif($stock->Aquantity > $stock->SquantityMin):$color='#00FF00';else:$color='#FFFFFF';endif;
					$this->table->add_row(
							$this->format_date->mysql_to_data_standard($stock->Aupdated),
							array('data' => $this->format_date->mysql_to_time_standard($stock->Aupdated), 'class' => 'hidden-xs'),
							array('data' =>  '<b>'.$stock->Aquantity.'</b> '. '<small>'.$unid[$stock->Sunid].'</small>', 'style' => 'color:'.$color.';'),
							array('data' =>  $stock->Aobservation, 'style' => 'color:'.$stock->Acolor.';font-size: 0.7em;font-style: italic;', 'class' => 'hidden-xs'));
				endforeach;
				echo $this->table->generate();
				echo modal();
				if(@$pagination)echo @$pagination;
?>
</div>

</form>
</div>