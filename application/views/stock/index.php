<div class="row">
<form class='form' method="post" action="">
<div class="col-md-10 col-md-offset-1">
<h1><?php echo $titulo; ?></h1>

<?php $this->message->quick_message_generate(); ?>

 <div class="row">
 <div class="col-md-12">
<?php 
control("add", array('label'=> 'Cadastrar no estoque'));
control("selectAll");
control("deleteLot", array('href' => 'stock/deletens/'));
?>
</div>
</div>
<div class="row">
<div class="col-md-4">
<?php search($search); ?>
</div>
</div>
<?php
$unid = array('Unidade(s)','Kg','g', 'l');
$this->table->set_template(array ( 'table_open'  => '<table class="table">' ));
				$this->table->set_heading(array('data' =>  '', 'class' => 'hidden-xs'), 'Produto', array('data' =>  'Descrição', 'class' => 'hidden-xs'), 'Quantidade', '', '');
				foreach(@$stocks->result() as $stock):
					if($stock->quantity <= $stock->quantityMin && $stock->quantity != 0):$color='#FFFF00';elseif($stock->quantity == 0):$color='#FF0000';elseif($stock->quantity > $stock->quantityMin):$color='#00FF00'; else:$color='#FFFFFF';endif;
					$this->table->add_row(
							array('data' =>  checkEdit($this->general->url_encode($stock->id)), 'class' => 'hidden-xs'),
							$stock->name,
							array('data' =>  '<small><i>'.$stock->description.'</i></small>', 'class' => 'hidden-xs'),
							array('data' =>  '<b>'.$stock->quantity.'</b> '. '<small>'.$unid[$stock->unid].'</small>', 'style' => 'color:'.$color.';'),														
							stockLink("stock/update/".$this->general->url_encode($stock->id)),
							deleteLink("stock/delete/".$this->general->url_encode($stock->id), $stock->name));
				endforeach;
				echo $this->table->generate();
				echo modal();
				if(@$pagination)echo @$pagination;
?>
</div>

</form>
</div>