<?php $id_verific = $this->general->url_decode($this->uri->segment(3)); ?>
<div class="row">
	<div class="col-md-10 col-md-offset-1">
	<form id="item" role="form" action="" method="POST">
		<h1><?php echo $titulo; ?></h1>

		<?php $this->message->quick_message_generate(); ?>

		<div class="row">
			<div class="col-md-12">

				<?php 
					control("savebt");
					
					if($id_verific != NULL):
						control("add", array('label'=> 'Cadastrar Novo', 'class'=>'hidden-sm hidden-xs'));
					endif;
					
					control("list", array('label' => 'Estoque', 'href'=>'stock/'));
					
					if($id_verific != NULL):
						control('other', array('href'=>'stock/report_stock/'.$this->general->url_encode(@$row->id), 'classIcon'=>'glyphicon glyphicon-list-alt', 'label'=> 'Histórico completo'));
					endif;					
				?>
			</div>
		</div>

	<div class="row">
		<div class="col-md-12">
		<?php
			if($id_verific != NULL): 
				$unid = array('Unidade(s)','Kg','g', 'l');
			@$row->description == NULL ? $descProd = NULL : $descProd = '('.@$row->description.')';
				echo'<hr/>';
				echo '<h3><b>'.@$row->name.'</b> <small>'.$descProd .'</small></h3>'.'<h5><b>Quantidade mínima:</b> <i>'.@$row->quantityMin.' '.$unid[@$row->unid].'</i></h5>';
				echo'<hr/>';
			endif;
		?>
			<fieldset>
				<div class="row">
					<input id="id" name="id" type="hidden" value="<?php echo @$row->id; ?>">
					<input id="id_itens" name="id_itens" type="hidden" value="<?php echo @$row->id_itens; ?>">
					<?php if($id_verific == NULL):?>
					<div class="col-md-6">
						<div class="form-group">  							
  								<?php
  									echo form_label('Nome');
  									echo form_input(array('name'=>'name', 'id'=>'name', 'placeholder'=>'Nome', 'class'=>'form-control'), set_value('name', @$row->name));
  									echo form_error("name");  									 									
  								?>
						</div>
					</div>
					
					<div class="col-md-6">
						<!-- Text input-->
						<div class="form-group">
  							<?php 
  								echo form_label('Descrição');
  								echo form_input(array('name'=>'description', 'id'=>'description', 'placeholder'=>'Descrição', 'class'=>'form-control'), set_value('description', @$row->description));
  								echo form_error("description");
  							?>
						</div>
					</div>
					<?php endif; ?>			
					
				</div><!-- row -->

				<div class="row">
					<?php if($id_verific != NULL):?>
					<div class="col-md-2 col-xs-5 col-sm-5">
						<!-- Text input-->
						<div class="form-group">
  							<?php 
  								echo form_label('Quantidade');
  								echo form_input(array('name'=>'quantity', 'id'=>'quantityA', 'placeholder'=>'0', 'type'=>'text', 'class'=>'form-control'), set_value('quantity', 0));
  								echo form_error("quantity");
  								echo form_hidden('name', @$row->name);
  								echo form_hidden('quantityMin', @$row->quantityMin);
  								echo form_hidden('description', @$row->description);
  								echo form_hidden('unid', @$row->unid);
  							?> 							
						</div>
					</div>
					
					<div class="col-md-3">
						<!-- Text input-->
						<div class="form-group">
  							<?php 								
  								control('buttonQuant', array('label'=>'Acrescentar', 'id'=>'addQuant', 'class'=>'btquant', 'classIcon'=>'glyphicon glyphicon-chevron-up'));
  								control('buttonQuant', array('label'=>'Diminuir', 'id'=>'remQuant', 'class'=>'btquant', 'classIcon'=>'glyphicon glyphicon-chevron-down'));
  							?>							
  							
						</div>
					</div>
					
					<?php 
						endif;
						if($id_verific == NULL):?>
									
					<div class="col-md-2 col-xs-5 col-sm-5">
						<!-- Text input-->
						<div class="form-group">
  							<?php 
  								$id_verific == NULL ? $quantityMin_init = 150 : $quantityMin_init = @$row->quantityMin;
  								echo form_label('Qtde. mínima');
  								echo form_input(array('name'=>'quantityMin', 'id'=>'quantityMin', 'placeholder'=>'0', 'type'=>'text', 'class'=>'form-control'), set_value('quantityMin', $quantityMin_init));
  								echo form_error("quantityMin");
  							?>
						</div>
					</div>
					
					<div class="col-md-3">
						<!-- Text input-->
						<div class="form-group">
  							<?php 								
  								control('buttonQuant', array('label'=>'Acrescentar', 'id'=>'addQuantMin', 'class'=>'btquant', 'classIcon'=>'glyphicon glyphicon-chevron-up'));
  								control('buttonQuant', array('label'=>'Diminuir', 'id'=>'remQuantMin', 'class'=>'btquant', 'classIcon'=>'glyphicon glyphicon-chevron-down'));
  							?>							
  							
						</div>
					</div>
  					
  					<div class="col-md-3 col-xs-12 col-md-offset-1">
						<!-- Text input-->
						<div class="form-group">
  							<?php 
  								echo form_label('Unid. de medida');
  								$options = array(
  											0=> 'Unidade(s)',
  											1=> 'Kg (Quilogramas)',
  											2=> 'g (Gramas)',
  											3=> 'l (Litros)'
  								);
  								$attr = 'id="unid" class="form-control"';
  								$config = array(set_value('unid', @$row->unid));
  								echo form_dropdown('unid', $options, $config, $attr);
  								echo form_error("unid");
  							?>
						</div>
					</div>
  					<?php endif; ?>
					<!-- <div class="col-md-3">
						<div class="form-group"> -->		
							<?php
								/* echo form_label('Item associado');
					
								$options_itens[' ']='Nenhum';
								foreach(@$query->result() as $itens):
									$options_itens[$itens->id] = '('.$itens->nameEcf.') - '.$itens->name;					
								endforeach;

								$disabled_itens = @$query->num_rows() == 0 ? 'disabled' : ''; 
								$attr_itens = 'id="id_itens" class="form-control"'.$disabled_itens;				
								$config_itens = array(set_value('id_itens', @$row->id_itens));
				
								echo form_dropdown('id_itens', $options_itens, $config_itens, $attr_itens);					
								echo form_error('id_itens'); */
							 ?>
						<!-- </div>							
					</div> -->
					
				</div><!-- Row -->

			</fieldset>			
			
			
			<hr/>
			
		</div>
		
		<?php
			$this->table->set_template(array ( 'table_open'  => '<table class="table">' ));
			$this->table->set_heading('Data', 'Hora', 'Quantidade', array('data' =>  'Observação', 'class' => 'hidden-xs'));
				foreach(@$regStock->result() as $regStock):
				if($regStock->Aquantity <= $regStock->SquantityMin && $regStock->Aquantity != 0):$color='#FFFF00';elseif($regStock->Aquantity == 0):$color='#FF0000';elseif($regStock->Aquantity > $regStock->SquantityMin):$color='#00FF00';else:$color='#FFFFFF';endif;
					 
					$this->table->add_row(
							$this->format_date->mysql_to_data_standard($regStock->Aupdated),
							$this->format_date->mysql_to_time_standard($regStock->Aupdated),
							array('data' =>  '<b>'.$regStock->Aquantity.'</b> '. '<small>'.$unid[$regStock->Sunid].'</small>', 'style' => 'color:'.$color.';'),														
							array('data' =>  $regStock->Aobservation, 'style' => 'color:'.$regStock->Acolor.';font-size: 0.7em;font-style: italic;', 'class' => 'hidden-xs'));
				endforeach;
				echo $this->table->generate();				
				if(@$pagination)echo @$pagination;
		?>
		
	</div><!-- row -->
	
	</form>
	
	</div>
	</div>
	
