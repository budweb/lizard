<div class="row">

<div class="col-md-8 col-md-offset-2">
<h1><?php echo $titulo;?></h1>

<?php $this->message->quick_message_generate(); ?>

<div class="row">
	<div class="col-md-12">
		<div role="tabpanel">

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="<?php echo base_url("sales") ?>" role="tab">Vendas</a></li>
    <li role="presentation"><a href="<?php echo base_url("sales/register") ?>"role="tab">Registrar vendas</a></li>
  </ul>

</div>

	</div>
</div>
 <div class="row">

 	<form name="filter" action="<?php echo base_url('sales'); ?>"method="get">
 <div class="col-md-3">
<div class="form-group">
    <label for="dateStart">Data inicial</label>
    <input type="date" class="form-control" name="dateStart" id="dateStart" value="<?php echo @$this->input->get('dateStart'); ?>">
</div>
</div>

<div class="col-md-3">
<div class="form-group">
    <label for="dateEnd">Data Final</label>
    <input type="date" class="form-control" name="dateEnd" id="dateEnd" value="<?php echo @$this->input->get('dateEnd'); ?>">
</div>
</div>
<div class="col-md-2">
<div class="form-group">
	 <label for="dateEnd"></label>
       <input type="submit" class="form-control btn btn-primary " value="Filtrar">
</div>
</div>
</form>

</div>

<?php

$this->table->set_template(array ( 'table_open'  => '<table class="table">' ));
$this->table->set_heading('Usuário', 'Mesa','Data', 'hora', 'Valor');

	$total = 0;
foreach($query->result() as $row):
	$total += $row->total;
	
	$this->table->add_row(
			$row->firstName." ".$row->lastName,
			$row->mesa,
			mdate("%d-%m-%Y", mysql_to_unix($row->finished)),
			mdate("%H:%i:%s", mysql_to_unix($row->finished)),
			"R$ ".priceFormat($row->total)
			);
endforeach;

$this->table->add_row('','','','Subtotal', 'R$ '.priceFormat($total));
echo $this->table->generate();


?>
<?php 
echo modal();
?>


</div>

</div>