<div class="row">

<div class="col-md-8 col-md-offset-2">
<h1><?php echo $titulo;?></h1>

<?php $this->message->quick_message_generate(); ?>

<div class="row">
	<div class="col-md-12">
		<div role="tabpanel">

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation"><a href="<?php echo base_url("sales") ?>" role="tab">Vendas</a></li>
    <li role="presentation" class="active"><a href="<?php echo base_url("sales/register") ?>"role="tab">Registrar vendas</a></li>
  </ul>

</div>

	</div>
</div>

<form action="<?php echo base_url("sales/register_new_sale"); ?>" method="get">
<div class="row">
	<div class="col-md-3 col-md-offset-9">
		<input type="submit" class="btn btn-primary" type="submit" value="Registrar Vendas">
</div>
<?php

$this->table->set_template(array ( 'table_open'  => '<table class="table">' ));
$this->table->set_heading('Usuário', 'Mesa','Data', 'hora', 'Valor');
	
	$total = 0;
foreach($query->result() as $row):
	$total += $row->total;
	
	$this->table->add_row(
			$row->firstName." ".$row->lastName,
			$row->mesa,
			mdate("%d-%m-%Y", mysql_to_unix($row->finished)),
			mdate("%H:%i:%s", mysql_to_unix($row->finished)),
			"R$ ".priceFormat($row->total)
			);
endforeach;
$this->table->add_row('','','','Total', 'R$ '.priceFormat($total));
echo $this->table->generate();


?>
<div class="row">
	<div class="col-md-3 col-md-offset-9">
		<input type="submit" class="btn btn-primary" type="submit" value="Registrar Vendas">
</div>
</div>

<input type="hidden" name="value" value="<?php echo @$total; ?>">
<input type="hidden" name="created" value="<?php echo $date_time; ?>">

</form>

</div>