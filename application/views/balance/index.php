<div class="row">

<div class="col-md-8 col-md-offset-2">
<h1><?php echo $titulo;?></h1>

<?php $this->message->quick_message_generate(); ?>

<div class="row">
	<div class="col-md-12">
		<div role="tabpanel">

  <!-- Nav tabs -->
  <?php
  	$url = $this->uri->segment(2);
  	$url = $url == "" ? "0" : $url;
  	$active[$url] = "active";
  ?>
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="<?php echo @$active["0"]; ?>"><a href="<?php echo base_url("balance") ?>" role="tab">Hoje</a></li>
    <li role="presentation" class="<?php echo @$active["yesterday"]; ?>"><a href="<?php echo base_url("balance/yesterday") ?>"role="tab">Ontem</a></li>
    <li role="presentation" class="<?php echo @$active["personality"]; ?>"><a href="<?php echo base_url("balance/personality") ?>"role="tab">Personalizado</a></li>
  </ul>

</div>

	</div>
</div>

<?php if($url == "personality") : ?>

<div class="row">

 	<form name="filter" action="<?php echo base_url('balance/personality'); ?>" method="post">
 <div class="col-md-3">
<div class="form-group">
    <label for="date">Escolha uma data</label>
    <input type="date" class="form-control" name="date" id="date" value="<?php echo @$this->input->post('date'); ?>">
</div>
</div>
<div class="col-md-2">
<div class="form-group">
	 <label for="dateEnd"></label>
       <input type="submit" class="form-control btn btn-primary " value="Encontrar">
</div>
</div>
</form>

</div>

<?php endif; ?>

 <div class="row">
 <div class="col-md-4 col-md-offset-8">
<small>Saldo Anterior:</small> <big><strong>R$ <?php echo priceFormat($balance); ?></strong></big>
</div>
</div>

<?php

$this->table->set_template(array ( 'table_open'  => '<table class="table">' ));
$this->table->set_heading('Descrição', 'Data', 'Hora', 'Entrada', 'Saída', 'Saldo');

foreach($query->result() as $row):
	if($row->type > 0){
		$inflow = 'R$ '.priceFormat($row->value);
		$outflow = "--";
		$balance += $row->value; 
	}else{
		$inflow = "--";
		$outflow = 'R$ '.priceFormat($row->value);
		$balance -= $row->value;
	}

	$this->table->add_row(
			$row->description,
			mdate("%d-%m-%Y", mysql_to_unix($row->created)),
			mdate("%H:%i:%s", mysql_to_unix($row->created)),
			$inflow,
			$outflow,
			"R$ ".priceFormat($balance));
endforeach;

echo $this->table->generate();


?>



</div>

</div>