<div class="row">
	<form class="form" method="post" action="">
		<div class="col-md-10 col-md-offset-1">
			<h1><?php echo $titulo; ?></h1>
			<?php $this->message->quick_message_generate(); ?>
			<div class="row">
				<div class="col-md-12">
					<?php						
						control("add", array('label'=> 'Novo')); 
						control("selectAll");
						control("deleteLot", array('href' => 'user/deletens/'));
					?>
				</div>
			</div>
			<?php
				$this->table->set_template(array ( 'table_open'  => '<table class="table">' ));
				$this->table->set_heading(array('data' =>  '', 'class' => 'hidden-xs'), 'Nome', array('data' => 'Telefone', 'class' => 'hidden-xs'), array('data' =>  'Cargo', 'class' => 'hidden-xs'), '', '');
				foreach(@$users->result() as $users):
					$this->table->add_row(
							array('data' =>  checkEdit($this->general->url_encode($users->id)), 'class' => 'hidden-xs'),
							$users->firstName.' '.$users->lastName,
							array('data' => '('.$users->ddd.') '.$users->phone, 'class' => 'hidden-xs'),
							array('data' => $this->session_logged->typeUser($users->type), 'class' => 'hidden-xs'),							
							updateLink("user/update/".$this->general->url_encode($users->id)),
							deleteLink("user/delete/".$this->general->url_encode($users->id), $users->firstName));
				endforeach;
				echo $this->table->generate();
				echo modal();
				if(@$pagination)echo @$pagination;
			?>
		</div>
	</form>
</div>