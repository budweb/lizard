<?php 
$id = $this->general->url_decode($this->uri->segment(3));

if($id != $this->session->userdata('id')):
redirect('user/');
endif;
?>

<form name="contato" action="" method="POST" class="form-horizontal">
 
 <legend><h2><?php echo $titulo;?></h2></legend>
 <div class="row" style="padding: 0 0 1% 0;">
	<div class="col-md-4 col-md-offset-4">

		<button type="submit" class="options">
			<span class="glyphicon glyphicon-floppy-disk"></span>
			<label>Alterar senha</label>
		</button>

		<?php 
			control("back", array('label'=>'Cancelar', 'classIcon'=>'glyphicon glyphicon-th-list'));
			//control("other", array('label' => 'Retornar', 'href'=>'main', 'classIcon'=>'glyphicon glyphicon-th'));
		?>
	</div>
</div>

<input id="id" name="id" type="hidden" value="<?php echo $this->general->url_encode($this->session->userdata('id')); ?>">
	
  <div class="row">
  	<div class="col-md-4 col-md-offset-4">
<div class="form-group">
<!-- Text input-->
  <label for="oldPass">Senha antiga</label>  
  <?php echo form_password(array('name'=>'oldPass', 'id'=>'oldPass', 'placeholder'=>'Senha Antiga', 'class'=>'form-control'), set_value('oldPass')); ?>
  
</div>
<small style="color:#f80d1a;font-size: 0.7em;font-style: italic;">
<?php echo form_error("oldPass"); 
if($this->session->flashdata('oldPass')):
echo $this->session->flashdata('oldPass');
endif;
?></small>
</div>
</div>

<div class="row">
  	<div class="col-md-4 col-md-offset-4">
<div class="form-group">
<!-- Text input-->
  <label for="password">Nova senha</label>  
  <?php echo form_password(array('name'=>'password', 'id'=>'password', 'placeholder'=>'Nova Senha', 'class'=>'form-control'), set_value('password')); ?>
</div>
<?php echo form_error("password"); ?>
</div>
</div>

<div class="row">
  	<div class="col-md-4 col-md-offset-4">
<div class="form-group">
<!-- Text input-->
  <label for="rPass">Repita a senha</label>  
  <?php echo form_password(array('name'=>'rPass', 'id'=>'rPass', 'placeholder'=>'Repita a nova senha', 'class'=>'form-control'), set_value('rPass')); ?>
</div>
<?php echo form_error("rPass");?>
</div>
</div>
  

  
</form>
