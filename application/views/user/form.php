<?php 
$id = $this->general->url_decode($this->uri->segment(3));

if($id!=NULL):
$row = $this->user->get_byid($id)->row();
endif;
?>
<form action="" method="POST" enctype="multipart/form-data" id="form_user">
	<div class="row">
	<fieldset>
		<legend><h2><?php echo '<b>'.@$row->firstName.' '.@$row->lastName.'</b> <br/><small>('.$titulo.')</small>'; ?></h2></legend>
		<div class="row">
			<div class="col-md-12">
				<?php				 
					control("savebt");
					$this->session->userdata('id') == $id ? control('cancel') : control("other", array('label' => 'Listar', 'href'=>'user/', 'classIcon'=>'glyphicon glyphicon-th-list'));										
				?>				
			</div>
		</div>
	<div class="col-md-8">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
     				<?php
     					echo form_hidden('function', 'cad_user');
     					echo form_hidden('cpf_bd', md5(@$row->cpf));
     					echo form_hidden('password_bd', @$row->password);
  						echo form_hidden('id', $this->general->url_encode(@$row->id));
  						echo form_label('Nome'); 
  						echo form_input(array('name'=>'firstName', 'id'=>'firstName', 'placeholder'=>'Nome', 'class'=>'form-control'), set_value('firstName', @$row->firstName), 'autofocus'); 
  						echo form_error("firstName");
  					?>
				</div>
			</div>
	<div class="col-md-6">
		<div class="form-group">			
			<?php 
				echo form_label('Sobrenome');
				echo form_input(array('name'=>'lastName', 'id'=>'lastName', 'placeholder'=>'Sobrenome', 'class'=>'form-control'), set_value('lastName', @$row->lastName));
				echo form_error("lastName");
			?> 			
		</div>
	</div>
		</div>
	
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<?php 
					echo form_label('E-Mail');
					echo form_input(array('name'=>'email', 'id'=>'email', 'placeholder'=>'E-Mail', 'class'=>'form-control'), set_value('email', @$row->email));
					echo form_error("email");
				?>
  			</div>
		</div>
		<div class="col-md-2">
				<div class="form-group">
				<?php 
					echo form_label('DDD');
					echo form_input(array('name'=>'ddd', 'id'=>'ddd', 'placeholder'=>'DDD', 'class'=>'form-control'), set_value('ddd', @$row->ddd));
					echo form_error("ddd");
				?>  
				</div>
		</div>
	<div class="col-md-4">
		<div class="form-group">
			<?php 
				echo form_label('Telefone');
				echo form_input(array('name'=>'phone', 'id'=>'phone', 'placeholder'=>'Telefone', 'class'=>'form-control'), set_value('phone', @$row->phone));
				echo form_error("phone");
			?>			
		</div>
	</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
				<?php 
					echo form_label('CPF');
					echo form_input(array('name'=>'cpf', 'id'=>'cpf', 'placeholder'=>'CPF', 'class'=>'form-control'), set_value('cpf', @$row->cpf));
					echo form_error("cpf");
				?>
			</div>
		</div>
		
	<div class="col-md-3">
		<div class="form-group">		
			<?php 
				echo form_label('Tipo');
				$options = $this->session_logged->typeUser();
				$disabled = @$row->type == 0 && @$row->id != NULL ? 'disabled' : '';
				$attr = 'id="type" class="form-control"'.$disabled;				
				$config = array(set_value('type', @$row->type));
				
				echo form_dropdown('type', $options, $config, $attr);
				echo form_error("type");
			 ?>
		</div>		
	</div>
		
	<!-- <div class="col-md-3">
		<div class="form-group"> -->
			 <?php 
				/* if($id==NULL):
					echo form_label('Senha');
					echo form_password(array('name'=>'password', 'id'=>'password', 'placeholder'=>'Senha', 'class'=>'form-control'));
					echo form_error("password");
				endif;*/ 
			?> 
  		<!-- </div>
	</div> -->
		
	<!-- <div class="col-md-3">
		<div class="form-group"> -->
			<?php 
				/* if($id==NULL):
					echo form_label('Repita a senha');
					echo form_password(array('name'=>'passwordr', 'id'=>'passwordr', 'placeholder'=>'Repita a Senha', 'class'=>'form-control'));
					echo form_error("passwordr");
				endif;*/ 
			?>  
  		<!-- </div>
	</div> -->	
	</div>
	<div class="row">
		<div class="col-md-3 col-md-offset-9">
		</div>
	</div>
	</div>
		
	<div id="photo-user" class="col-md-4">
			<a href="../remove_photo/<?php echo $this->general->url_encode($id); ?>" class="glyphicon glyphicon-remove remove_photo" id="remove_photo"></a>
			<a class="dropdown-toggle" onclick="photo.click()" data-toggle="dropdown">			
			<img src="<?php if($id == NULL || $row->photo == (NULL || '0' || $row->photo == base_url().'public/photos/thumbs/')){echo base_url().'public/images/no-photo.jpg';} else{echo base_url().'public/photos/thumbs/'.@$row->photo;} ?>" class="img-rounded" id="file_user" alt="" />
		</a>
		<?php if(@$id != NULL):?>		
			<div class="form-group hidden"> 				
				<input id="photo" name="photo" class="input-file" type="file">			
			</div>
		<?php 
			endif;
			echo form_error("photo");
		?>
		
		<small style="color:#f80d1a;font-size: 0.7em;font-style: italic;">
		<?php  
			if($this->session->flashdata('error_photo')):
				echo $this->session->flashdata('error_photo');
			endif;
		?>
		</small>	
	</div>		
	</fieldset>
	</div>
</form>

<?php if(@$row->id != NULL && $id != $this->session->userdata('id')):?>
	<form action="" method="POST" enctype="multipart/form-data" id="form_user">			
		<?php
			echo form_hidden('function', 'reset_pass');			
			echo form_hidden('id', $this->uri->segment(3));
			if(@$row->password == md5(@$row->cpf)):
					$label = 'Senha Insegura';
					$classIcon = 'glyphicon glyphicon-alert';
					$other_disabled = 'disabled';
					$style = 'background-color:red;'; 
				else:
					$label = 'Resetar a Senha';
					$classIcon = 'glyphicon glyphicon-lock';
					$other_disabled = NULL;
					$style = NULL;
				endif;
			control("button", array('label' => $label, 'href'=>'user/', 'classIcon'=>$classIcon, 'style'=>$style, 'other'=>$other_disabled)); 
		?>		
	</form>
<?php endif;?>