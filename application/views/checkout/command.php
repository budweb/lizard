

<div class="row">
	<div class="col-md-4 command">
<?php


foreach ($itens->result() as $row){
	$this->table->add_row(
			str_pad($row->cod, 6, "0", STR_PAD_LEFT)."<br />"."<small>".$row->qty." ".unid($row->unid)."</small>",
			$row->nameEcf."<br>R$ ".priceFormat($row->price),
			"R$ ".number_format($row->price*@$row->qty, 2, ',', '')
	);
	$total = $row->price*@$row->qty + @$total;
}


//recebe os adicionais



foreach ($additionals->result() as $row){
	$this->table->add_row(
			'',
			$row->description,
			"R$ ".number_format($row->price, 2, ',', '')
	);
	$total = $row->price + @$total;
}

//taxa de serviço 10%


$row = $serviceCost->row();

if($row->serviceCost != NULL){
	$serviceCost = @$total*10/100;
	$this->table->add_row(
			'',
			'Tx de serviço',
			"R$ ".number_format($serviceCost, 2, ',', '')
			
	);

	$total = @$total+$serviceCost;
}

// $this->table->add_row(
// 		'',
// 		'Total',
// 		'R$ '.number_format(@$total, 2, ',', '')
		
// );
echo $this->table->generate();
?>
</div>
<form name="values" id="valuesForm" action="<?php echo base_url("checkout/finished"); ?>">

	<input type="hidden" name="total" value="<?php echo @$total; ?>"/>
	<input type="hidden" name="id_demands" value="<?php echo $this->uri->segment(3); ?>"/>
<div class="col-md-7">
	<div class="row">
	<label class="col-md-2 col-md-offset-6">Total:</label>
	<div class="valores total col-md-4">
		R$
			<?php echo number_format(@$total, 2, ',', ''); ?>
	</div>
	</div>

	<div class="row">
	<div class="col-md-3 col-md-offset-5">
		<label><input type="radio" name="paymethod" checked="checked" value='0' /> Dinheiro:</label> <br />
		<label><input type="radio" name="paymethod" value='1' /> Cartão:</label>
	</div>
	<div class="valores dinheiro col-md-4">
		<input name="dinheiro" id="dinheiro" type="number" rel="money" class="form-control" autofocus />
	</div>
	</div>

	<div class="row">
	<label class="col-md-2 col-md-offset-6">troco:</label>
	<div class="valores troco col-md-4">
	R$ 0,00
			
	</div>
	</div>
	<div class="row">
		<div class="col-md-4 col-md-offset-8">
	<button type="submit" class="btn btn-success btn-lg btn-block" role="button">Fechar pedido <span class="glyphicon glyphicon-check"></span></button>
		</div>
	</div>
</div>
</form>

</div>