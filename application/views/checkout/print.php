<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Imprimir Documento</title>
	<style>
	@font-face { 
		font-family: 'antonio-regular'; 
		src: url('../fonts/antonio-regular.ttf'); 
 	} 
	*{

			margin:0;
			padding: 0;
	}
	body{
		font-family: "antonio-regular";
		font-size: 10pt;
		background: white;
		/*letter-spacing: -0.2mm;*/
	}
		.paper{
			width: 78mm;
		}
		.margin{
			width:70mm;
			margin: 12mm 4mm;
		}
		.info{
			border-bottom: 1px solid #000;
		}
		.info .f_name{font-size: 1.35em; font-weight: bold; text-align: center; }
		.info .name{font-size: 0.9em; text-align: center;}
		.info .address{font-size: 0.8em; line-height: 8pt; margin: 5pt 0; text-align: left;}
		.title{
			text-align: center;
			font-weight: bold;
			font-size: 1.35em;
			text-transform: uppercase;
		}
		.legends{
			font-size: 0.8em;
			border-bottom: 1px #000 solid;
			padding: 1mm 0;
			text-transform: uppercase;
		}
		.legends ul{list-style: none;}
		.legends ul li{float: left; margin-right: 6mm;}

		.items{font-size: 1.1em; border-bottom: 1px #000 dotted;}
		.items td{ vertical-align: top; }
		.item{font-weight: bold; margin-right:1.5mm;}
		.descr{margin-right:1.5mm; }

		.result table{width:70mm;}
		.result table tr td:last-child{text-align: right;}
		.total_label{font-weight: bold; font-size: 1.4em;}
		.total{font-size: 1.6em; }

		.bye{text-align: center;}
		.serviceCost{font-size: 0.9em;}

		.comanda{border-top: 1px solid #000; margin-top:5pt; }


		.nowrap{
			white-space: nowrap;
		}
	</style>
</head>
<body>
	<?php 
	$demands = $demands->row();
	$prop = $property->row();
	?>


<div class="paper">
	<div class="margin">
		<div class="info">
			<div class="f_name"><?php echo $prop->fantasyName; ?></div>
			<div class="name"><?php echo $prop->name; ?></div>
			<div class="address">
				<?php echo $prop->address.", ".$prop->district; ?><br />
				<?php echo $prop->city." - ".$prop->state; ?><br />
				<?php echo $prop->zipCode; ?>
			</div>
			<div class="others">
				<strong>CNPJ: </strong><?php echo $prop->cnpj; ?><br />
				<strong>Phone: </strong><?php echo "(".$prop->ddd.") ".$prop->phone; ?><br />
			</div>
		</div>
		<div class="datetime"><?php echo mdate("%d/%m/%Y %H:%i:%s", mysql_to_unix($demands->finished)); ?></div>
		<div class="title">Cupom NÃO Fiscal</div>
		<div class="legends">
		<ul>
			<li>Ítem</li> 
			<li>Código</li>
			<li>descrição</li>
			<br />
			<li>QTD.</li>
			<li>UN.</li>
			<li>Vl Unit R$</li>
			<li>Vl Ítem R$</li>
		</ul> 
	<div style="clear:both"></div>
	</div>

<div class="items">
<?php

$i = 0;
foreach ($itens->result() as $row){
	$i++;
	$this->table->add_row(
			"<span class='item'>".str_pad($i, 3, "0", STR_PAD_LEFT)."</span> ",
			"<span class='descr'>".str_pad($row->cod, 6, "0", STR_PAD_LEFT)."   ".
			$row->nameEcf." "."<span class='nowrap'>".
			$row->qty."  ".
			substr(unid($row->unid), 0, 2)."  X ".
			priceFormat($row->price)."</span></span>",
			number_format($row->price*@$row->qty, 2, ',', '')
	);
	$total = $row->price*@$row->qty + @$total;
}


//recebe os adicionais



foreach ($additionals->result() as $row){
	$this->table->add_row(
			'',
			$row->description,
			"R$ ".number_format($row->price, 2, ',', '')
	);
	$total = $row->price + @$total;
}

//taxa de serviço 10%



if($demands->serviceCost != NULL){
	$serviceCost = @$total*10/100;
	$this->table->add_row(
			'',
			'Tx de serviço',
			number_format($serviceCost, 2, ',', '')
			
	);

	$total = @$total+$serviceCost;
}
echo $this->table->generate();
?>
</div>
<div class="result">
	<?php 
		$dinheiro = $demands->money;
		$dinheiro = $dinheiro >= $total ? $dinheiro : $total;
		$troco = $dinheiro - $total;
	?>
<table>
	<tr>
		<td><span class="total_label">Total R$</span></td>
		<td><div class="total"><?php echo number_format(@$total, 2, ',', ''); ?></div>	</td>
	</tr>
	<tr>
		<td><span class="">Dinheiro</span></td>
		<td><div class=""><?php echo number_format($dinheiro, 2, ',', ''); ?></div>	</td>
	</tr>
	<tr>
		<td><span class="">Troco</span></td>
		<td><div class=""><?php echo number_format($troco, 2, ',', ''); ?></div>	</td>
	</tr>		
</table>

</div>

<div class="bye">Obrigado - Volte Sempre</div>
<?php
if($demands->serviceCost != NULL):
?>
<div class="serviceCost">Está sendo cobrada taxa de serviço de 10%</div>
<?php
endif;
?>
<div class="comanda">
	<strong>Nº Comanda: </strong><?php echo str_pad($demands->id, 9, "0", STR_PAD_LEFT); ?><br />
	<?php 
	$user = $this->user->get_user_per_id($demands->user_fk);
	 ?>
	<strong>U. Atend: </strong><?php echo $user->firstName." ".$user->lastName; ?><br />
	<strong>Emissão: </strong><?php echo mdate("%d/%m/%Y %H:%i:%s") ?></div>

</div>  <!-- margin -->
</div>  <!-- paper -->

</body>
</html>