<h2><?php echo $titulo.'<b>'.@$row->firstName.' '.@$row->lastName.'</b>';?></h2>
<div class="row">
	<div class="col-md-12">
		<?php
			control('other', array('label'=> 'Consumação', 'href' => 'consume', 'classIcon' => 'glyphicon glyphicon-scale'));
			control('other', array('label'=> 'Rendimento dos Funcionários', 'href' => 'worker', 'classIcon' => 'glyphicon glyphicon-user'));
			control('other', array('label'=> 'Mercadoria em estoque', 'href' => 'stuff', 'classIcon' => 'glyphicon glyphicon-check'));						
		?>
	</div>
</div>
<br/>

<!-- ============================================================================================= -->

<div role="tabpanel">

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Mensal</a></li>
    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Semanal</a></li>
    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Diário</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home">
    
    <script type="text/javascript">                                        
        	var randomnb = function(){ return Math.round(Math.random()*300)};
            var options = {
                responsive:true
            };

            var data = {
                 labels: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
                  datasets: [
                      {
                        label: "Dados primários",
                         fillColor: "rgba(220,220,220,0.5)",
                         strokeColor: "rgba(220,220,220,0.8)",
                         highlightFill: "rgba(220,220,220,0.75)",
                         highlightStroke: "rgba(220,220,220,1)",
                         data: [randomnb(), randomnb(), randomnb(), randomnb(), randomnb(), randomnb(), randomnb(), randomnb(), randomnb(), randomnb(), randomnb(), randomnb()]
                       },
                       {
                         label: "Dados secundários",
                         fillColor: "rgba(151,187,205,0.5)",
                         strokeColor: "rgba(151,187,205,0.8)",
                         highlightFill: "rgba(151,187,205,0.75)",
                         highlightStroke: "rgba(151,187,205,1)",
                         data: [28, 48, 40, 19, 86, 27, 90, randomnb(), randomnb(), randomnb(), randomnb(), randomnb()]
                       }
                   ]
               };                

             window.onload = function(){
             var ctx = document.getElementById("GraficoBarra").getContext("2d");
             var BarChart = new Chart(ctx).Bar(data, options);
             }           
</script>
    
    <h3>Análise do Faturamento <small>(Análise dos meses anteriores)</small></h3>

<div class="row">
	<div class="col-md-6">
		<div class="box">
			<div class="box-chart">
	    		<canvas id="GraficoBarra" style="width:100%;"></canvas> 	          
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<h4>Legenda: </h4>
		<div class="form-group">
			<div class="legend_mouth"></div><small class="descricao_legend">Gastos</small>
		</div>
		<div class="form-group">
			<div class="legend_med"></div><small class="descricao_legend">Ganhos</small>
		</div>
	<hr/>
	<h4><u>Observações:</u> </h4>
	<h5><b>Melhor mês: </b> <i>Julho</i></h5><h5><b>Pior mês: </b> <i>Fevereiro</i></h5>

	</div>
</div>
    
    
    </div>
    
    <div role="tabpanel" class="tab-pane" id="profile"><label>Semanal</label></div>
    
    <div role="tabpanel" class="tab-pane" id="messages"><label>Diário</label></div>
   </div>

</div>
