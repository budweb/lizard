
<div class="row">
<form class='form' method="post" action="">
<div class="col-md-8 col-md-offset-2">
<h1><?php echo $titulo; ?></h1>

<?php $this->message->quick_message_generate(); ?>

 <div class="row">
 <div class="col-md-12">
<?php 
control("add", array('label'=> 'Novo Ítem'));
control("other", array(
					'href' => 'itens/categories',
					'label' => 'Categorias',
					'classIcon' => 'glyphicon glyphicon-tags')); 
control("selectAll");
control("deleteLot", array('href' => 'itens/deleteLot/'));
?>
</div>
</div>
<div class="row">
<div class="col-md-4">
<?php search($search);?>
</div>
</div>
<?php
$this->table->set_template(array ( 'table_open'  => '<table class="table">' ));
$this->table->set_heading(array('class' => 'hidden-xs'), 
		array('class' => 'hidden-xs','data' => 'Cód'), 
		'Referência', 
		'Preço', 
		array('class' => 'hidden-ce','data' => 'Categoria'), '', '');



foreach($query->result() as $row):
$this->table->add_row(
					array('data' => checkEdit($this->general->url_encode($row->Iid)), 'class' =>'hidden-xs'),
					array('class' => 'hidden-xs','data' => str_pad($row->cod, 5, "0", STR_PAD_LEFT)),
					$row->Iname,
					"<span class='hidden-xs'>R$</span> ".priceFormat($row->price),
					array('class' => 'hidden-ce','data' => "<i class='hidden-xs category-icon-sm-white category-sm-".$row->icon."'></i>".$row->Cname),
					updateLink("itens/update/".$this->general->url_encode($row->Iid)),
					deleteLink("itens/delete/".$this->general->url_encode($row->Iid), $row->Iname));
endforeach;

echo $this->table->generate();


?>

<?php echo modal(); ?>

<?php if($pagination)echo $pagination; ?>
</div>

</form>
</div>