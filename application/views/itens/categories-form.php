<div class="row">
<form role="form" action="" method="POST">
<div class="col-sm-6 col-md-8 col-sm-offset-3 col-md-offset-2">
<h1><?php echo $titulo;?></h1>
<?php 
 echo alert($this->session->flashdata('type'), $this->session->flashdata('message'));
 ?>
<div class="row">
<div class="col-md-12">

<?php control("save");?>

<?php control("add", array('label'=> 'Nova Categoria', 'href'=>'itens/addCategories', 'show-link' => array($this->uri->segment(2), "updateCategories")));?>

<?php control("list", array('label' => 'Categorias', 'href'=>'itens/categories'));?>

</div>
</div>
<div class="row">

<div class="col-md-3">
<div class="form-group">
  <input id="id" name="id" type="hidden" value="<?php echo @$row->id; ?>">

  <label for="name">Nome</label>  
  <input id="name" name="name" type="text" placeholder="Nome da categoria" class="form-control" value="<?php echo @$rowUp->name; ?>">
</div>
</div>
<div class="col-md-7">
<div class="form-group">

  <label for="description">Descriçao</label>  
  <input id="description" name="description" type="text" placeholder="Descriçao" class="form-control" value="<?php echo @$rowUp->description; ?>">
</div>
</div>

<div class="col-md-2">
<div class="form-group">

  <label for="color">Cor</label>  
  <input id="color" name="color" type="color" class="form-control" value="<?php echo @$rowUp->color; ?>">
</div>
</div>

</div>
<label for="icon">Escolha um ícone para representar sua categoria:</label> 
<div class="row">
<?php 

$letras = array("A", "B", "C", "D", "E");


foreach($letras as $letra){

	
	for($i=1; $i<12; $i++){
		
		$select = @$rowUp->icon == $letra.$i? "checked='checked'": "";
		echo "<div class='col-sm-4 col-md-3 category'>";
		echo "<div class='category-icon-block'>";
		echo "<input type='radio' name='icon' class='category-radio' ".$select." value='".$letra.$i."' />";
		echo "<i class='category-icon category-".$letra.$i."'></i>";
		echo "</div></div>";
	}
	
}

?>

</div>


</div>
</form>



</div>