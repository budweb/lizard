<div class="row">
<div class="col-sm-8 col-md-6 col-md-offset-3 col-sm-offset-2">

<h1><?php echo $titulo;?></h1>
<form class="form" action="" method="post">

<?php $this->message->quick_message_generate(); ?>

 <div class="row">
 <div class="col-md-12">
<?php 
 control("add", array('label'=> 'Nova Categoria', 'href' => 'itens/addCategories/'));
 control("list", array('label' => 'Itens', 'href'=>'itens'));
 control("selectAll");
 control("deleteLot", array('href' => 'catDeleteLot/'));
 ?>
</div>
</div>
<div class="row">
<div class="col-md-4">
<?php //search($search);?>
</div>
</div>
<?php

$this->table->set_template(array ( 'table_open'  => '<table class="table">' ));
$this->table->set_heading('', 'Ícone', 'Nome / Descrição', '', '');

foreach($query->result() as $row):
$this->table->add_row(checkEdit($this->general->url_encode($row->id)), 
					"<i class='category-icon-sm-white category-sm-".$row->icon."'></i>",
					$row->name."<br /><small class='text-success'><i>".$row->description."</i></small>",
					updateLink("itens/updateCategories/".$this->general->url_encode($row->id)),
					deleteLink("itens/catDelete/".$this->general->url_encode($row->id), $row->name));
endforeach;

echo $this->table->generate();


?>


<?php 
echo modal();
?>
<?php if($pagination)echo $pagination; ?>
</form>
</div>
</div>