<div class="row">
<div class="col-md-10 col-md-offset-1">
<form id="item" role="form" action="" method="POST">
<h1><?php echo $titulo; ?></h1>

<?php $this->message->quick_message_generate(); ?>

<div class="row">
<div class="col-md-12">


<?php control("save");?>

<?php control("add", array('label'=> 'Novo Ítem', 'href'=>'itens/add', 'show-link' => array($this->uri->segment(2), "update")));?>

<?php control("list", array('label' => 'Ítens', 'href'=>'itens/'));?>
</div>
</div>

<div class="row">
<div class="col-md-8">
<fildset>
<legend>Dados</legend>
<div class="row">
<div class="col-md-3">
<div class="form-group">
  <label for="cod">Código</label>  
  <input id="cod" name="cod" type="text" class="form-control" value="<?php echo @$row->cod; ?>">
</div>
</div>
</div>
<div class="row">
<!-- Text input-->
<div class="col-md-7">
<div class="form-group">
  <input id="id" name="id" type="hidden" value="<?php echo @$row->id; ?>">

  <label for="name">Nome</label>  
  <input id="name" name="name" type="text" placeholder="Nome" class="form-control nameItens" value="<?php echo @$row->name; ?>">
</div>
</div>
<div class="col-md-5">
<!-- Text input-->
<div class="form-group">
  <label for="nameEcf">Nome ECF</label>  
  <input id="nameEcf" name="nameEcf" type="text" placeholder="Nome curto" maxlength="29" class="form-control" value="<?php echo @$row->nameEcf; ?>">
</div>
</div>
</div><!-- row -->

<div class="row">
<div class="col-md-7">
<!-- Text input-->
<div class="form-group">
  <label for="description">Descrição</label>  
  <input id="description" name="description" type="text" placeholder="Descrição" class="form-control" value="<?php echo @$row->description; ?>">
</div>
</div>

<div class="col-md-2">
<!-- Text input-->
<div class="form-group">
  <label for="unid">Unidade</label>  
<select name="unid" id="unid" class="form-control">
<?php 

foreach(unid() as $i => $option):
$selected = $i==$row->unid ? "selected='selected'" : "";
?>
<option value="<?php echo $i; ?>" <?php echo $selected;?>><?php echo $option; ?></option>
<?php endforeach;?>
</select>
</div>
</div>



</div><!-- Row -->
<div class="row">

<div class="col-md-3">
<div class="form-group type">
  <label for="type">Tipo</label> 
  <p>
  <input type="radio" name="type" value="0" <?php echo @$row->type == 0 ? "checked='checked'" : ""; ?> />
  <label for=""><small>Produção</small></label>
  </p>
  <p>
  <input type="radio" name="type" value="1" <?php echo @$row->type == 1 ? "checked='checked'" : ""; ?> />
  <label for=""><small>Auto Serviço</small></label>
  </p>
  <p>
  <input type="radio" name="type" value="2" <?php echo @$row->type == 2 ? "checked='checked'" : ""; ?> />
  <label for=""><small>Estoque</small></label>
  </p>
</div>
</div>

<div class="col-md-3">
<div class="form-group productionTime hide">
  <label for="productionTime"><small>Tempo Prod. </small></label>  
  <input id="productionTime" name="productionTime" type="time" class="form-control" value="<?php echo @$row->productionTime; ?>">
</div>
</div>

<br class="hidden-xs hidden-sm" />

<div class="col-md-3">
<div class="form-group productionTime hide">
<?php
  $checkbox = array(
            'name'=> 'ctrlStock[]',
            'class' => 'checkEdit ctrlStock',
            'checked' => FALSE
            );
            echo form_checkbox($checkbox);
            echo form_label('Ativar estoque', 'ctrlStock[]');
?>
</div>
</div>
<br class="hidden-xs hidden-sm" />
<br class="hidden-xs hidden-sm" />


<div class="col-md-3">
<div class="form-group linkStock hide">
  <label for="linkStock"><small>Quant. Mínima</small></label>  
  <?php
    $id_verific = $this->general->url_decode($this->uri->segment(3));
    $id_verific == NULL || @$item_stock->quantityMin == NULL ? $quantityMin_init = 150 : $quantityMin_init = @$item_stock->quantityMin;
		echo form_input(array('name'=>'quantityMin', 'id'=>'quantityMin', 'type'=>'text', 'class'=>'form-control'), set_value('quantityMin', @$quantityMin_init)); 					
	
   $checkbox = array(
            'name'=> 'controlAtive[]',
            'class' => 'checkEdit controlAtive',
            'value' => @$item_stock->controlAtive == 1 ? TRUE : FALSE,
            'checked' => @$item_stock->controlAtive == NULL || @$item_stock->controlAtive == 0 ? FALSE : @$item_stock->controlAtive
            );
            echo form_checkbox($checkbox);
            echo form_label('<small>Controlar estoque</small>', 'controlAtive[]');           
  ?>
</div>
</div>

<br class="hidden-xs hidden-sm" />

<div class="col-md-4">
   <div class="form-group linkStock hide">
    <?php                 
      control('buttonQuant', array('label'=>'Acrescentar', 'id'=>'addQuantMin', 'class'=>'btquant', 'classIcon'=>'glyphicon glyphicon-chevron-up'));
      control('buttonQuant', array('label'=>'Diminuir', 'id'=>'remQuantMin', 'class'=>'btquant', 'classIcon'=>'glyphicon glyphicon-chevron-down'));
    ?>              
  </div>
</div>

</div>

</fildset>
<fildset>
	<legend>Valores</legend>
<div class="row">	

<div class="col-md-3">
<div class="form-group">
  <label for="cost">Custo</label>
  <div class="input-group">
  <div class="input-group-addon">R$</div>  
  <input id="cost" name="cost" type="number" rel="money" class="form-control" value="<?php echo priceNoFloat(@$row->cost); ?>" cache="<?php echo priceNoFloat(@$row->cost); ?>"  >
  </div>
</div>
</div>
<div class="col-md-3 col-md-offset-3">
<div class="form-group">
  <label for="margin">Margem</label>
  <div class="input-group">
  <div class="input-group-addon">R$</div>  
  <input id="margin" name="margin" type="number" rel="money" class="form-control"  value="<?php echo priceNoFloat(@$row->margin); ?>" cache="<?php echo priceNoFloat(@$row->margin); ?>">
  </div>
</div>
</div>
<div class="col-md-3">
<div class="form-group">
  <label for="marginp">Margem Perc.</label>
  <div class="input-group">
  <input id="marginp" name="marginp" type="number" class="form-control" value="">
  <div class="input-group-addon">%</div> 
  </div>
</div>
</div>
</div><!-- Row -->
<div class="row">
<div class="col-md-3">
<div class="form-group">
  <label for="price">Preço</label>
  <div class="input-group">
  <div class="input-group-addon">R$</div>  
  <input id="price" name="price" type="number" rel="money" class="form-control" value="<?php echo priceNoFloat(@$row->price); ?>" cache="<?php echo priceNoFloat(@$row->price); ?>" >
  </div>
</div>
</div>
	

</div><!-- Row -->
</fildset>
</div>

<div class="col-md-4">
<fildset>
<legend>Categorias</legend>
<table width="100%">
  <?php 
  $this->db->where('id_property', $this->session->userdata('id_property'));
  $categories = $this->db->get('categories');
  foreach($categories->result() as $cat):
  $checked = $cat->id == @$row->category? "checked='checked'": "";
  ?>
  <tr>
 <td><input type="radio" name="category" <?php echo $checked;?> value="<?php echo $cat->id; ?>" /></td>
 <td><i class="category-icon-sm-white category-sm-<?php echo $cat->icon; ?>"></i> 
 <?php echo $cat->name; ?><br />
 <small class="text-success"><i><?php echo $cat->description; ?></i></small>
 </td>
  </tr>

  <?php endforeach;?>

</table>
</fildset>
</div>
</div><!-- row -->

</form>
</div>
</div>

<script>
$(document).ready(function(){
// 	costv
// 	$("#item #cost")
// 	val(price).attr('cache', price).keyup();
});
</script>