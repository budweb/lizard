<!doctype html>
<html lang="pt-BR">
<head>
<meta charset="utf-8">
<title>Lizard<?php echo @$titulo!=NULL? " | ".$titulo: "" ; ?></title>

<meta name="viewport" content="width=320, initial-scale=1">

<link rel='icon' type='image/png' href='<?php echo base_url("public/images/favicon.gif"); ?>'>

<?php

echo link_tag('assets/bootstrap/css/bootstrap.css');
echo link_tag('assets/bootstrap/css/bootstrap-theme.min.css');
echo link_tag('assets/css/style.css');


?>

<!--  library Javascript -->
<?php
// echo script_tag('public/js/less.min.js');
echo script_tag('assets/jquery/jquery-1.11.2.min.js');
// echo script_tag('public/jquery/jquery-2.1.3.min.js');
// echo script_tag('public/jquery/jquery-migrate-1.2.1.min.js');
echo script_tag('assets/jquery/jquery.mobile.custom.min.js');
echo script_tag('assets/bootstrap/js/bootstrap.min.js');
echo script_tag('assets/js/general.js');

//retorna os scripts passados pela forew
if(isset($scripts)){
foreach ($scripts as $script){

	echo script_tag('assets/js/'.$script.'.js');

}
}

?>
<script> /* Provisory for dev environment: */ localStorage.clear(); </script>
</head>

<body>
<div id="base_url" class="hide"><?php echo base_url(); ?></div>
<div id="ajax-loader" class="hide"><img src="<?php echo base_url("assets/images/ajax-loader.gif"); ?>" alt=""></div>
<div class="container">
<div class="row">
<div class="col-xs-6 col-sm-3 col-md-3">
<a href="<?php if($this->session->userdata('type') == '6'): echo base_url('admin/'); else: echo base_url(""); endif; ?>">
<img src="<?php echo base_url("assets/images/logo-white.png"); ?>" alt="" class="logo" />
</a>
</div>
<div id="name-user" class="hidden-xs col-sm-5 col-sm-offset-1 col-md-4 col-md-offset-4">
<?php
$this->db->where('id', $this->session->userdata('id'));
$get = $this->db->get('user');
$get->num_rows == 1;
$data = $get->row_array();
 
$this->session->userdata('type') == '5' ?  print  '<h1>'.$data['userName'].'</h1>' : print '<h1>'.$data['firstName'].' '.$data['lastName'].'</h1>';
echo '<h2>'.$this->session_logged->typeUser($data['type']).'</h2>';
?>
</div>
<div id="icon-user" class="col-xs-offset-3 col-sm-offset-0 col-xs-3 col-sm-2 col-md-1">

<div class="btn-group">
  <a class="dropdown-toggle" data-toggle="dropdown">
<img src="<?php if($this->session->userdata('logged') != TRUE || $this->session->userdata('id') == NULL || @$data['photo'] == (NULL || '0' || @$data['photo'] == base_url('assets/photos/thumbs/'))){echo base_url('assets/images/no-photo.jpg');} elseif(@$data['type'] == '5'){echo base_url('assets/images/cookery.jpg'); } else{echo base_url('assets/photos/thumbs/'.@$data['photo']);}; ?>" class="img-circle img-responsive" alt="" />
  </a>
  <ul class="dropdown-menu pull-right" role="menu">
 	<li class="visible-xs" >
 	<a>
 	<?php 
		echo $data['firstName'].' '.$data['lastName'].' - ';
		echo $this->session_logged->typeUser($data['type']);
		
		$this->session->userdata('type') == 6 ? $patch_link = 'admin' : $patch_link = 'user';
		$this->session->userdata('type') == 6 ? $patch_settings = 'admin/setting' : $patch_settings = 'settings/';
	?>
	</a>
	
 	</li>
 	<?php if($this->session->userdata('type') != '5'):?>
      <br class="hidden-lg hidden-md"/>
    	<li><?php echo anchor($patch_link.'/edit_pass/'.$this->general->url_encode($this->session->userdata('id')), '<span class="glyphicon glyphicon-asterisk"></span> Alterar Senha'); ?></li>
   		<br class="hidden-lg hidden-md"/>
      <li><?php echo anchor($patch_link.'/my_account/'.$this->general->url_encode($this->session->userdata('id')), '<span class="glyphicon glyphicon-pencil"></span> Editar meu usuário'); ?></li>
      <br class="hidden-lg hidden-md"/>
    <?php endif; ?>
    <li><?php echo anchor($patch_settings, '<span class="glyphicon glyphicon-cog"></span> Configurações'); ?></li>
    <br class="hidden-lg hidden-md"/>
    <li class="divider"></li>
    <br class="hidden-lg hidden-md"/>
    <li><a href="#"><span class="glyphicon glyphicon-info-sign"></span> Sobre o sistema</a></li>
    <br class="hidden-lg hidden-md"/>
    <li class="divider"></li>
    <br class="hidden-lg hidden-md"/>
    <li><?php echo anchor('login/logout', '<span class="glyphicon glyphicon-off"> Sair');?></li>
    <br class="hidden-lg hidden-md"/>
  </ul>
</div>


</div>

</div>
</div>

<div class="container-fluid content">
  <div class="row">
    <div class="col-sm-1 menu">
      <ul class="hidden-xs">
      </ul>
    </div>
    <div class="col-sm-10">
    

