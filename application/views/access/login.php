<!doctype html>
<html lang="pt-BR">
<head>
<meta charset="utf-8">
<title><?php echo $titulo; ?></title>

<meta name="viewport" content="width=320, initial-scale=1">
<link rel='icon' type='image/png' href='<?php echo base_url("public/images/favicon.gif"); ?>'>


<?php 
echo link_tag("assets/bootstrap/css/bootstrap.css")."\n";
echo link_tag("assets/css/loadfonts.css.php")."\n";
echo link_tag('assets/style.css.less', "stylesheet/less");
//echo link_tag('assets/css/style.less', "stylesheet/less");
echo link_tag('assets/css/style.css');

?>

<!--  library Javascript -->
<script src="<?php echo base_url(); ?>assets/js/less.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/moderniz.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>


</head>

<body>
<div id="background" style="background-image:url(<?php echo base_url()."assets/images/bg-login/".$bg; ?>)"></div>
<div id="login" class="container">
<div class="row">
<div class="col-md-4 col-md-offset-4">
<form action="login" method="POST">

   <h1 id="title">Login</h1>
   
   <div class="form-group">   
   <div class="input-group">   		
      <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
      <?php echo form_input(array('name'=>'userName', 'id'=>'userName', 'placeholder'=>'Usuário', 'class'=>'form-control input-lg'), set_value('userName')); ?>
   <!-- <input type="text" name="userName" id="userName" class="form-control input-lg" placeholder="Usuário"> -->   
   </div>
   <small style="color:#f80d1a;font-size: 0.7em;font-style: italic;"><?php
   if($this->session->flashdata('error_userName')):
   echo $this->session->flashdata('error_userName');
   endif;
   ?></small>
   </div>
   <div class="form-group">   
   <div class="input-group">
      <div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
      <?php echo form_password(array('name'=>'password','id'=>'password', 'placeholder'=>'Senha', 'class'=>'form-control input-lg'), set_value('password')); 
      
      ?>
   <!-- <input type="password" name="password" id="password" class="form-control input-lg" placeholder="Senha"> -->   
   </div>
   <small style="color:#f80d1a;font-size: 0.7em;font-style: italic;"><?php ; 
   if($this->session->flashdata('error_password') || $this->session->flashdata('error_status')):
   echo $this->session->flashdata('error_password');
   echo $this->session->flashdata('error_status');
   endif;
   ?></small>
   </div>
    <div class="form-group">
      <input type="submit" name="entrar" id="entrar" class="btn btn-primary btn-lg btn-block" value="Login">
   </div>    
</form> 
</div>
</div>
<div class="row">
<div class="col-md-12" id="footer">
			
				<span>&copy; Copyright <?php echo date("Y")?> Todos os direitos reservadosa Budweb <br />
				developed by João Neto & Thiago Lima</span>
						
</div>
</div>
		</div>
	</body>
</html>
