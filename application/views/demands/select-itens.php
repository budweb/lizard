<div class="row">
<div class="row">
<div class="col-sm-8 col-md-8">

<div class="row">
<div class="col-md-12">
<fieldset class="search flex" status="open">
<legend>Pesquisa <span class="dropup"><span class="caret"></span></span></legend>
<div id="search" class="show">

<div class="col-sm-4 col-md-3">
<div class="form-group">
  <input id="cod" name="cod" type="number" placeholder="Por código" class="form-control">
</div>
</div>

<div class="col-sm-5 col-md-4">
<div class="form-group">
  <input id="name" name="name" type="text" placeholder="Por nome" class="form-control">
</div>

</div>

</div>
</fieldset>
</div>
</div>
<div class="row">
<div class="col-md-12">
<fieldset class="category flex" status="close">
<legend>Categoria <span class="dropdown"><span class="caret"></span></span></legend>
<div id="select_category" class="scroll-pane hide">
<?php foreach($categories->result() as $cat):?>

<div class="col-xs-6 col-sm-4 col-md-3 block">
<a href="<?php echo base_url('demands/ajax_select_itens/?cat='.$this->general->url_encode($cat->id)); ?>" class="category" style="background:<?php echo $cat->color?>">
<i class="category-icon-sm-white category-sm-<?php echo $cat->icon?>"></i>
<p><?php echo $cat->name?></p>
</a>
</div>

<?php endforeach;?>
</div>
</fieldset>
</div>
</div>


<div class="row">
<div class="col-md-12">
<fieldset>
<legend>Itens</legend>
<div id="select_itens" demand="<?php echo $this->uri->segment(3);?>" load="<?php echo base_url("demands/ajax_select_itens")?>">


</div>
</fieldset>
</div>
</div>


</div>
<div class="col-sm-4 col-md-4">
<div id="demands">
<fieldset>
<legend>
  <?php echo $titulo; ?>
  <?php if($this->session->userdata("type") == 4): ?>
  <a href="<?php echo base_url("checkout/command/".$this->uri->segment(3)); ?>" class="btn btn-primary" alt="Fechar pedido" title="Fechar pedido">
  <span class="glyphicon glyphicon-log-in"></span> Fechar Comanda
    </a>
  <?php endif; ?>
</legend>
<div class="info">
<?php if($this->uri->segment(4) != "desk"): ?>
<p><?php echo 'Mesa '.$demands->name?></p>
<?php endif; ?>
<p><?php echo mdate("Criado em %d/%m/%Y às %H:%i:%s", mysql_to_unix($demands->created));?></p>
<p><?php echo 'Por '.$demands->firstName." ".$demands->lastName; ?></p>

</div>
<div class="list"  load="<?php echo base_url("demands/ajax_demand_list/".$this->uri->segment(3)); ?>">

</div>
Adicionais


<div id="additional" class="row">
<div class="col-md-12">
<div class="checkbox">
    <label>
     <?php 
     // $this->db->select("commission");
     // $this->db->where("id", $this->session->userdata("id_property"));
     // $this->db->get("property");

     //$checked = $demands->serviceCost == 1 ? "checked=checked": "";
     $checked = $comission_std->commission == 1 ? "checked=checked": "";

     ?>
      <input type="checkbox" <?php echo $checked; ?> id="servicetax" load="<?php echo base_url("demands/ajax_service_tax/".$this->uri->segment(3)); ?>"> Cobrar Taxa de Serviço 10%
   
    </label>
  </div>
</div>
<div class="col-md-12">
<div class="row">
<div class="col-xs-4 col-sm-4 col-md-4">
<div class="radio">
  <label>
    <input type="radio" name="additional" id="plus" value="plus" checked>
    + Acréscimo
  </label>
</div>
</div>
<div class="col-xs-4 col-sm-4 col-md-4">
<div class="radio">
  <label>
    <input type="radio" name="additional" id="minus" value="minus">
    - Desconto
  </label>
</div>
</div>
</div>
</div>
<div class="col-xs-6 col-sm-6 col-md-6">

  <div class="form-group">
    <label for="description" class="control-label">Descrição</label>
    <input type="hidden" name="url" id="url" value="<?php echo base_url("demands/add_additional?id=".$this->uri->segment(3)); ?>">
      <input type="text" maxlength="21" class="form-control input-sm" name="description" id="description" placeholder="Descrição">
  </div>

</div>
<div class="col-xs-4 col-sm-4 col-md-4">

  <div class="form-group">
    <label for="value" class="control-label">Valor</label>
	  <input type="number" rel="money" class="form-control input-sm" name="value" id="value" placeholder="1.50">
  </div>

</div>
<div class="col-xs-2 col-sm-2 col-md-2">

  <div class="form-group">
    <label for="ok" class="control-label">&nbsp;</label>
	  <input type="button" class="btn btn-primary btn-sm" name="ok" id="ok" value="ok">
  </div>

</div>

<div class="col-xs-12 col-sm-12 col-md-12">

  <div class="form-group">

    <textarea name="comment" class="comment form-control input-sm" url="<?php echo base_url("demands/ajax_comment?id=".$this->general->url_encode($demands->id)); ?>" placeholder="Comentário"><?php echo @$demands->comment; ?></textarea>

  </div>

</div>
</div>
</fieldset>
</div>
</div>

</div>

<!-- moldal aplicar a quantidade -->
<div class='modal fade qty'  id='myModal' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
  <div class='modal-dialog'>
  <div class='modal-content'>
  <div class='modal-header'>
  <button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>
  <h4 class='modal-title'>Digite a Quantidade</h4>
  </div>
  <div class='modal-body'>
    <div class="row">
    <div class="col-xs-10 col-sm-10 col-md-10">
      <input type="number" name="qty" class="form-control input-md qty" placeholder="Ex: 120.40" />
    </div>
     <div class="col-xs-2 col-sm-2 col-md-2 unids">
     
     </div>
     </div>
  </div>
  <div class='modal-footer'>
  <button type='button' class='btn btn-default cancel' data-dismiss='modal'>Cancelar</button>
  <a class='btn btn-primary confirm'>Ok</a>
  </div>
  </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
  <!-- Button trigger modal -->
