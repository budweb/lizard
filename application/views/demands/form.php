<div class="row">
<div class="col-md-6 col-md-offset-3">
<form role="form" action="" method="POST">
<h1><?php echo $titulo; ?></h1>

<div class="row">
<div class="col-md-12">

<?php 
control("save");
control("add", array('label'=> 'Novo Ítem', 'href'=>'tables/add', 'show-link' => array($this->uri->segment(2), "update")));
control("list", array('label' => 'Lista', 'href'=>'tables/'));
 ?>
</div>
</div>


<div class="row">
<!-- Text input-->
<div class="col-md-3">
<div class="form-group">
  <input id="id" name="id" type="hidden" value="<?php echo @$row->id; ?>">

  <label for="name">Nome da Mesa</label>  
  <input id="name" name="name" type="text" placeholder="Nome" class="form-control" value="<?php echo @$row->name; ?>">
</div>
</div>
<div class="col-md-9">
<div class="form-group">
<!-- Text input-->
<div class="form-group">
  <label for="description">Descrição</label>  
  <input id="description" name="description" type="text" placeholder="Descrição" class="form-control" value="<?php echo @$row->description; ?>">
</div>
</div>
</div>
</div><!-- row -->

</form>
</div>
</div>