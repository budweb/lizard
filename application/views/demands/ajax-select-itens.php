<?php foreach($query->result() as $row):?>


<div class="col-xs-12 col-sm-6 col-md-4 block">
<span href="<?php echo base_url("demands/register_itens/".$this->general->url_encode($row->id)); ?>"
	unid="<?php echo $row->unid; ?>"
	unidname="<?php echo unid($row->unid) ?>"
	class="itens"
	style="background:<?php //echo $row->color; ?>">
	<i class="category-icon-sm-white category-sm-<?php echo $row->icon?>"></i>
<p class="cod"><?php echo str_pad($row->cod, 4, "0", STR_PAD_LEFT); ?> 
	<span class="click" style="display:none;"> (+<span class="num"></span>)</span>
</p>
<p class="name"><?php echo $row->name; ?></p>
<div class="info-context">
	<p><strong>Código:</strong> <?php echo str_pad($row->cod, 4, "0", STR_PAD_LEFT); ?></p>
	<p><strong>Nome:</strong> <?php echo $row->name; ?></p>
	<p><strong>Nome ECF:</strong> <?php echo $row->nameEcf; ?></p>
	<p><strong>Descr:</strong> <?php echo $row->description; ?></p>
	<p><strong>Categoria:</strong> <?php echo $row->category; ?></p>
	<p><strong>Preço:</strong> R$ <?php echo priceFormat($row->price); ?></p>
	<p><strong>Unid:</strong><?php echo unid($row->unid); ?></p>
	<?php if($row->type == 0): ?>
	<p><strong>Tipo:</strong>Produção</p>
	<?php elseif($row->type == 1):?>
	<p><strong>Tipo:</strong>Auto-serviço</p>
	<?php elseif($row->type == 2):?>
	<p><strong>Tipo:</strong>Estoque</p>
	<?php endif; ?>

</div>
</span>
</div>




<?php endforeach; ?>

<script>

$(document).ready(function(){
	select_itens_touch();
});


 

</script>