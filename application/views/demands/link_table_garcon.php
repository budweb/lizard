<h1>Ligue um Garçon a uma mesa</h1>
<div class="row link">
	<div class="col-md-3 getGarcon"></div>
	<div class="col-md-2 getLink"><img src='<?php echo base_url("public/images/link.png"); ?>' style="display:none" alt=""></div>
	<div class="col-md-3 getTable"></div>
	<div class="col-md-3 form">
		<form action="<?php echo base_url('demands/register_new_table_garcon'); ?>" method="post">
			<input type="hidden" name="garcon">
			<input type="hidden" name="table">
			<input type="submit" class="btn btn-primary" style="display:none" value="0k">
		</form>
	</div>

</div>
<div class="row link_table_garcon">
<div class="col-md-5">
<div class="dropdown">
  <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Escolha um Garçon
    <span class="caret"></span>
  </button>

  
	
  <ul class="dropdown-menu setGarcon" role="menu" aria-labelledby="dLabel">
  <?php 
  	foreach ($garcons->result() as $garcon):
  ?>
	
	<li value="<?php echo $this->general->url_encode(@$garcon->id) ?>">
  	<?php if(@$garcon->photo == NULL or @$garcon->photo == "0" or @$garcon->photo == ""): ?>
  	<img src='<?php echo base_url("public/images/no-photo.jpg"); ?>' class="photo_garcon img-thumbnail" alt="">
  	<?php else: ?>
  	<img src='<?php echo base_url("public/photos/thumbs/".@$garcon->photo); ?>' class="photo_garcon img-thumbnail" alt="">
	<?php endif; ?>

   <?php echo @$garcon->firstName." ".@$garcon->lastName; ?>
	</li>
    <?php endforeach; ?>
  </ul>

 
</div>
</div>


<div class="col-md-5">
<div class="dropdown">
  <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Escolha uma mesa
    <span class="caret"></span>
  </button>

  
	
  <ul class="dropdown-menu setTable" role="menu" aria-labelledby="dLabel">
  <?php 
  	foreach ($tables->result() as $table):
  ?>
	
	<li value="<?php echo $this->general->url_encode($table->id) ?>">
  	<img src='<?php echo base_url("public/images/icon-table-black.png"); ?>' class="photo_garcon img-thumbnail" alt="">
   <?php echo $table->name; ?>
	</li>
    <?php endforeach; ?>
  </ul>

 
</div>
</div>

</div>