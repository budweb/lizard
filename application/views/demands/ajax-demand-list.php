<?php
$mobile = is_mobile();


foreach ($query->result() as $row){


	$this->table->add_row(
			str_pad($row->cod, 6, "0", STR_PAD_LEFT)."<br />"."<small>".$row->qty." ".unid($row->unid)."</small>",
			$row->nameEcf."<br>R$ ".priceFormat($row->price),
			"R$ ".number_format($row->price*@$row->qty, 2, ',', ''),
			'<a href="'.base_url('demands/delete_itens_demand/'.$this->general->url_encode($row->id)).'" class="deleteItem"><span class="glyphicon glyphicon-remove"></span></a>'
			);
	$total = $row->price*@$row->qty + @$total;
	}
	
	
	//recebe os adicionais
	
	$this->db->where('demand_fk', $id_demand);
	$query = $this->db->get('additional');
	
	foreach ($query->result() as $row){
		$this->table->add_row(
				'',
				$row->description,
				"R$ ".number_format($row->price, 2, ',', ''),
				'<a href="'.base_url('demands/delete_additional/'.$this->general->url_encode($row->id)).'" class="deleteItem">
				<span class="glyphicon glyphicon-remove"></span></a>'
		);
		$total = $row->price + @$total;
	}
	
	//taxa de serviço 10%
	
	$this->db->select("serviceCost");
	$this->db->where('id', $id_demand);
	$query = $this->db->get('demands');
	$row = $query->row();
	
	if($row->serviceCost != NULL){
		$serviceCost = @$total*10/100;
		$this->table->add_row(
				'',
				'Tx de serviço',
				"R$ ".number_format($serviceCost, 2, ',', ''),
				''
		);
		
		$total = @$total+$serviceCost;
	}
	
	$this->table->add_row(
			'',
			'Total',
			'R$ '.number_format(@$total, 2, ',', ''),
			''
	);
	echo $this->table->generate();

	?>

	<script type="text/javascript">
		$(document).ready(function(){
			demand_list_touch_delete();

		});
		
	</script>