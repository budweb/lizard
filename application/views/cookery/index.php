<div class="row">
	<form class="form" method="post" action="">
		<div class="col-md-6 col-md-offset-3">
			<h1><?php echo $titulo; ?></h1>
			<?php $this->message->quick_message_generate(); ?>
			<div class="row">
				<div class="col-md-12">
					<?php
						control("add", array('label'=> 'Novo'));
						control("selectAll");
						control("deleteLot", array('href' => 'cookery/deletens/'));
					?>
				</div>
			</div>
			<?php
				$this->table->set_template(array ( 'table_open'  => '<table class="table">' ));
				$this->table->set_heading(array('data' =>  '', 'class' => 'hidden-xs'), 'Login', 'Conta', array('data' => 'Descrição', 'class' => 'hidden-xs'), '', '');
				foreach($cook->result() as $cook):
									
					$this->table->add_row(
							array('data' => checkEdit($this->general->url_encode(@$cook->id)), 'class' => 'hidden-xs'),
							$cook->userName,
							$this->session_logged->typeUser(5),
							array('data' => $cook->description, 'class' => 'hidden-xs'),
							updateLink("cookery/update/".$this->general->url_encode($cook->id)),
							deleteLink("cookery/delete/".$this->general->url_encode($cook->id), $cook->userName));
				endforeach;
				echo $this->table->generate();
				echo modal();				
			?>
		</div>
	</form>
</div>