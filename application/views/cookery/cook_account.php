<?php 
$id = $this->general->url_decode($this->uri->segment(3));

if($id!=NULL):
$row = $this->cook->get_byid($id)->row();
endif;
?>
<form action="" method="POST" class="form-horizontal">
 
 <legend><h2><?php echo $titulo;?></h2></legend>
 <div class="row" style="padding: 0 0 1% 0;">
	<div class="col-md-4 col-md-offset-4">

		<?php 
			control("savebt");
			control("other", array('label' => 'Listar', 'href'=>'cookery/', 'classIcon'=>'glyphicon glyphicon-th-list'));
		?>	
	</div>
</div>
  <div class="row">
  	<div class="col-md-4 col-md-offset-4">
<div class="form-group">
<!-- Text input-->
  <?php
  	echo form_hidden('type', 5);
  	echo form_hidden('id', $this->general->url_encode(@$row->id));
  	echo form_label('Login');
  	echo form_input(array('name'=>'userName', 'id'=>'userName', 'placeholder'=>'Digite um nome para o login', 'class'=>'form-control'), set_value('userName', @$row->userName)); 
  	echo form_error("userName");
  	echo '<br />';
  	echo form_error("type");
  	if($this->session->flashdata('userName')):
  		echo $this->session->flashdata('userName');
  	endif;
  ?>  
</div>
</div>
</div>

<div class="row">
  	<div class="col-md-4 col-md-offset-4">
<div class="form-group">
<!-- Text input-->
 <?php
  	echo form_label('Descrição');
  	echo form_input(array('name'=>'description', 'id'=>'description', 'placeholder'=>'Descrição', 'class'=>'form-control'), set_value('description', @$row->description)); 
  	echo form_error("description");
  ?>
</div>
</div>
</div>

<div class="row">
  	<div class="col-md-4 col-md-offset-4">
<div class="form-group">
<!-- Text input-->
 <?php
  	echo form_label('Senha');
  	echo form_password(array('name'=>'password', 'id'=>'password', 'placeholder'=>'Nova Senha', 'class'=>'form-control'), set_value('password')); 
  	echo form_error("password");
  ?>
</div>
</div>
</div>

<div class="row">
  	<div class="col-md-4 col-md-offset-4">
<div class="form-group">
<!-- Text input-->
  <?php 
  	echo form_label("Repita a senha");
  	echo form_password(array('name'=>'rPass', 'id'=>'rPass', 'placeholder'=>'Repita a senha', 'class'=>'form-control'), set_value('rPass')); 
  	echo form_error("rPass");
  ?>
</div>
</div>
</div> 
</form>