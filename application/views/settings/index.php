<?php $row = $this->settings->get_byid('property', $this->session->userdata('id_property'))->row(); ?>

<form action="" method="POST" id="form_settings">
	<fieldset>
		<legend><h2><?php echo $titulo;?></h2></legend>
		<?php $this->message->quick_message_generate(); ?>
		
		<div class="col-md-6" id="left_col"> <!-- Begin LEFT_COL -->
			<div class="row">
					<div class="col-md-7">	
						<?php				
							control("button", array('classIcon' => 'glyphicon glyphicon-ok', 'label'=>'Configurar'));
							control("cancel");
						?>			
					</div>
			</div> <!-- End ROW -->			
			
			<!-- CONFIGURAÇÕES DO ADMINISTRADOR -->
				
				<?php if($this->session->userdata("type") == 0 || $this->session->userdata("type") == 1):?>
				<div class="col-md-12">
					<h2><label>Localidade</label></h2>				
					<hr/>
				</div>			
				<div class="col-md-10">
					<?php					
						$attributes = array(
								'class' => '',
						);
     					echo form_label('Fuso horário', 'timezones', $attributes);
						echo timezone_menu(@$row->timeZone,"form-control");					
					?>
				</div>
				
				<div class="col-md-10">
				<?php 
					$checkbox = array(
						'name'=> 'daylightSaving[]',
						'class' => 'checkEdit',
						'value' => @$row->daylightSaving == '1' ? TRUE : FALSE,
						'checked' => @$row->daylightSaving
						);
						echo form_checkbox($checkbox);
						echo form_label('Horário de verão', 'daylightSaving[]');
				?>
			</div>
			
			<div class="col-md-12">			 
				<h2><label>Cobranças adicionais</label></h2>
				<hr/>
			</div>
			
			<div class="col-md-10">
				<div class="form-group">		
					<?php
						$checkbox = array(
						'name'=> 'commission[]',
						'class' => 'checkEdit',
						'value' => @$row->commission == '1' ? TRUE : FALSE,
						'checked' => @$row->commission
						);
						echo form_checkbox($checkbox);
						echo form_label('Comissão do garçon (10% por padrão)', 'commission[]');
					?>			 		
				</div>
			</div>
				<?php endif;?>			
				
				<!-- FIM DAS CONFIGURAÇÕES DO ADMINISTRADOR -->
				
				<!-- =========================================== -->
				
				<!-- CONFIGURAÇÕES DA COZINHA -->
				
				<?php if($this->session->userdata("type") == 5):?>
				<div class="col-md-12">				
					<h2><label>Redirecionamento Automático</label></h2>				
				<hr/>
			</div>			
			<div class="col-md-10">
				<?php
						echo form_hidden('val', 'val');
						$checkbox = array(
						'name'=> 'redirectAutomatic[]',
						'class' => 'checkEdit',
						'value' => @$row->redirectAutomatic == '1' ? TRUE : FALSE,
						'checked' => @$row->redirectAutomatic
						);
						echo form_checkbox($checkbox);
						echo form_label('Monitor da cozinha', 'redirectAutomatic[]');
						echo form_error("redirectAutomatic");
					?>
				</div>			
			
			<!-- <div class="col-md-12">			 
				<h2><label>Outras Configurações</label></h2>
				<hr/>
			</div>
			
			<div class="col-md-10">
				<div class="form-group">		
					<label>Adicionar configurações aqui</label>		 		
				</div>  -->			
			</div>
				<?php endif;?>
				
				<!-- FIM DAS CONFIGURAÇÕES DA COZINHA -->
				
				<!-- =========================================== -->
										
		</div> <!-- End LEFT_COL -->
		
		<!-- Begin RIGTH_COL --> <!-- <div class="col-md-6" id="left_col"> 
						
			 <div class="col-md-12">
				<h2><label>Outras Configurações</label></h2>
				<hr/>
			</div>
			
			<div class="col-md-10">
				
				</div>
				
			<div class="col-md-10">
				
			</div>
			
			<div class="col-md-12">
				<h2><label>Outras Configurações</label></h2>
				<hr/>
			</div>
			
			<div class="col-md-10">
				<div class="form-group">		
						 		
				</div>				
			</div>		
		</div> --> <!-- End RIGTH_COL -->	
										
	</fieldset>
</form>