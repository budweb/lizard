<div class="row">
	<form class="form" method="post" action="">
		<div class="col-md-8 col-md-offset-2">
			<h1><?php echo $titulo; ?></h1>
			<?php $this->message->quick_message_generate(); ?>
			<div class="row">
				<div class="col-md-12">
					<?php						
						control("add", array('label'=> 'Novo')); 
						control("selectAll");
						control("deleteLot", array('href' => 'deletens/'));
					?>
				</div>
			</div>
			
			<?php 
				$this->table->set_template(array ( 'table_open'  => '<table class="table">' ));
				$this->table->set_heading(array('data' =>  '', 'class' => 'hidden-xs'), 'Nome', 'Telefone', array('data' =>  'E-Mail', 'class' => 'hidden-xs'), '', '');
				foreach($customers->result() as $customers):
								
				$this->table->add_row(
						array('data' => checkEdit($this->general->url_encode(@$customers->id)), 'class' => 'hidden-xs'),
						$customers->name,
						'('.$customers->ddd.') '.$customers->phone,
						array('data' => $customers->email, 'class' => 'hidden-xs'),
						updateLink("customer/update/".$this->general->url_encode($customers->id)),
						deleteLink("customer/delete/".$this->general->url_encode($customers->id), $customers->name));
				endforeach;
				echo $this->table->generate();
				echo modal();
				if(@$pagination)echo @$pagination;
			?>
					
		</div>
	</form>
</div>