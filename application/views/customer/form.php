<?php 
	$id = $this->general->url_decode($this->uri->segment(3));

	if($id!=NULL):
		$row = $this->customer->get_byid($id)->row();
	endif;
?>

<form action="" method="POST" enctype="multipart/form-data" id="form_customer">
	<fieldset>
		<legend><h2><?php echo $titulo.'<b>'.@$row->name.'</b>';?></h2></legend>
		<div class="row"> <!-- BEGIN ROW - BUTTON -->
			<div class="col-md-3 col-md-offset-2">
				<?php
					control("savebt");
					control("other", array('label' => 'Listar', 'href'=>'customer/', 'classIcon'=>'glyphicon glyphicon-th-list'));										
				?>			
			</div>		
		</div> <!-- END ROW - BUTTON -->
		
		<div class="row"> <!-- BEGIN ROW - LINE 01 -->
			<div class="col-md-4 col-md-offset-2">
				<div class="form-group">
					<?php
						echo form_hidden('id', $this->general->url_encode(@$row->id)); 
						echo form_label("Nome");
						echo form_input(array('name'=>'name', 'id'=>'name', 'placeholder'=>'Nome', 'class'=>'form-control'), set_value('name', @$row->name), 'autofocus');
						echo form_error("name");
					?>
				</div>
			</div>
			
			<div class="col-md-1">
				<div class="form-group">
					<?php 
						echo form_label("DDD");
						echo form_input(array('type'=>'number', 'name'=>'ddd', 'id'=>'ddd', 'placeholder'=>'ddd', 'class'=>'form-control'), set_value('ddd', @$row->ddd));
						echo form_error("ddd");
					?>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="form-group">
					<?php 
						echo form_label("Telefone");
						echo form_input(array('type'=>'number', 'name'=>'phone', 'id'=>'phone', 'placeholder'=>'Telefone', 'class'=>'form-control'), set_value('phone', @$row->phone));
						echo form_error("phone");
					?>
				</div>
			</div>			
		</div> <!-- BEGIN ROW - LINE 01 -->
		
		<div class="row"> <!-- BEGIN LINE 02 -->
			<div class="col-md-5 col-md-offset-2">
				<div class="form-group">
					<?php 
						echo form_label("Endereço");
						echo form_input(array('name'=>'address', 'id'=>'address', 'placeholder'=>'Endereço', 'class'=>'form-control'), set_value('address', @$row->address));
						echo form_error('address')
					?>
				</div>				
			</div>
			
			<div class="col-md-3">
				<div class="form-group">
					<?php 
						echo form_label("Bairro");
						echo form_input(array('name'=>'district', 'id'=>'district', 'placeholder'=>'Bairro', 'class'=>'form-control'), set_value('address', @$row->district));
						echo form_error('district')
					?>
				</div>				
			</div>
		</div> <!-- END LINE 02 -->
		
		<div class="row"> <!-- BEGIN LINE 03 -->
			<div class="col-md-1 col-md-offset-2">
				<div class="form-group">
					<?php 
						echo form_label("Número");
						echo form_input(array('name'=>'number', 'id'=>'number', 'placeholder'=>'Nº', 'class'=>'form-control'), set_value('number', @$row->number));
						echo form_error('number')
					?>
				</div>				
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<?php 
						echo form_label("CEP");
						echo form_input(array('name'=>'zipCode', 'id'=>'zipCode', 'placeholder'=>'CEP', 'class'=>'form-control'), set_value('zipCode', @$row->zipCode));
						echo form_error('zipCode')
					?>
				</div>				
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<?php 
						echo form_label("Complemento");
						echo form_input(array('name'=>'complement', 'id'=>'complement', 'placeholder'=>'Complemento', 'class'=>'form-control'), set_value('complement', @$row->complement));
						echo form_error('complement')
					?>
				</div>				
			</div>			
		</div> <!-- END LINE 03 -->
		
		<div class="row"> <!-- BEGIN LINE 04 -->
			<div class="col-md-3 col-md-offset-2">
				<div class="form-group">
					<?php 
						echo form_label("Cidade");
						echo form_input(array('name'=>'city', 'id'=>'city', 'placeholder'=>'Cidade', 'class'=>'form-control'), set_value('city', @$row->city));
						echo form_error('city')
					?>
				</div>				
			</div>
			
			<div class="col-md-1">
				<div class="form-group">		
					<?php 
						echo form_label('Estado');
						$options = array('Acre'=>'AC', 'Alagoas'=>'AL', 'Amapá'=>'AP', 'Amazonas'=>'AM', 'Bahia'=>'BA', 'Ceará'=>'CE', 'Distrito Federal'=>'DF', 'Espírito Santo'=>'ES', 'Goiás'=>'GO', 'Maranhão'=>'MA', 'Mato Grosso'=>'MT', 'Mato Grosso do Sul'=>'MS', 'Minas Gerais'=>'MG', 'Pará'=>'PA', 'Paraíba'=>'PB', 'Paraná'=>'PR', 'Pernambuco'=>'PE', 'Piauí'=>'PI', 'Rio de Janeiro'=>'RJ', 'Rio Grande do Norte'=>'RN', 'Rio Grande do Sul'=>'RS', 'Rondônia'=>'RO', 'Roraima'=>'RR', 'Santa Catarina'=>'SC', 'São Paulo'=>'SP', 'Sergipe'=>'SE', 'Tocantins'=>'TO');
						$attr = 'id="state" class="form-control"';				
						$config = array(set_value('state', @$row->state));
				
						echo form_dropdown('state', $options, $config, $attr);
						echo form_error("state");
			 		?>
				</div>		
			</div>			
		
			<div class="col-md-4">
				<div class="form-group">
					<?php 
						echo form_label("E-Mail");
						echo form_input(array('type'=>'email', 'name'=>'email', 'id'=>'email', 'placeholder'=>'E-Mail', 'class'=>'form-control'), set_value('email', @$row->email));
						echo form_error('email')
					?>
				</div>				
			</div>			
		</div> <!-- END LINE 04 -->
		
	</fieldset>
</form>
