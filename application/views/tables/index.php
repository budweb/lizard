<div class="row">
<form class="form" method="post" action="">
<div class="col-md-8 col-md-offset-2">
<h1><?php echo $titulo;?></h1>

<?php $this->message->quick_message_generate(); ?>

 <div class="row">
 <div class="col-md-12">
<?php 
 control("add", array('label'=> 'Nova Mesa', 'href' => 'tables/add'));
 control("selectAll");
 control("deleteLot", array('href' => 'deleteLot/'));
 ?>
</div>
</div>

<?php

$this->table->set_template(array ( 'table_open'  => '<table class="table">' ));
$this->table->set_heading(array('data' =>  '', 'class' => 'hidden-xs'), 'Mesa', array('data' =>  'Descrição', 'class' => 'hidden-xs'), '', '');

foreach($query->result() as $row):
	
	$this->table->add_row(
			array('data' => checkEdit($this->general->url_encode($row->id)), 'class' => 'hidden-xs'),
			$row->name,
			array('data' => $row->description, 'class' => 'hidden-xs'),
			updateLink("tables/update/".$this->general->url_encode($row->id)),
			deleteLink("tables/delete/".$this->general->url_encode($row->id), $row->name));
endforeach;

echo $this->table->generate();


?>
<?php 
echo modal();
?>
<?php if(@$pagination)echo @$pagination; ?>

</div>

</form>
</div>