-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 17-Maio-2015 às 15:48
-- Versão do servidor: 5.5.39
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lizard`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `stock`
--

CREATE TABLE IF NOT EXISTS `stock` (
`id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `unid` int(11) NOT NULL,
  `quantity` int(10) NOT NULL,
  `quantityMin` int(10) NOT NULL,
  `lastUpdated` datetime NOT NULL,
  `controlAtive` tinyint(4) DEFAULT NULL,
  `id_property` int(11) NOT NULL,
  `id_itens` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=70 ;

--
-- Extraindo dados da tabela `stock`
--

INSERT INTO `stock` (`id`, `name`, `description`, `unid`, `quantity`, `quantityMin`, `lastUpdated`, `controlAtive`, `id_property`, `id_itens`) VALUES
(42, 'Bohemia ', 'Cerveja 600 ml', 0, 0, 150, '2015-05-15 15:49:03', 1, 3, 1),
(49, 'Coca-Cola', 'Refrigerante  1L', 0, 50, 100, '2015-05-15 18:06:07', 1, 3, 38),
(67, 'Skol Lata', 'Cerveja em lata 350ml', 0, 0, 100, '2015-05-15 22:30:18', 1, 3, 31),
(69, 'Carangueijo', 'Carangueijo', 0, 0, 30, '2015-05-15 22:31:44', 1, 3, 11);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `stock`
--
ALTER TABLE `stock`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `stock`
--
ALTER TABLE `stock`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=70;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
