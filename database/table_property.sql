-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 30-Mar-2015 às 16:33
-- Versão do servidor: 5.5.39
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lizard`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `property`
--

CREATE TABLE IF NOT EXISTS `property` (
`id` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `timeZone` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `daylightSaving` tinyint(11) NOT NULL,
  `commission` tinyint(11) NOT NULL,
  `fantasyName` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `district` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `ddd` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `zipCode` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `cnpj` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `manager` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `webSite` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stateRegistration` int(100) DEFAULT NULL,
  `municipalRegistration` int(100) DEFAULT NULL,
  `filial` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `ative` tinyint(11) NOT NULL,
  `numOwner` int(10) NOT NULL,
  `numAdministrative` int(10) NOT NULL,
  `numMaitre` int(10) NOT NULL,
  `numWaiter` int(10) NOT NULL,
  `numDesk` int(10) NOT NULL,
  `numCook` int(10) NOT NULL,
  `created` datetime NOT NULL,
  `initialCount` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `dateTmp` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `cleaningTime` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `tomorrow` tinyint(4) NOT NULL,
  `redirectAutomatic` tinyint(4) NOT NULL,
  `basic` tinyint(4) NOT NULL,
  `automaticService` tinyint(4) NOT NULL,
  `stock` tinyint(4) NOT NULL,
  `financialControl` tinyint(4) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `property`
--
ALTER TABLE `property`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `property`
--
ALTER TABLE `property`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
