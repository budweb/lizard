-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 30-Mar-2015 às 16:34
-- Versão do servidor: 5.5.39
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lizard`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(25) NOT NULL,
  `firstName` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `lastName` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cpf` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ddd` int(10) NOT NULL,
  `phone` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(10) DEFAULT NULL,
  `id_property` int(11) NOT NULL,
  `userName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime NOT NULL,
  `timeZone` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `daylightSaving` tinyint(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=152 ;

--
-- Extraindo dados da tabela `user`
--

INSERT INTO `user` (`id`, `firstName`, `lastName`, `cpf`, `password`, `photo`, `type`, `email`, `ddd`, `phone`, `status`, `id_property`, `userName`, `description`, `created`, `timeZone`, `daylightSaving`) VALUES
(102, 'João ', 'Neto', '66891086353', '81dc9bdb52d04dc20036dbd8313ed055', 'ad56e403c2cd27db0b05f58d903c9ed3_thumb.JPG', '6', 'joaonetoce@hotmail.com', 85, '99457802', 1, 0, NULL, NULL, '0000-00-00 00:00:00', 'UM3', 0),
(140, 'Thiago', 'Lima', '987654321', '81dc9bdb52d04dc20036dbd8313ed055', '0', '6', 'thiagolima86@gmail.com', 85, '96526648', 1, 0, NULL, NULL, '2015-03-19 10:01:11', 'UM3', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id` int(25) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=152;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
