
-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 15/06/2015 às 21:41:11
-- Versão do Servidor: 10.0.12-MariaDB
-- Versão do PHP: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `u987883859_lizar`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `demand_item`
--

CREATE TABLE IF NOT EXISTS `demand_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `demand_fk` int(11) DEFAULT NULL,
  `item_fk` int(11) DEFAULT NULL,
  `qty` float DEFAULT NULL,
  `price` decimal(11,2) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `startProduction` datetime DEFAULT NULL,
  `finishedProduction` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=198 ;

--
-- Extraindo dados da tabela `demand_item`
--

INSERT INTO `demand_item` (`id`, `demand_fk`, `item_fk`, `qty`, `price`, `status`, `startProduction`, `finishedProduction`) VALUES
(99, 18, 9, 1, NULL, 2, '2015-04-02 16:15:43', '2015-04-02 16:24:09'),
(118, NULL, 8, 1, NULL, NULL, NULL, NULL),
(119, NULL, 8, 1, NULL, NULL, NULL, NULL),
(120, NULL, 8, 1, NULL, NULL, NULL, NULL),
(121, NULL, 8, 1, NULL, NULL, NULL, NULL),
(123, 18, 8, 0.6, NULL, NULL, NULL, NULL),
(124, 18, 8, 0.4, NULL, NULL, NULL, NULL),
(128, 19, 12, 2, '6.00', NULL, NULL, NULL),
(130, 19, 14, 0.3, '6.60', NULL, NULL, NULL),
(131, 19, 9, 2, '44.00', NULL, NULL, NULL),
(132, 26, 10, 2, '21.00', NULL, NULL, NULL),
(133, 26, 8, 0.2, '2.20', NULL, NULL, NULL),
(134, 28, 9, 1, '22.00', NULL, NULL, NULL),
(135, 28, 10, 2, '21.00', NULL, NULL, NULL),
(136, 29, 9, 1, '22.00', NULL, NULL, NULL),
(137, 29, 12, 3, '3.00', NULL, NULL, NULL),
(138, 30, 9, 1, '22.00', NULL, NULL, NULL),
(139, 30, 12, 1, '3.00', NULL, NULL, NULL),
(140, 30, 13, 1, '6.00', NULL, NULL, NULL),
(141, 31, 9, 1, '22.00', NULL, NULL, NULL),
(142, 31, 12, 1, '3.00', NULL, NULL, NULL),
(143, 31, 8, 0.2, '2.20', NULL, NULL, NULL),
(144, 32, 10, 1, '21.00', NULL, NULL, NULL),
(145, 32, 13, 1, '6.00', NULL, NULL, NULL),
(146, 33, 9, 1, '22.00', NULL, NULL, NULL),
(147, 39, 10, 1, '21.00', NULL, NULL, NULL),
(148, 39, 9, 1, '22.00', NULL, NULL, NULL),
(149, 40, 10, 1, '21.00', NULL, NULL, NULL),
(150, 40, 13, 1, '6.00', NULL, NULL, NULL),
(151, 42, 10, 1, '21.00', NULL, NULL, NULL),
(152, 42, 9, 1, '22.00', NULL, NULL, NULL),
(153, 42, 12, 1, '3.00', NULL, NULL, NULL),
(154, 43, 9, 1, '22.00', NULL, NULL, NULL),
(155, 43, 8, 0.3, '2.20', NULL, NULL, NULL),
(156, 43, 10, 1, '21.00', NULL, NULL, NULL),
(157, 45, 10, 1, '21.00', NULL, NULL, NULL),
(158, 45, 13, 1, '6.00', NULL, NULL, NULL),
(159, 45, 12, 2, '3.00', NULL, NULL, NULL),
(160, 46, 9, 1, '22.00', NULL, NULL, NULL),
(161, 46, 12, 1, '3.00', NULL, NULL, NULL),
(162, 46, 15, 2, '80.00', NULL, NULL, NULL),
(163, 44, 10, 1, '21.00', NULL, NULL, NULL),
(164, 44, 13, 1, '6.00', NULL, NULL, NULL),
(165, 47, 9, 1, '22.00', NULL, NULL, NULL),
(166, 47, 12, 2, '3.00', NULL, NULL, NULL),
(167, 48, 10, 1, '21.00', NULL, NULL, NULL),
(168, 48, 13, 2, '6.00', NULL, NULL, NULL),
(169, 49, 9, 2, '22.00', NULL, NULL, NULL),
(170, 49, 10, 1, '21.00', NULL, NULL, NULL),
(171, 49, 13, 1, '6.00', NULL, NULL, NULL),
(172, 50, 10, 1, '21.00', NULL, NULL, NULL),
(173, 50, 13, 2, '6.00', NULL, NULL, NULL),
(174, 50, 15, 1, '80.00', NULL, NULL, NULL),
(175, 51, 10, 1, '21.00', NULL, NULL, NULL),
(176, 51, 13, 1, '6.00', NULL, NULL, NULL),
(177, 51, 12, 1, '3.00', NULL, NULL, NULL),
(178, 51, 11, 4, '4.10', NULL, NULL, NULL),
(179, 52, 10, 1, '21.00', NULL, NULL, NULL),
(180, 52, 13, 1, '6.00', NULL, NULL, NULL),
(181, 52, 12, 2, '3.00', NULL, NULL, NULL),
(182, 52, 14, 0.6, '22.00', NULL, NULL, NULL),
(183, 54, 9, 2, '22.00', NULL, NULL, NULL),
(184, 54, 12, 1, '3.00', NULL, NULL, NULL),
(185, 54, 13, 1, '6.00', NULL, NULL, NULL),
(196, 56, 17, 1, '5.00', NULL, NULL, NULL),
(193, 53, 18, 1, '5.00', NULL, NULL, NULL),
(195, 53, 17, 2, '5.00', NULL, NULL, NULL),
(197, 56, 18, 2, '5.00', NULL, NULL, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
