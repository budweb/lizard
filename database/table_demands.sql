-- phpMyAdmin SQL Dump
-- version 4.4.3
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Tempo de geração: 07/07/2015 às 01:59
-- Versão do servidor: 5.5.43-0ubuntu0.14.04.1
-- Versão do PHP: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de dados: `lizard`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `demands`
--

CREATE TABLE IF NOT EXISTS `demands` (
  `id` int(11) NOT NULL,
  `table_fk` int(11) DEFAULT NULL,
  `user_fk` int(11) NOT NULL,
  `serviceCost` int(1) DEFAULT NULL,
  `total` decimal(11,2) DEFAULT NULL,
  `money` decimal(10,2) DEFAULT NULL,
  `paymethod` int(11) DEFAULT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `created` datetime NOT NULL,
  `id_property` int(11) NOT NULL,
  `finished` datetime DEFAULT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=59 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `demands`
--
ALTER TABLE `demands`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `demands`
--
ALTER TABLE `demands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=59;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
