-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 17-Maio-2015 às 15:48
-- Versão do servidor: 5.5.39
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lizard`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `itens`
--

CREATE TABLE IF NOT EXISTS `itens` (
`id` int(11) NOT NULL,
  `cod` int(5) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `nameEcf` varchar(29) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `unid` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `cost` decimal(10,2) NOT NULL,
  `margin` decimal(10,2) NOT NULL,
  `productionTime` time NOT NULL,
  `id_property` int(11) NOT NULL,
  `category` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=39 ;

--
-- Extraindo dados da tabela `itens`
--

INSERT INTO `itens` (`id`, `cod`, `name`, `nameEcf`, `description`, `type`, `unid`, `price`, `cost`, `margin`, `productionTime`, `id_property`, `category`) VALUES
(1, 1, 'Bohemia ', 'Bohe.  ', 'Cerveja 600 ml', 2, 0, '6.00', '3.00', '3.00', '00:00:00', 3, 1),
(3, 3, 'Calabrezza Grande', 'Calabr. Gran. ', 'Linguiça, queijo, cebola, tomate, orégano', 0, 0, '22.50', '15.00', '7.50', '00:20:00', 3, 2),
(4, 4, 'Sorvete de Chocolate', 'Sorv.  Choco. ', '2 bolas de sorvete', 1, 3, '4.50', '3.00', '1.50', '00:00:00', 3, 3),
(5, 5, 'Napolitano', 'Napoli. ', '2 bolas de sorvete', 1, 3, '6.00', '4.00', '2.00', '00:00:00', 3, 3),
(6, 6, 'Pizza mussarela', 'Piz. mussa. ', '', 0, 0, '25.00', '15.00', '10.00', '00:20:00', 3, 2),
(7, 7, 'Pudim de leite', 'Pud.  lei. ', '', 0, 2, '10.00', '5.00', '5.00', '00:10:00', 3, 3),
(10, 10, 'Almoço', 'Almo. ', '', 1, 1, '32.00', '10.00', '22.00', '00:00:00', 3, 5),
(11, 11, 'Carangueijo', 'car. ', 'Carangueijo', 0, 0, '6.00', '3.00', '3.00', '00:15:00', 3, 4),
(31, 12, 'Skol Lata', 'Sko. Lat. ', 'Cerveja em lata 350ml', 2, 0, '3.50', '2.00', '1.50', '00:10:00', 3, 1),
(38, 13, 'Coca-Cola', 'Coca-. ', 'Refrigerante  1L', 2, 0, '2.00', '1.00', '1.00', '00:00:00', 3, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `itens`
--
ALTER TABLE `itens`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `itens`
--
ALTER TABLE `itens`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=39;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
