-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 17-Maio-2015 às 15:47
-- Versão do servidor: 5.5.39
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lizard`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `stockAux`
--

CREATE TABLE IF NOT EXISTS `stockAux` (
`id` int(11) NOT NULL,
  `id_property` int(11) NOT NULL,
  `id_itens` int(11) DEFAULT NULL,
  `observation` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `id_stock` int(11) NOT NULL,
  `updated` datetime NOT NULL,
  `quantity` int(11) NOT NULL,
  `added` int(30) DEFAULT NULL,
  `remove` int(30) DEFAULT NULL,
  `color` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=98 ;

--
-- Extraindo dados da tabela `stockAux`
--

INSERT INTO `stockAux` (`id`, `id_property`, `id_itens`, `observation`, `id_stock`, `updated`, `quantity`, `added`, `remove`, `color`) VALUES
(66, 3, 1, 'Cadastrado no estoque', 42, '2015-05-15 15:49:03', 0, 0, 0, '#FFFF00'),
(75, 3, 38, 'Cadastrado no estoque', 49, '2015-05-15 18:05:52', 0, 0, 0, '#FFFF00'),
(76, 3, 38, 'Acrescentado 50 Unidade(s)', 49, '2015-05-15 18:06:07', 50, 50, 0, '#00FF00'),
(95, 3, 31, 'Cadastrado no estoque', 67, '2015-05-15 22:30:18', 0, 0, 0, '#FFFF00'),
(97, 3, 11, 'Cadastrado no estoque', 69, '2015-05-15 22:31:44', 0, 0, 0, '#FFFF00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `stockAux`
--
ALTER TABLE `stockAux`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `stockAux`
--
ALTER TABLE `stockAux`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=98;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
